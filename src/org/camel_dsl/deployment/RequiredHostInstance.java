/**
 */
package org.camel_dsl.deployment;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Required Host Instance</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see org.camel_dsl.deployment.DeploymentPackage#getRequiredHostInstance()
 * @model
 * @generated
 */
public interface RequiredHostInstance extends HostingPortInstance {
} // RequiredHostInstance
