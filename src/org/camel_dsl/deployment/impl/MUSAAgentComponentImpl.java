/**
 */
package org.camel_dsl.deployment.impl;

import org.camel_dsl.deployment.DeploymentPackage;
import org.camel_dsl.deployment.MUSAAgentComponent;
import org.camel_dsl.deployment.RequiredCommunication;
import org.camel_dsl.deployment.RequiredHost;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>MUSA Agent Component</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link org.camel_dsl.deployment.impl.MUSAAgentComponentImpl#getCompositeInternalComponents <em>Composite Internal Components</em>}</li>
 *   <li>{@link org.camel_dsl.deployment.impl.MUSAAgentComponentImpl#getRequiredCommunications <em>Required Communications</em>}</li>
 *   <li>{@link org.camel_dsl.deployment.impl.MUSAAgentComponentImpl#getRequiredHost <em>Required Host</em>}</li>
 *   <li>{@link org.camel_dsl.deployment.impl.MUSAAgentComponentImpl#getVersion <em>Version</em>}</li>
 *   <li>{@link org.camel_dsl.deployment.impl.MUSAAgentComponentImpl#getContextPath <em>Context Path</em>}</li>
 * </ul>
 *
 * @generated
 */
public class MUSAAgentComponentImpl extends ComponentImpl implements MUSAAgentComponent {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected MUSAAgentComponentImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return DeploymentPackage.Literals.MUSA_AGENT_COMPONENT;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	public EList<MUSAAgentComponent> getCompositeInternalComponents() {
		return (EList<MUSAAgentComponent>)eGet(DeploymentPackage.Literals.MUSA_AGENT_COMPONENT__COMPOSITE_INTERNAL_COMPONENTS, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	public EList<RequiredCommunication> getRequiredCommunications() {
		return (EList<RequiredCommunication>)eGet(DeploymentPackage.Literals.MUSA_AGENT_COMPONENT__REQUIRED_COMMUNICATIONS, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public RequiredHost getRequiredHost() {
		return (RequiredHost)eGet(DeploymentPackage.Literals.MUSA_AGENT_COMPONENT__REQUIRED_HOST, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setRequiredHost(RequiredHost newRequiredHost) {
		eSet(DeploymentPackage.Literals.MUSA_AGENT_COMPONENT__REQUIRED_HOST, newRequiredHost);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getVersion() {
		return (String)eGet(DeploymentPackage.Literals.MUSA_AGENT_COMPONENT__VERSION, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setVersion(String newVersion) {
		eSet(DeploymentPackage.Literals.MUSA_AGENT_COMPONENT__VERSION, newVersion);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getContextPath() {
		return (String)eGet(DeploymentPackage.Literals.MUSA_AGENT_COMPONENT__CONTEXT_PATH, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setContextPath(String newContextPath) {
		eSet(DeploymentPackage.Literals.MUSA_AGENT_COMPONENT__CONTEXT_PATH, newContextPath);
	}

} //MUSAAgentComponentImpl
