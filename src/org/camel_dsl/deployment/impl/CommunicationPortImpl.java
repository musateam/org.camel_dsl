/**
 */
package org.camel_dsl.deployment.impl;

import org.camel_dsl.deployment.CommunicationPort;
import org.camel_dsl.deployment.DeploymentPackage;

import org.eclipse.emf.ecore.EClass;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Communication Port</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link org.camel_dsl.deployment.impl.CommunicationPortImpl#getPortNumber <em>Port Number</em>}</li>
 *   <li>{@link org.camel_dsl.deployment.impl.CommunicationPortImpl#getMinPortNumber <em>Min Port Number</em>}</li>
 *   <li>{@link org.camel_dsl.deployment.impl.CommunicationPortImpl#getMaxPortNumber <em>Max Port Number</em>}</li>
 *   <li>{@link org.camel_dsl.deployment.impl.CommunicationPortImpl#getContextPath <em>Context Path</em>}</li>
 * </ul>
 *
 * @generated
 */
public abstract class CommunicationPortImpl extends DeploymentElementImpl implements CommunicationPort {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected CommunicationPortImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return DeploymentPackage.Literals.COMMUNICATION_PORT;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public int getPortNumber() {
		return (Integer)eGet(DeploymentPackage.Literals.COMMUNICATION_PORT__PORT_NUMBER, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setPortNumber(int newPortNumber) {
		eSet(DeploymentPackage.Literals.COMMUNICATION_PORT__PORT_NUMBER, newPortNumber);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public int getMinPortNumber() {
		return (Integer)eGet(DeploymentPackage.Literals.COMMUNICATION_PORT__MIN_PORT_NUMBER, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setMinPortNumber(int newMinPortNumber) {
		eSet(DeploymentPackage.Literals.COMMUNICATION_PORT__MIN_PORT_NUMBER, newMinPortNumber);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public int getMaxPortNumber() {
		return (Integer)eGet(DeploymentPackage.Literals.COMMUNICATION_PORT__MAX_PORT_NUMBER, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setMaxPortNumber(int newMaxPortNumber) {
		eSet(DeploymentPackage.Literals.COMMUNICATION_PORT__MAX_PORT_NUMBER, newMaxPortNumber);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getContextPath() {
		return (String)eGet(DeploymentPackage.Literals.COMMUNICATION_PORT__CONTEXT_PATH, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setContextPath(String newContextPath) {
		eSet(DeploymentPackage.Literals.COMMUNICATION_PORT__CONTEXT_PATH, newContextPath);
	}

} //CommunicationPortImpl
