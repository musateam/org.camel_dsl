/**
 */
package org.camel_dsl.deployment.impl;

import org.camel_dsl.deployment.CommunicationPort;
import org.camel_dsl.deployment.CommunicationPortInstance;
import org.camel_dsl.deployment.DeploymentPackage;

import org.eclipse.emf.ecore.EClass;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Communication Port Instance</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link org.camel_dsl.deployment.impl.CommunicationPortInstanceImpl#getType <em>Type</em>}</li>
 * </ul>
 *
 * @generated
 */
public class CommunicationPortInstanceImpl extends DeploymentElementImpl implements CommunicationPortInstance {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected CommunicationPortInstanceImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return DeploymentPackage.Literals.COMMUNICATION_PORT_INSTANCE;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public CommunicationPort getType() {
		return (CommunicationPort)eGet(DeploymentPackage.Literals.COMMUNICATION_PORT_INSTANCE__TYPE, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setType(CommunicationPort newType) {
		eSet(DeploymentPackage.Literals.COMMUNICATION_PORT_INSTANCE__TYPE, newType);
	}

} //CommunicationPortInstanceImpl
