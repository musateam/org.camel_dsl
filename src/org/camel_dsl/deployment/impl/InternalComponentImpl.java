/**
 */
package org.camel_dsl.deployment.impl;

import java.lang.reflect.InvocationTargetException;

import org.camel_dsl.camel.COTSAServiceType;
import org.camel_dsl.camel.MUSAComponentType;

import org.camel_dsl.deployment.DeploymentPackage;
import org.camel_dsl.deployment.InternalComponent;
import org.camel_dsl.deployment.MUSAContainer;
import org.camel_dsl.deployment.ProvidedHost;
import org.camel_dsl.deployment.ProvidedMUSACapability;
import org.camel_dsl.deployment.RequiredCommunication;
import org.camel_dsl.deployment.RequiredHost;
import org.camel_dsl.deployment.RequiredMUSACapability;

import org.eclipse.emf.common.util.BasicEList;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.common.util.WrappedException;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EOperation;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Internal Component</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link org.camel_dsl.deployment.impl.InternalComponentImpl#getCompositeInternalComponents <em>Composite Internal Components</em>}</li>
 *   <li>{@link org.camel_dsl.deployment.impl.InternalComponentImpl#getRequiredCommunications <em>Required Communications</em>}</li>
 *   <li>{@link org.camel_dsl.deployment.impl.InternalComponentImpl#getRequiredHost <em>Required Host</em>}</li>
 *   <li>{@link org.camel_dsl.deployment.impl.InternalComponentImpl#getRequiredContainer <em>Required Container</em>}</li>
 *   <li>{@link org.camel_dsl.deployment.impl.InternalComponentImpl#getRequiredContainerVMHost <em>Required Container VM Host</em>}</li>
 *   <li>{@link org.camel_dsl.deployment.impl.InternalComponentImpl#getVersion <em>Version</em>}</li>
 *   <li>{@link org.camel_dsl.deployment.impl.InternalComponentImpl#getContextPath <em>Context Path</em>}</li>
 *   <li>{@link org.camel_dsl.deployment.impl.InternalComponentImpl#getType <em>Type</em>}</li>
 *   <li>{@link org.camel_dsl.deployment.impl.InternalComponentImpl#getOrder <em>Order</em>}</li>
 *   <li>{@link org.camel_dsl.deployment.impl.InternalComponentImpl#isIPpublic <em>IPpublic</em>}</li>
 *   <li>{@link org.camel_dsl.deployment.impl.InternalComponentImpl#getTypeEnum <em>Type Enum</em>}</li>
 *   <li>{@link org.camel_dsl.deployment.impl.InternalComponentImpl#getRequiredMUSACapabilities <em>Required MUSA Capabilities</em>}</li>
 *   <li>{@link org.camel_dsl.deployment.impl.InternalComponentImpl#getProvidedMUSACapabilities <em>Provided MUSA Capabilities</em>}</li>
 * </ul>
 *
 * @generated
 */
public class InternalComponentImpl extends ComponentImpl implements InternalComponent {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected InternalComponentImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return DeploymentPackage.Literals.INTERNAL_COMPONENT;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	public EList<InternalComponent> getCompositeInternalComponents() {
		return (EList<InternalComponent>)eGet(DeploymentPackage.Literals.INTERNAL_COMPONENT__COMPOSITE_INTERNAL_COMPONENTS, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	public EList<RequiredCommunication> getRequiredCommunications() {
		return (EList<RequiredCommunication>)eGet(DeploymentPackage.Literals.INTERNAL_COMPONENT__REQUIRED_COMMUNICATIONS, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public RequiredHost getRequiredHost() {
		return (RequiredHost)eGet(DeploymentPackage.Literals.INTERNAL_COMPONENT__REQUIRED_HOST, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setRequiredHost(RequiredHost newRequiredHost) {
		eSet(DeploymentPackage.Literals.INTERNAL_COMPONENT__REQUIRED_HOST, newRequiredHost);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MUSAContainer getRequiredContainer() {
		return (MUSAContainer)eGet(DeploymentPackage.Literals.INTERNAL_COMPONENT__REQUIRED_CONTAINER, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setRequiredContainer(MUSAContainer newRequiredContainer) {
		eSet(DeploymentPackage.Literals.INTERNAL_COMPONENT__REQUIRED_CONTAINER, newRequiredContainer);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ProvidedHost getRequiredContainerVMHost() {
		return (ProvidedHost)eGet(DeploymentPackage.Literals.INTERNAL_COMPONENT__REQUIRED_CONTAINER_VM_HOST, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setRequiredContainerVMHost(ProvidedHost newRequiredContainerVMHost) {
		eSet(DeploymentPackage.Literals.INTERNAL_COMPONENT__REQUIRED_CONTAINER_VM_HOST, newRequiredContainerVMHost);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getVersion() {
		return (String)eGet(DeploymentPackage.Literals.INTERNAL_COMPONENT__VERSION, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setVersion(String newVersion) {
		eSet(DeploymentPackage.Literals.INTERNAL_COMPONENT__VERSION, newVersion);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getContextPath() {
		return (String)eGet(DeploymentPackage.Literals.INTERNAL_COMPONENT__CONTEXT_PATH, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setContextPath(String newContextPath) {
		eSet(DeploymentPackage.Literals.INTERNAL_COMPONENT__CONTEXT_PATH, newContextPath);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MUSAComponentType getType() {
		return (MUSAComponentType)eGet(DeploymentPackage.Literals.INTERNAL_COMPONENT__TYPE, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setType(MUSAComponentType newType) {
		eSet(DeploymentPackage.Literals.INTERNAL_COMPONENT__TYPE, newType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public int getOrder() {
		return (Integer)eGet(DeploymentPackage.Literals.INTERNAL_COMPONENT__ORDER, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setOrder(int newOrder) {
		eSet(DeploymentPackage.Literals.INTERNAL_COMPONENT__ORDER, newOrder);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isIPpublic() {
		return (Boolean)eGet(DeploymentPackage.Literals.INTERNAL_COMPONENT__IPPUBLIC, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setIPpublic(boolean newIPpublic) {
		eSet(DeploymentPackage.Literals.INTERNAL_COMPONENT__IPPUBLIC, newIPpublic);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public COTSAServiceType getTypeEnum() {
		return (COTSAServiceType)eGet(DeploymentPackage.Literals.INTERNAL_COMPONENT__TYPE_ENUM, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setTypeEnum(COTSAServiceType newTypeEnum) {
		eSet(DeploymentPackage.Literals.INTERNAL_COMPONENT__TYPE_ENUM, newTypeEnum);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	public EList<RequiredMUSACapability> getRequiredMUSACapabilities() {
		return (EList<RequiredMUSACapability>)eGet(DeploymentPackage.Literals.INTERNAL_COMPONENT__REQUIRED_MUSA_CAPABILITIES, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	public EList<ProvidedMUSACapability> getProvidedMUSACapabilities() {
		return (EList<ProvidedMUSACapability>)eGet(DeploymentPackage.Literals.INTERNAL_COMPONENT__PROVIDED_MUSA_CAPABILITIES, true);
	}

	/**
	 * The cached invocation delegate for the '{@link #contains(org.camel_dsl.deployment.InternalComponent, org.camel_dsl.deployment.InternalComponent) <em>Contains</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #contains(org.camel_dsl.deployment.InternalComponent, org.camel_dsl.deployment.InternalComponent)
	 * @generated
	 * @ordered
	 */
	protected static final EOperation.Internal.InvocationDelegate CONTAINS_INTERNAL_COMPONENT_INTERNAL_COMPONENT__EINVOCATION_DELEGATE = ((EOperation.Internal)DeploymentPackage.Literals.INTERNAL_COMPONENT___CONTAINS__INTERNALCOMPONENT_INTERNALCOMPONENT).getInvocationDelegate();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean contains(InternalComponent ic, InternalComponent rc) {
		try {
			return (Boolean)CONTAINS_INTERNAL_COMPONENT_INTERNAL_COMPONENT__EINVOCATION_DELEGATE.dynamicInvoke(this, new BasicEList.UnmodifiableEList<Object>(2, new Object[]{ic, rc}));
		}
		catch (InvocationTargetException ite) {
			throw new WrappedException(ite);
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eInvoke(int operationID, EList<?> arguments) throws InvocationTargetException {
		switch (operationID) {
			case DeploymentPackage.INTERNAL_COMPONENT___CONTAINS__INTERNALCOMPONENT_INTERNALCOMPONENT:
				return contains((InternalComponent)arguments.get(0), (InternalComponent)arguments.get(1));
		}
		return super.eInvoke(operationID, arguments);
	}

} //InternalComponentImpl
