/**
 */
package org.camel_dsl.deployment.impl;

import org.camel_dsl.deployment.Communication;
import org.camel_dsl.deployment.CommunicationType;
import org.camel_dsl.deployment.Configuration;
import org.camel_dsl.deployment.DeploymentPackage;
import org.camel_dsl.deployment.ProtocolType;
import org.camel_dsl.deployment.ProvidedCommunication;
import org.camel_dsl.deployment.RequiredCommunication;

import org.eclipse.emf.ecore.EClass;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Communication</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link org.camel_dsl.deployment.impl.CommunicationImpl#getType <em>Type</em>}</li>
 *   <li>{@link org.camel_dsl.deployment.impl.CommunicationImpl#getProvidedCommunication <em>Provided Communication</em>}</li>
 *   <li>{@link org.camel_dsl.deployment.impl.CommunicationImpl#getRequiredCommunication <em>Required Communication</em>}</li>
 *   <li>{@link org.camel_dsl.deployment.impl.CommunicationImpl#getProvidedPortConfiguration <em>Provided Port Configuration</em>}</li>
 *   <li>{@link org.camel_dsl.deployment.impl.CommunicationImpl#getRequiredPortConfiguration <em>Required Port Configuration</em>}</li>
 *   <li>{@link org.camel_dsl.deployment.impl.CommunicationImpl#getProtocol <em>Protocol</em>}</li>
 * </ul>
 *
 * @generated
 */
public class CommunicationImpl extends DeploymentElementImpl implements Communication {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected CommunicationImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return DeploymentPackage.Literals.COMMUNICATION;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public CommunicationType getType() {
		return (CommunicationType)eGet(DeploymentPackage.Literals.COMMUNICATION__TYPE, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setType(CommunicationType newType) {
		eSet(DeploymentPackage.Literals.COMMUNICATION__TYPE, newType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ProvidedCommunication getProvidedCommunication() {
		return (ProvidedCommunication)eGet(DeploymentPackage.Literals.COMMUNICATION__PROVIDED_COMMUNICATION, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setProvidedCommunication(ProvidedCommunication newProvidedCommunication) {
		eSet(DeploymentPackage.Literals.COMMUNICATION__PROVIDED_COMMUNICATION, newProvidedCommunication);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public RequiredCommunication getRequiredCommunication() {
		return (RequiredCommunication)eGet(DeploymentPackage.Literals.COMMUNICATION__REQUIRED_COMMUNICATION, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setRequiredCommunication(RequiredCommunication newRequiredCommunication) {
		eSet(DeploymentPackage.Literals.COMMUNICATION__REQUIRED_COMMUNICATION, newRequiredCommunication);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Configuration getProvidedPortConfiguration() {
		return (Configuration)eGet(DeploymentPackage.Literals.COMMUNICATION__PROVIDED_PORT_CONFIGURATION, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setProvidedPortConfiguration(Configuration newProvidedPortConfiguration) {
		eSet(DeploymentPackage.Literals.COMMUNICATION__PROVIDED_PORT_CONFIGURATION, newProvidedPortConfiguration);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Configuration getRequiredPortConfiguration() {
		return (Configuration)eGet(DeploymentPackage.Literals.COMMUNICATION__REQUIRED_PORT_CONFIGURATION, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setRequiredPortConfiguration(Configuration newRequiredPortConfiguration) {
		eSet(DeploymentPackage.Literals.COMMUNICATION__REQUIRED_PORT_CONFIGURATION, newRequiredPortConfiguration);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ProtocolType getProtocol() {
		return (ProtocolType)eGet(DeploymentPackage.Literals.COMMUNICATION__PROTOCOL, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setProtocol(ProtocolType newProtocol) {
		eSet(DeploymentPackage.Literals.COMMUNICATION__PROTOCOL, newProtocol);
	}

} //CommunicationImpl
