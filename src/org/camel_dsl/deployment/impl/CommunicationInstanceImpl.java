/**
 */
package org.camel_dsl.deployment.impl;

import org.camel_dsl.deployment.Communication;
import org.camel_dsl.deployment.CommunicationInstance;
import org.camel_dsl.deployment.DeploymentPackage;
import org.camel_dsl.deployment.ProvidedCommunicationInstance;
import org.camel_dsl.deployment.RequiredCommunicationInstance;

import org.eclipse.emf.ecore.EClass;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Communication Instance</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link org.camel_dsl.deployment.impl.CommunicationInstanceImpl#getType <em>Type</em>}</li>
 *   <li>{@link org.camel_dsl.deployment.impl.CommunicationInstanceImpl#getProvidedCommunicationInstance <em>Provided Communication Instance</em>}</li>
 *   <li>{@link org.camel_dsl.deployment.impl.CommunicationInstanceImpl#getRequiredCommunicationInstance <em>Required Communication Instance</em>}</li>
 * </ul>
 *
 * @generated
 */
public class CommunicationInstanceImpl extends DeploymentElementImpl implements CommunicationInstance {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected CommunicationInstanceImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return DeploymentPackage.Literals.COMMUNICATION_INSTANCE;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Communication getType() {
		return (Communication)eGet(DeploymentPackage.Literals.COMMUNICATION_INSTANCE__TYPE, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setType(Communication newType) {
		eSet(DeploymentPackage.Literals.COMMUNICATION_INSTANCE__TYPE, newType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ProvidedCommunicationInstance getProvidedCommunicationInstance() {
		return (ProvidedCommunicationInstance)eGet(DeploymentPackage.Literals.COMMUNICATION_INSTANCE__PROVIDED_COMMUNICATION_INSTANCE, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setProvidedCommunicationInstance(ProvidedCommunicationInstance newProvidedCommunicationInstance) {
		eSet(DeploymentPackage.Literals.COMMUNICATION_INSTANCE__PROVIDED_COMMUNICATION_INSTANCE, newProvidedCommunicationInstance);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public RequiredCommunicationInstance getRequiredCommunicationInstance() {
		return (RequiredCommunicationInstance)eGet(DeploymentPackage.Literals.COMMUNICATION_INSTANCE__REQUIRED_COMMUNICATION_INSTANCE, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setRequiredCommunicationInstance(RequiredCommunicationInstance newRequiredCommunicationInstance) {
		eSet(DeploymentPackage.Literals.COMMUNICATION_INSTANCE__REQUIRED_COMMUNICATION_INSTANCE, newRequiredCommunicationInstance);
	}

} //CommunicationInstanceImpl
