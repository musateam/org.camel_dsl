/**
 */
package org.camel_dsl.deployment.impl;

import org.camel_dsl.deployment.Component;
import org.camel_dsl.deployment.Configuration;
import org.camel_dsl.deployment.DeploymentPackage;
import org.camel_dsl.deployment.ProvidedCommunication;
import org.camel_dsl.deployment.ProvidedHost;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Component</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link org.camel_dsl.deployment.impl.ComponentImpl#getProvidedCommunications <em>Provided Communications</em>}</li>
 *   <li>{@link org.camel_dsl.deployment.impl.ComponentImpl#getProvidedHosts <em>Provided Hosts</em>}</li>
 *   <li>{@link org.camel_dsl.deployment.impl.ComponentImpl#getConfigurations <em>Configurations</em>}</li>
 * </ul>
 *
 * @generated
 */
public abstract class ComponentImpl extends DeploymentElementImpl implements Component {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ComponentImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return DeploymentPackage.Literals.COMPONENT;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	public EList<ProvidedCommunication> getProvidedCommunications() {
		return (EList<ProvidedCommunication>)eGet(DeploymentPackage.Literals.COMPONENT__PROVIDED_COMMUNICATIONS, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	public EList<ProvidedHost> getProvidedHosts() {
		return (EList<ProvidedHost>)eGet(DeploymentPackage.Literals.COMPONENT__PROVIDED_HOSTS, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	public EList<Configuration> getConfigurations() {
		return (EList<Configuration>)eGet(DeploymentPackage.Literals.COMPONENT__CONFIGURATIONS, true);
	}

} //ComponentImpl
