/**
 */
package org.camel_dsl.deployment.impl;

import org.camel_dsl.deployment.ConfiguratorCHEF;
import org.camel_dsl.deployment.DeploymentPackage;

import org.eclipse.emf.ecore.EClass;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Configurator CHEF</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link org.camel_dsl.deployment.impl.ConfiguratorCHEFImpl#getCookbook <em>Cookbook</em>}</li>
 *   <li>{@link org.camel_dsl.deployment.impl.ConfiguratorCHEFImpl#getRecipe <em>Recipe</em>}</li>
 * </ul>
 *
 * @generated
 */
public class ConfiguratorCHEFImpl extends DeploymentElementImpl implements ConfiguratorCHEF {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ConfiguratorCHEFImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return DeploymentPackage.Literals.CONFIGURATOR_CHEF;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getCookbook() {
		return (String)eGet(DeploymentPackage.Literals.CONFIGURATOR_CHEF__COOKBOOK, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setCookbook(String newCookbook) {
		eSet(DeploymentPackage.Literals.CONFIGURATOR_CHEF__COOKBOOK, newCookbook);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getRecipe() {
		return (String)eGet(DeploymentPackage.Literals.CONFIGURATOR_CHEF__RECIPE, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setRecipe(String newRecipe) {
		eSet(DeploymentPackage.Literals.CONFIGURATOR_CHEF__RECIPE, newRecipe);
	}

} //ConfiguratorCHEFImpl
