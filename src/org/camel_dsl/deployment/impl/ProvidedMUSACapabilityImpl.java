/**
 */
package org.camel_dsl.deployment.impl;

import org.camel_dsl.deployment.DeploymentPackage;
import org.camel_dsl.deployment.ProvidedMUSACapability;

import org.camel_dsl.security.MUSASecurityCapability;

import org.eclipse.emf.ecore.EClass;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Provided MUSA Capability</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link org.camel_dsl.deployment.impl.ProvidedMUSACapabilityImpl#getProvMUSAsecurityCapability <em>Prov MUS Asecurity Capability</em>}</li>
 * </ul>
 *
 * @generated
 */
public class ProvidedMUSACapabilityImpl extends DeploymentElementImpl implements ProvidedMUSACapability {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ProvidedMUSACapabilityImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return DeploymentPackage.Literals.PROVIDED_MUSA_CAPABILITY;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MUSASecurityCapability getProvMUSAsecurityCapability() {
		return (MUSASecurityCapability)eGet(DeploymentPackage.Literals.PROVIDED_MUSA_CAPABILITY__PROV_MUS_ASECURITY_CAPABILITY, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setProvMUSAsecurityCapability(MUSASecurityCapability newProvMUSAsecurityCapability) {
		eSet(DeploymentPackage.Literals.PROVIDED_MUSA_CAPABILITY__PROV_MUS_ASECURITY_CAPABILITY, newProvMUSAsecurityCapability);
	}

} //ProvidedMUSACapabilityImpl
