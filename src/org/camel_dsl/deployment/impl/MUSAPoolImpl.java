/**
 */
package org.camel_dsl.deployment.impl;

import org.camel_dsl.camel.MUSAPolicyType;
import org.camel_dsl.camel.MUSAPoolType;

import org.camel_dsl.deployment.DeploymentPackage;
import org.camel_dsl.deployment.MUSAPool;
import org.camel_dsl.deployment.MUSAPoolProvidedHost;
import org.camel_dsl.deployment.ProvidedHost;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>MUSA Pool</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link org.camel_dsl.deployment.impl.MUSAPoolImpl#getType <em>Type</em>}</li>
 *   <li>{@link org.camel_dsl.deployment.impl.MUSAPoolImpl#getPolicyEnum <em>Policy Enum</em>}</li>
 *   <li>{@link org.camel_dsl.deployment.impl.MUSAPoolImpl#getRequiredHosts <em>Required Hosts</em>}</li>
 *   <li>{@link org.camel_dsl.deployment.impl.MUSAPoolImpl#getProvidedHost <em>Provided Host</em>}</li>
 * </ul>
 *
 * @generated
 */
public class MUSAPoolImpl extends DeploymentElementImpl implements MUSAPool {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected MUSAPoolImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return DeploymentPackage.Literals.MUSA_POOL;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MUSAPoolType getType() {
		return (MUSAPoolType)eGet(DeploymentPackage.Literals.MUSA_POOL__TYPE, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setType(MUSAPoolType newType) {
		eSet(DeploymentPackage.Literals.MUSA_POOL__TYPE, newType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MUSAPolicyType getPolicyEnum() {
		return (MUSAPolicyType)eGet(DeploymentPackage.Literals.MUSA_POOL__POLICY_ENUM, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setPolicyEnum(MUSAPolicyType newPolicyEnum) {
		eSet(DeploymentPackage.Literals.MUSA_POOL__POLICY_ENUM, newPolicyEnum);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	public EList<ProvidedHost> getRequiredHosts() {
		return (EList<ProvidedHost>)eGet(DeploymentPackage.Literals.MUSA_POOL__REQUIRED_HOSTS, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MUSAPoolProvidedHost getProvidedHost() {
		return (MUSAPoolProvidedHost)eGet(DeploymentPackage.Literals.MUSA_POOL__PROVIDED_HOST, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setProvidedHost(MUSAPoolProvidedHost newProvidedHost) {
		eSet(DeploymentPackage.Literals.MUSA_POOL__PROVIDED_HOST, newProvidedHost);
	}

} //MUSAPoolImpl
