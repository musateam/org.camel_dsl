/**
 */
package org.camel_dsl.deployment.impl;

import org.camel_dsl.deployment.CapabilityLink;
import org.camel_dsl.deployment.DeploymentPackage;
import org.camel_dsl.deployment.ProvidedMUSACapability;
import org.camel_dsl.deployment.RequiredMUSACapability;

import org.eclipse.emf.ecore.EClass;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Capability Link</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link org.camel_dsl.deployment.impl.CapabilityLinkImpl#getRequiredMUSACapability <em>Required MUSA Capability</em>}</li>
 *   <li>{@link org.camel_dsl.deployment.impl.CapabilityLinkImpl#getProvidedMUSACapability <em>Provided MUSA Capability</em>}</li>
 * </ul>
 *
 * @generated
 */
public class CapabilityLinkImpl extends DeploymentElementImpl implements CapabilityLink {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected CapabilityLinkImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return DeploymentPackage.Literals.CAPABILITY_LINK;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public RequiredMUSACapability getRequiredMUSACapability() {
		return (RequiredMUSACapability)eGet(DeploymentPackage.Literals.CAPABILITY_LINK__REQUIRED_MUSA_CAPABILITY, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setRequiredMUSACapability(RequiredMUSACapability newRequiredMUSACapability) {
		eSet(DeploymentPackage.Literals.CAPABILITY_LINK__REQUIRED_MUSA_CAPABILITY, newRequiredMUSACapability);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ProvidedMUSACapability getProvidedMUSACapability() {
		return (ProvidedMUSACapability)eGet(DeploymentPackage.Literals.CAPABILITY_LINK__PROVIDED_MUSA_CAPABILITY, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setProvidedMUSACapability(ProvidedMUSACapability newProvidedMUSACapability) {
		eSet(DeploymentPackage.Literals.CAPABILITY_LINK__PROVIDED_MUSA_CAPABILITY, newProvidedMUSACapability);
	}

} //CapabilityLinkImpl
