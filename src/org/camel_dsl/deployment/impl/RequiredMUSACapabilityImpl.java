/**
 */
package org.camel_dsl.deployment.impl;

import org.camel_dsl.deployment.DeploymentPackage;
import org.camel_dsl.deployment.RequiredMUSACapability;

import org.camel_dsl.security.MUSASecurityCapability;

import org.eclipse.emf.ecore.EClass;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Required MUSA Capability</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link org.camel_dsl.deployment.impl.RequiredMUSACapabilityImpl#getReqMUSAsecurityCapability <em>Req MUS Asecurity Capability</em>}</li>
 * </ul>
 *
 * @generated
 */
public class RequiredMUSACapabilityImpl extends DeploymentElementImpl implements RequiredMUSACapability {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected RequiredMUSACapabilityImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return DeploymentPackage.Literals.REQUIRED_MUSA_CAPABILITY;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MUSASecurityCapability getReqMUSAsecurityCapability() {
		return (MUSASecurityCapability)eGet(DeploymentPackage.Literals.REQUIRED_MUSA_CAPABILITY__REQ_MUS_ASECURITY_CAPABILITY, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setReqMUSAsecurityCapability(MUSASecurityCapability newReqMUSAsecurityCapability) {
		eSet(DeploymentPackage.Literals.REQUIRED_MUSA_CAPABILITY__REQ_MUS_ASECURITY_CAPABILITY, newReqMUSAsecurityCapability);
	}

} //RequiredMUSACapabilityImpl
