/**
 */
package org.camel_dsl.deployment.impl;

import org.camel_dsl.camel.MUSAContainerType;
import org.camel_dsl.camel.MUSADockerType;

import org.camel_dsl.deployment.DeploymentPackage;
import org.camel_dsl.deployment.MUSAContainer;
import org.camel_dsl.deployment.MUSAPool;
import org.camel_dsl.deployment.ProvidedHost;

import org.eclipse.emf.ecore.EClass;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>MUSA Container</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link org.camel_dsl.deployment.impl.MUSAContainerImpl#getType <em>Type</em>}</li>
 *   <li>{@link org.camel_dsl.deployment.impl.MUSAContainerImpl#getAllocationStrategyEnum <em>Allocation Strategy Enum</em>}</li>
 *   <li>{@link org.camel_dsl.deployment.impl.MUSAContainerImpl#getRequiredPoolHost <em>Required Pool Host</em>}</li>
 *   <li>{@link org.camel_dsl.deployment.impl.MUSAContainerImpl#getRequiredPoolVMHost <em>Required Pool VM Host</em>}</li>
 * </ul>
 *
 * @generated
 */
public class MUSAContainerImpl extends ComponentImpl implements MUSAContainer {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected MUSAContainerImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return DeploymentPackage.Literals.MUSA_CONTAINER;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MUSAContainerType getType() {
		return (MUSAContainerType)eGet(DeploymentPackage.Literals.MUSA_CONTAINER__TYPE, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setType(MUSAContainerType newType) {
		eSet(DeploymentPackage.Literals.MUSA_CONTAINER__TYPE, newType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MUSADockerType getAllocationStrategyEnum() {
		return (MUSADockerType)eGet(DeploymentPackage.Literals.MUSA_CONTAINER__ALLOCATION_STRATEGY_ENUM, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setAllocationStrategyEnum(MUSADockerType newAllocationStrategyEnum) {
		eSet(DeploymentPackage.Literals.MUSA_CONTAINER__ALLOCATION_STRATEGY_ENUM, newAllocationStrategyEnum);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MUSAPool getRequiredPoolHost() {
		return (MUSAPool)eGet(DeploymentPackage.Literals.MUSA_CONTAINER__REQUIRED_POOL_HOST, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setRequiredPoolHost(MUSAPool newRequiredPoolHost) {
		eSet(DeploymentPackage.Literals.MUSA_CONTAINER__REQUIRED_POOL_HOST, newRequiredPoolHost);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ProvidedHost getRequiredPoolVMHost() {
		return (ProvidedHost)eGet(DeploymentPackage.Literals.MUSA_CONTAINER__REQUIRED_POOL_VM_HOST, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setRequiredPoolVMHost(ProvidedHost newRequiredPoolVMHost) {
		eSet(DeploymentPackage.Literals.MUSA_CONTAINER__REQUIRED_POOL_VM_HOST, newRequiredPoolVMHost);
	}

} //MUSAContainerImpl
