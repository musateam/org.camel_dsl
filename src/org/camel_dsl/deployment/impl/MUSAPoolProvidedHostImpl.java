/**
 */
package org.camel_dsl.deployment.impl;

import org.camel_dsl.deployment.DeploymentPackage;
import org.camel_dsl.deployment.MUSAPoolProvidedHost;

import org.eclipse.emf.ecore.EClass;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>MUSA Pool Provided Host</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class MUSAPoolProvidedHostImpl extends HostingPortImpl implements MUSAPoolProvidedHost {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected MUSAPoolProvidedHostImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return DeploymentPackage.Literals.MUSA_POOL_PROVIDED_HOST;
	}

} //MUSAPoolProvidedHostImpl
