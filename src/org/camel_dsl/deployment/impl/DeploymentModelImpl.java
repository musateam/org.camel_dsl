/**
 */
package org.camel_dsl.deployment.impl;

import org.camel_dsl.camel.impl.ModelImpl;

import org.camel_dsl.deployment.CapabilityLink;
import org.camel_dsl.deployment.Communication;
import org.camel_dsl.deployment.CommunicationInstance;
import org.camel_dsl.deployment.DeploymentModel;
import org.camel_dsl.deployment.DeploymentPackage;
import org.camel_dsl.deployment.Hosting;
import org.camel_dsl.deployment.HostingInstance;
import org.camel_dsl.deployment.InternalComponent;
import org.camel_dsl.deployment.InternalComponentInstance;
import org.camel_dsl.deployment.MUSAAgentComponent;
import org.camel_dsl.deployment.MUSAContainer;
import org.camel_dsl.deployment.MUSAPool;
import org.camel_dsl.deployment.VM;
import org.camel_dsl.deployment.VMInstance;
import org.camel_dsl.deployment.VMRequirementSet;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Model</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link org.camel_dsl.deployment.impl.DeploymentModelImpl#getPools <em>Pools</em>}</li>
 *   <li>{@link org.camel_dsl.deployment.impl.DeploymentModelImpl#getContainers <em>Containers</em>}</li>
 *   <li>{@link org.camel_dsl.deployment.impl.DeploymentModelImpl#getInternalComponents <em>Internal Components</em>}</li>
 *   <li>{@link org.camel_dsl.deployment.impl.DeploymentModelImpl#getInternalComponentInstances <em>Internal Component Instances</em>}</li>
 *   <li>{@link org.camel_dsl.deployment.impl.DeploymentModelImpl#getVms <em>Vms</em>}</li>
 *   <li>{@link org.camel_dsl.deployment.impl.DeploymentModelImpl#getVmInstances <em>Vm Instances</em>}</li>
 *   <li>{@link org.camel_dsl.deployment.impl.DeploymentModelImpl#getCommunications <em>Communications</em>}</li>
 *   <li>{@link org.camel_dsl.deployment.impl.DeploymentModelImpl#getCommunicationInstances <em>Communication Instances</em>}</li>
 *   <li>{@link org.camel_dsl.deployment.impl.DeploymentModelImpl#getHostings <em>Hostings</em>}</li>
 *   <li>{@link org.camel_dsl.deployment.impl.DeploymentModelImpl#getHostingInstances <em>Hosting Instances</em>}</li>
 *   <li>{@link org.camel_dsl.deployment.impl.DeploymentModelImpl#getVmRequirementSets <em>Vm Requirement Sets</em>}</li>
 *   <li>{@link org.camel_dsl.deployment.impl.DeploymentModelImpl#getGlobalVMRequirementSet <em>Global VM Requirement Set</em>}</li>
 *   <li>{@link org.camel_dsl.deployment.impl.DeploymentModelImpl#getMusaAgentComponents <em>Musa Agent Components</em>}</li>
 *   <li>{@link org.camel_dsl.deployment.impl.DeploymentModelImpl#getCapabilityLinks <em>Capability Links</em>}</li>
 * </ul>
 *
 * @generated
 */
public class DeploymentModelImpl extends ModelImpl implements DeploymentModel {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected DeploymentModelImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return DeploymentPackage.Literals.DEPLOYMENT_MODEL;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	public EList<MUSAPool> getPools() {
		return (EList<MUSAPool>)eGet(DeploymentPackage.Literals.DEPLOYMENT_MODEL__POOLS, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	public EList<MUSAContainer> getContainers() {
		return (EList<MUSAContainer>)eGet(DeploymentPackage.Literals.DEPLOYMENT_MODEL__CONTAINERS, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	public EList<InternalComponent> getInternalComponents() {
		return (EList<InternalComponent>)eGet(DeploymentPackage.Literals.DEPLOYMENT_MODEL__INTERNAL_COMPONENTS, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	public EList<InternalComponentInstance> getInternalComponentInstances() {
		return (EList<InternalComponentInstance>)eGet(DeploymentPackage.Literals.DEPLOYMENT_MODEL__INTERNAL_COMPONENT_INSTANCES, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	public EList<VM> getVms() {
		return (EList<VM>)eGet(DeploymentPackage.Literals.DEPLOYMENT_MODEL__VMS, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	public EList<VMInstance> getVmInstances() {
		return (EList<VMInstance>)eGet(DeploymentPackage.Literals.DEPLOYMENT_MODEL__VM_INSTANCES, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	public EList<Communication> getCommunications() {
		return (EList<Communication>)eGet(DeploymentPackage.Literals.DEPLOYMENT_MODEL__COMMUNICATIONS, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	public EList<CommunicationInstance> getCommunicationInstances() {
		return (EList<CommunicationInstance>)eGet(DeploymentPackage.Literals.DEPLOYMENT_MODEL__COMMUNICATION_INSTANCES, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	public EList<Hosting> getHostings() {
		return (EList<Hosting>)eGet(DeploymentPackage.Literals.DEPLOYMENT_MODEL__HOSTINGS, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	public EList<HostingInstance> getHostingInstances() {
		return (EList<HostingInstance>)eGet(DeploymentPackage.Literals.DEPLOYMENT_MODEL__HOSTING_INSTANCES, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	public EList<VMRequirementSet> getVmRequirementSets() {
		return (EList<VMRequirementSet>)eGet(DeploymentPackage.Literals.DEPLOYMENT_MODEL__VM_REQUIREMENT_SETS, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public VMRequirementSet getGlobalVMRequirementSet() {
		return (VMRequirementSet)eGet(DeploymentPackage.Literals.DEPLOYMENT_MODEL__GLOBAL_VM_REQUIREMENT_SET, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setGlobalVMRequirementSet(VMRequirementSet newGlobalVMRequirementSet) {
		eSet(DeploymentPackage.Literals.DEPLOYMENT_MODEL__GLOBAL_VM_REQUIREMENT_SET, newGlobalVMRequirementSet);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	public EList<MUSAAgentComponent> getMusaAgentComponents() {
		return (EList<MUSAAgentComponent>)eGet(DeploymentPackage.Literals.DEPLOYMENT_MODEL__MUSA_AGENT_COMPONENTS, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	public EList<CapabilityLink> getCapabilityLinks() {
		return (EList<CapabilityLink>)eGet(DeploymentPackage.Literals.DEPLOYMENT_MODEL__CAPABILITY_LINKS, true);
	}

} //DeploymentModelImpl
