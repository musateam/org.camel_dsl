/**
 */
package org.camel_dsl.deployment.impl;

import org.camel_dsl.deployment.DeploymentPackage;
import org.camel_dsl.deployment.RequiredHost;

import org.eclipse.emf.ecore.EClass;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Required Host</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class RequiredHostImpl extends HostingPortImpl implements RequiredHost {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected RequiredHostImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return DeploymentPackage.Literals.REQUIRED_HOST;
	}

} //RequiredHostImpl
