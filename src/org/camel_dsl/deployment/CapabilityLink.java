/**
 */
package org.camel_dsl.deployment;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Capability Link</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.camel_dsl.deployment.CapabilityLink#getRequiredMUSACapability <em>Required MUSA Capability</em>}</li>
 *   <li>{@link org.camel_dsl.deployment.CapabilityLink#getProvidedMUSACapability <em>Provided MUSA Capability</em>}</li>
 * </ul>
 *
 * @see org.camel_dsl.deployment.DeploymentPackage#getCapabilityLink()
 * @model
 * @generated
 */
public interface CapabilityLink extends DeploymentElement {
	/**
	 * Returns the value of the '<em><b>Required MUSA Capability</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Required MUSA Capability</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Required MUSA Capability</em>' reference.
	 * @see #setRequiredMUSACapability(RequiredMUSACapability)
	 * @see org.camel_dsl.deployment.DeploymentPackage#getCapabilityLink_RequiredMUSACapability()
	 * @model required="true"
	 * @generated
	 */
	RequiredMUSACapability getRequiredMUSACapability();

	/**
	 * Sets the value of the '{@link org.camel_dsl.deployment.CapabilityLink#getRequiredMUSACapability <em>Required MUSA Capability</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Required MUSA Capability</em>' reference.
	 * @see #getRequiredMUSACapability()
	 * @generated
	 */
	void setRequiredMUSACapability(RequiredMUSACapability value);

	/**
	 * Returns the value of the '<em><b>Provided MUSA Capability</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Provided MUSA Capability</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Provided MUSA Capability</em>' reference.
	 * @see #setProvidedMUSACapability(ProvidedMUSACapability)
	 * @see org.camel_dsl.deployment.DeploymentPackage#getCapabilityLink_ProvidedMUSACapability()
	 * @model required="true"
	 * @generated
	 */
	ProvidedMUSACapability getProvidedMUSACapability();

	/**
	 * Sets the value of the '{@link org.camel_dsl.deployment.CapabilityLink#getProvidedMUSACapability <em>Provided MUSA Capability</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Provided MUSA Capability</em>' reference.
	 * @see #getProvidedMUSACapability()
	 * @generated
	 */
	void setProvidedMUSACapability(ProvidedMUSACapability value);

} // CapabilityLink
