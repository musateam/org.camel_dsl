/**
 */
package org.camel_dsl.deployment;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Provided Communication Instance</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see org.camel_dsl.deployment.DeploymentPackage#getProvidedCommunicationInstance()
 * @model
 * @generated
 */
public interface ProvidedCommunicationInstance extends CommunicationPortInstance {
} // ProvidedCommunicationInstance
