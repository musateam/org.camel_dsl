/**
 */
package org.camel_dsl.deployment;

import org.camel_dsl.camel.MUSAPolicyType;
import org.camel_dsl.camel.MUSAPoolType;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>MUSA Pool</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.camel_dsl.deployment.MUSAPool#getType <em>Type</em>}</li>
 *   <li>{@link org.camel_dsl.deployment.MUSAPool#getPolicyEnum <em>Policy Enum</em>}</li>
 *   <li>{@link org.camel_dsl.deployment.MUSAPool#getRequiredHosts <em>Required Hosts</em>}</li>
 *   <li>{@link org.camel_dsl.deployment.MUSAPool#getProvidedHost <em>Provided Host</em>}</li>
 * </ul>
 *
 * @see org.camel_dsl.deployment.DeploymentPackage#getMUSAPool()
 * @model
 * @generated
 */
public interface MUSAPool extends DeploymentElement {
	/**
	 * Returns the value of the '<em><b>Type</b></em>' attribute.
	 * The literals are from the enumeration {@link org.camel_dsl.camel.MUSAPoolType}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Type</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Type</em>' attribute.
	 * @see org.camel_dsl.camel.MUSAPoolType
	 * @see #setType(MUSAPoolType)
	 * @see org.camel_dsl.deployment.DeploymentPackage#getMUSAPool_Type()
	 * @model
	 * @generated
	 */
	MUSAPoolType getType();

	/**
	 * Sets the value of the '{@link org.camel_dsl.deployment.MUSAPool#getType <em>Type</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Type</em>' attribute.
	 * @see org.camel_dsl.camel.MUSAPoolType
	 * @see #getType()
	 * @generated
	 */
	void setType(MUSAPoolType value);

	/**
	 * Returns the value of the '<em><b>Policy Enum</b></em>' attribute.
	 * The literals are from the enumeration {@link org.camel_dsl.camel.MUSAPolicyType}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Policy Enum</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Policy Enum</em>' attribute.
	 * @see org.camel_dsl.camel.MUSAPolicyType
	 * @see #setPolicyEnum(MUSAPolicyType)
	 * @see org.camel_dsl.deployment.DeploymentPackage#getMUSAPool_PolicyEnum()
	 * @model
	 * @generated
	 */
	MUSAPolicyType getPolicyEnum();

	/**
	 * Sets the value of the '{@link org.camel_dsl.deployment.MUSAPool#getPolicyEnum <em>Policy Enum</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Policy Enum</em>' attribute.
	 * @see org.camel_dsl.camel.MUSAPolicyType
	 * @see #getPolicyEnum()
	 * @generated
	 */
	void setPolicyEnum(MUSAPolicyType value);

	/**
	 * Returns the value of the '<em><b>Required Hosts</b></em>' reference list.
	 * The list contents are of type {@link org.camel_dsl.deployment.ProvidedHost}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Required Hosts</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Required Hosts</em>' reference list.
	 * @see org.camel_dsl.deployment.DeploymentPackage#getMUSAPool_RequiredHosts()
	 * @model required="true"
	 * @generated
	 */
	EList<ProvidedHost> getRequiredHosts();

	/**
	 * Returns the value of the '<em><b>Provided Host</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Provided Host</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Provided Host</em>' containment reference.
	 * @see #setProvidedHost(MUSAPoolProvidedHost)
	 * @see org.camel_dsl.deployment.DeploymentPackage#getMUSAPool_ProvidedHost()
	 * @model containment="true" required="true"
	 * @generated
	 */
	MUSAPoolProvidedHost getProvidedHost();

	/**
	 * Sets the value of the '{@link org.camel_dsl.deployment.MUSAPool#getProvidedHost <em>Provided Host</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Provided Host</em>' containment reference.
	 * @see #getProvidedHost()
	 * @generated
	 */
	void setProvidedHost(MUSAPoolProvidedHost value);

} // MUSAPool
