/**
 */
package org.camel_dsl.deployment;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Communication Port</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.camel_dsl.deployment.CommunicationPort#getPortNumber <em>Port Number</em>}</li>
 *   <li>{@link org.camel_dsl.deployment.CommunicationPort#getMinPortNumber <em>Min Port Number</em>}</li>
 *   <li>{@link org.camel_dsl.deployment.CommunicationPort#getMaxPortNumber <em>Max Port Number</em>}</li>
 *   <li>{@link org.camel_dsl.deployment.CommunicationPort#getContextPath <em>Context Path</em>}</li>
 * </ul>
 *
 * @see org.camel_dsl.deployment.DeploymentPackage#getCommunicationPort()
 * @model abstract="true"
 * @generated
 */
public interface CommunicationPort extends DeploymentElement {
	/**
	 * Returns the value of the '<em><b>Port Number</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Port Number</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Port Number</em>' attribute.
	 * @see #setPortNumber(int)
	 * @see org.camel_dsl.deployment.DeploymentPackage#getCommunicationPort_PortNumber()
	 * @model
	 * @generated
	 */
	int getPortNumber();

	/**
	 * Sets the value of the '{@link org.camel_dsl.deployment.CommunicationPort#getPortNumber <em>Port Number</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Port Number</em>' attribute.
	 * @see #getPortNumber()
	 * @generated
	 */
	void setPortNumber(int value);

	/**
	 * Returns the value of the '<em><b>Min Port Number</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Min Port Number</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Min Port Number</em>' attribute.
	 * @see #setMinPortNumber(int)
	 * @see org.camel_dsl.deployment.DeploymentPackage#getCommunicationPort_MinPortNumber()
	 * @model
	 * @generated
	 */
	int getMinPortNumber();

	/**
	 * Sets the value of the '{@link org.camel_dsl.deployment.CommunicationPort#getMinPortNumber <em>Min Port Number</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Min Port Number</em>' attribute.
	 * @see #getMinPortNumber()
	 * @generated
	 */
	void setMinPortNumber(int value);

	/**
	 * Returns the value of the '<em><b>Max Port Number</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Max Port Number</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Max Port Number</em>' attribute.
	 * @see #setMaxPortNumber(int)
	 * @see org.camel_dsl.deployment.DeploymentPackage#getCommunicationPort_MaxPortNumber()
	 * @model
	 * @generated
	 */
	int getMaxPortNumber();

	/**
	 * Sets the value of the '{@link org.camel_dsl.deployment.CommunicationPort#getMaxPortNumber <em>Max Port Number</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Max Port Number</em>' attribute.
	 * @see #getMaxPortNumber()
	 * @generated
	 */
	void setMaxPortNumber(int value);

	/**
	 * Returns the value of the '<em><b>Context Path</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Context Path</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Context Path</em>' attribute.
	 * @see #setContextPath(String)
	 * @see org.camel_dsl.deployment.DeploymentPackage#getCommunicationPort_ContextPath()
	 * @model
	 * @generated
	 */
	String getContextPath();

	/**
	 * Sets the value of the '{@link org.camel_dsl.deployment.CommunicationPort#getContextPath <em>Context Path</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Context Path</em>' attribute.
	 * @see #getContextPath()
	 * @generated
	 */
	void setContextPath(String value);

} // CommunicationPort
