/**
 */
package org.camel_dsl.deployment;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Configurator CHEF</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.camel_dsl.deployment.ConfiguratorCHEF#getCookbook <em>Cookbook</em>}</li>
 *   <li>{@link org.camel_dsl.deployment.ConfiguratorCHEF#getRecipe <em>Recipe</em>}</li>
 * </ul>
 *
 * @see org.camel_dsl.deployment.DeploymentPackage#getConfiguratorCHEF()
 * @model
 * @generated
 */
public interface ConfiguratorCHEF extends DeploymentElement {
	/**
	 * Returns the value of the '<em><b>Cookbook</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Cookbook</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Cookbook</em>' attribute.
	 * @see #setCookbook(String)
	 * @see org.camel_dsl.deployment.DeploymentPackage#getConfiguratorCHEF_Cookbook()
	 * @model
	 * @generated
	 */
	String getCookbook();

	/**
	 * Sets the value of the '{@link org.camel_dsl.deployment.ConfiguratorCHEF#getCookbook <em>Cookbook</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Cookbook</em>' attribute.
	 * @see #getCookbook()
	 * @generated
	 */
	void setCookbook(String value);

	/**
	 * Returns the value of the '<em><b>Recipe</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Recipe</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Recipe</em>' attribute.
	 * @see #setRecipe(String)
	 * @see org.camel_dsl.deployment.DeploymentPackage#getConfiguratorCHEF_Recipe()
	 * @model
	 * @generated
	 */
	String getRecipe();

	/**
	 * Sets the value of the '{@link org.camel_dsl.deployment.ConfiguratorCHEF#getRecipe <em>Recipe</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Recipe</em>' attribute.
	 * @see #getRecipe()
	 * @generated
	 */
	void setRecipe(String value);

} // ConfiguratorCHEF
