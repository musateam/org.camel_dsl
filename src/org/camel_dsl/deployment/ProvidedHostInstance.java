/**
 */
package org.camel_dsl.deployment;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Provided Host Instance</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see org.camel_dsl.deployment.DeploymentPackage#getProvidedHostInstance()
 * @model
 * @generated
 */
public interface ProvidedHostInstance extends HostingPortInstance {
} // ProvidedHostInstance
