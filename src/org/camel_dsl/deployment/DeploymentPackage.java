/**
 */
package org.camel_dsl.deployment;

import org.camel_dsl.camel.CamelPackage;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EEnum;
import org.eclipse.emf.ecore.EOperation;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

/**
 * <!-- begin-user-doc -->
 * The <b>Package</b> for the model.
 * It contains accessors for the meta objects to represent
 * <ul>
 *   <li>each class,</li>
 *   <li>each feature of each class,</li>
 *   <li>each operation of each class,</li>
 *   <li>each enum,</li>
 *   <li>and each data type</li>
 * </ul>
 * <!-- end-user-doc -->
 * @see org.camel_dsl.deployment.DeploymentFactory
 * @model kind="package"
 *        annotation="http://www.eclipse.org/emf/2002/Ecore invocationDelegates='http://www.eclipse.org/emf/2002/Ecore/OCL/Pivot' settingDelegates='http://www.eclipse.org/emf/2002/Ecore/OCL/Pivot' validationDelegates='http://www.eclipse.org/emf/2002/Ecore/OCL/Pivot'"
 * @generated
 */
public interface DeploymentPackage extends EPackage {
	/**
	 * The package name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNAME = "deployment";

	/**
	 * The package namespace URI.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_URI = "http://www.camel-dsl.eu/2017/03/camel/deployment";

	/**
	 * The package namespace name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_PREFIX = "deployment";

	/**
	 * The singleton instance of the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	DeploymentPackage eINSTANCE = org.camel_dsl.deployment.impl.DeploymentPackageImpl.init();

	/**
	 * The meta object id for the '{@link org.camel_dsl.deployment.impl.DeploymentElementImpl <em>Element</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.camel_dsl.deployment.impl.DeploymentElementImpl
	 * @see org.camel_dsl.deployment.impl.DeploymentPackageImpl#getDeploymentElement()
	 * @generated
	 */
	int DEPLOYMENT_ELEMENT = 0;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DEPLOYMENT_ELEMENT__NAME = 0;

	/**
	 * The number of structural features of the '<em>Element</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DEPLOYMENT_ELEMENT_FEATURE_COUNT = 1;

	/**
	 * The number of operations of the '<em>Element</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DEPLOYMENT_ELEMENT_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link org.camel_dsl.deployment.impl.DeploymentModelImpl <em>Model</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.camel_dsl.deployment.impl.DeploymentModelImpl
	 * @see org.camel_dsl.deployment.impl.DeploymentPackageImpl#getDeploymentModel()
	 * @generated
	 */
	int DEPLOYMENT_MODEL = 1;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DEPLOYMENT_MODEL__NAME = CamelPackage.MODEL__NAME;

	/**
	 * The feature id for the '<em><b>Import URI</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DEPLOYMENT_MODEL__IMPORT_URI = CamelPackage.MODEL__IMPORT_URI;

	/**
	 * The feature id for the '<em><b>Pools</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DEPLOYMENT_MODEL__POOLS = CamelPackage.MODEL_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Containers</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DEPLOYMENT_MODEL__CONTAINERS = CamelPackage.MODEL_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Internal Components</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DEPLOYMENT_MODEL__INTERNAL_COMPONENTS = CamelPackage.MODEL_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>Internal Component Instances</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DEPLOYMENT_MODEL__INTERNAL_COMPONENT_INSTANCES = CamelPackage.MODEL_FEATURE_COUNT + 3;

	/**
	 * The feature id for the '<em><b>Vms</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DEPLOYMENT_MODEL__VMS = CamelPackage.MODEL_FEATURE_COUNT + 4;

	/**
	 * The feature id for the '<em><b>Vm Instances</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DEPLOYMENT_MODEL__VM_INSTANCES = CamelPackage.MODEL_FEATURE_COUNT + 5;

	/**
	 * The feature id for the '<em><b>Communications</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DEPLOYMENT_MODEL__COMMUNICATIONS = CamelPackage.MODEL_FEATURE_COUNT + 6;

	/**
	 * The feature id for the '<em><b>Communication Instances</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DEPLOYMENT_MODEL__COMMUNICATION_INSTANCES = CamelPackage.MODEL_FEATURE_COUNT + 7;

	/**
	 * The feature id for the '<em><b>Hostings</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DEPLOYMENT_MODEL__HOSTINGS = CamelPackage.MODEL_FEATURE_COUNT + 8;

	/**
	 * The feature id for the '<em><b>Hosting Instances</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DEPLOYMENT_MODEL__HOSTING_INSTANCES = CamelPackage.MODEL_FEATURE_COUNT + 9;

	/**
	 * The feature id for the '<em><b>Vm Requirement Sets</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DEPLOYMENT_MODEL__VM_REQUIREMENT_SETS = CamelPackage.MODEL_FEATURE_COUNT + 10;

	/**
	 * The feature id for the '<em><b>Global VM Requirement Set</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DEPLOYMENT_MODEL__GLOBAL_VM_REQUIREMENT_SET = CamelPackage.MODEL_FEATURE_COUNT + 11;

	/**
	 * The feature id for the '<em><b>Musa Agent Components</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DEPLOYMENT_MODEL__MUSA_AGENT_COMPONENTS = CamelPackage.MODEL_FEATURE_COUNT + 12;

	/**
	 * The feature id for the '<em><b>Capability Links</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DEPLOYMENT_MODEL__CAPABILITY_LINKS = CamelPackage.MODEL_FEATURE_COUNT + 13;

	/**
	 * The number of structural features of the '<em>Model</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DEPLOYMENT_MODEL_FEATURE_COUNT = CamelPackage.MODEL_FEATURE_COUNT + 14;

	/**
	 * The number of operations of the '<em>Model</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DEPLOYMENT_MODEL_OPERATION_COUNT = CamelPackage.MODEL_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.camel_dsl.deployment.impl.ComponentImpl <em>Component</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.camel_dsl.deployment.impl.ComponentImpl
	 * @see org.camel_dsl.deployment.impl.DeploymentPackageImpl#getComponent()
	 * @generated
	 */
	int COMPONENT = 2;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPONENT__NAME = DEPLOYMENT_ELEMENT__NAME;

	/**
	 * The feature id for the '<em><b>Provided Communications</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPONENT__PROVIDED_COMMUNICATIONS = DEPLOYMENT_ELEMENT_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Provided Hosts</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPONENT__PROVIDED_HOSTS = DEPLOYMENT_ELEMENT_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Configurations</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPONENT__CONFIGURATIONS = DEPLOYMENT_ELEMENT_FEATURE_COUNT + 2;

	/**
	 * The number of structural features of the '<em>Component</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPONENT_FEATURE_COUNT = DEPLOYMENT_ELEMENT_FEATURE_COUNT + 3;

	/**
	 * The number of operations of the '<em>Component</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPONENT_OPERATION_COUNT = DEPLOYMENT_ELEMENT_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.camel_dsl.deployment.impl.InternalComponentImpl <em>Internal Component</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.camel_dsl.deployment.impl.InternalComponentImpl
	 * @see org.camel_dsl.deployment.impl.DeploymentPackageImpl#getInternalComponent()
	 * @generated
	 */
	int INTERNAL_COMPONENT = 3;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INTERNAL_COMPONENT__NAME = COMPONENT__NAME;

	/**
	 * The feature id for the '<em><b>Provided Communications</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INTERNAL_COMPONENT__PROVIDED_COMMUNICATIONS = COMPONENT__PROVIDED_COMMUNICATIONS;

	/**
	 * The feature id for the '<em><b>Provided Hosts</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INTERNAL_COMPONENT__PROVIDED_HOSTS = COMPONENT__PROVIDED_HOSTS;

	/**
	 * The feature id for the '<em><b>Configurations</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INTERNAL_COMPONENT__CONFIGURATIONS = COMPONENT__CONFIGURATIONS;

	/**
	 * The feature id for the '<em><b>Composite Internal Components</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INTERNAL_COMPONENT__COMPOSITE_INTERNAL_COMPONENTS = COMPONENT_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Required Communications</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INTERNAL_COMPONENT__REQUIRED_COMMUNICATIONS = COMPONENT_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Required Host</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INTERNAL_COMPONENT__REQUIRED_HOST = COMPONENT_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>Required Container</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INTERNAL_COMPONENT__REQUIRED_CONTAINER = COMPONENT_FEATURE_COUNT + 3;

	/**
	 * The feature id for the '<em><b>Required Container VM Host</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INTERNAL_COMPONENT__REQUIRED_CONTAINER_VM_HOST = COMPONENT_FEATURE_COUNT + 4;

	/**
	 * The feature id for the '<em><b>Version</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INTERNAL_COMPONENT__VERSION = COMPONENT_FEATURE_COUNT + 5;

	/**
	 * The feature id for the '<em><b>Context Path</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INTERNAL_COMPONENT__CONTEXT_PATH = COMPONENT_FEATURE_COUNT + 6;

	/**
	 * The feature id for the '<em><b>Type</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INTERNAL_COMPONENT__TYPE = COMPONENT_FEATURE_COUNT + 7;

	/**
	 * The feature id for the '<em><b>Order</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INTERNAL_COMPONENT__ORDER = COMPONENT_FEATURE_COUNT + 8;

	/**
	 * The feature id for the '<em><b>IPpublic</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INTERNAL_COMPONENT__IPPUBLIC = COMPONENT_FEATURE_COUNT + 9;

	/**
	 * The feature id for the '<em><b>Type Enum</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INTERNAL_COMPONENT__TYPE_ENUM = COMPONENT_FEATURE_COUNT + 10;

	/**
	 * The feature id for the '<em><b>Required MUSA Capabilities</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INTERNAL_COMPONENT__REQUIRED_MUSA_CAPABILITIES = COMPONENT_FEATURE_COUNT + 11;

	/**
	 * The feature id for the '<em><b>Provided MUSA Capabilities</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INTERNAL_COMPONENT__PROVIDED_MUSA_CAPABILITIES = COMPONENT_FEATURE_COUNT + 12;

	/**
	 * The number of structural features of the '<em>Internal Component</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INTERNAL_COMPONENT_FEATURE_COUNT = COMPONENT_FEATURE_COUNT + 13;

	/**
	 * The operation id for the '<em>Contains</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INTERNAL_COMPONENT___CONTAINS__INTERNALCOMPONENT_INTERNALCOMPONENT = COMPONENT_OPERATION_COUNT + 0;

	/**
	 * The number of operations of the '<em>Internal Component</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INTERNAL_COMPONENT_OPERATION_COUNT = COMPONENT_OPERATION_COUNT + 1;

	/**
	 * The meta object id for the '{@link org.camel_dsl.deployment.impl.VMImpl <em>VM</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.camel_dsl.deployment.impl.VMImpl
	 * @see org.camel_dsl.deployment.impl.DeploymentPackageImpl#getVM()
	 * @generated
	 */
	int VM = 4;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VM__NAME = COMPONENT__NAME;

	/**
	 * The feature id for the '<em><b>Provided Communications</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VM__PROVIDED_COMMUNICATIONS = COMPONENT__PROVIDED_COMMUNICATIONS;

	/**
	 * The feature id for the '<em><b>Provided Hosts</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VM__PROVIDED_HOSTS = COMPONENT__PROVIDED_HOSTS;

	/**
	 * The feature id for the '<em><b>Configurations</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VM__CONFIGURATIONS = COMPONENT__CONFIGURATIONS;

	/**
	 * The feature id for the '<em><b>Vm Requirement Set</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VM__VM_REQUIREMENT_SET = COMPONENT_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>VM</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VM_FEATURE_COUNT = COMPONENT_FEATURE_COUNT + 1;

	/**
	 * The number of operations of the '<em>VM</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VM_OPERATION_COUNT = COMPONENT_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.camel_dsl.deployment.impl.VMRequirementSetImpl <em>VM Requirement Set</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.camel_dsl.deployment.impl.VMRequirementSetImpl
	 * @see org.camel_dsl.deployment.impl.DeploymentPackageImpl#getVMRequirementSet()
	 * @generated
	 */
	int VM_REQUIREMENT_SET = 5;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VM_REQUIREMENT_SET__NAME = 0;

	/**
	 * The feature id for the '<em><b>Location Requirement</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VM_REQUIREMENT_SET__LOCATION_REQUIREMENT = 1;

	/**
	 * The feature id for the '<em><b>Provider Requirement</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VM_REQUIREMENT_SET__PROVIDER_REQUIREMENT = 2;

	/**
	 * The feature id for the '<em><b>Qualitative Hardware Requirement</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VM_REQUIREMENT_SET__QUALITATIVE_HARDWARE_REQUIREMENT = 3;

	/**
	 * The feature id for the '<em><b>Quantitative Hardware Requirement</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VM_REQUIREMENT_SET__QUANTITATIVE_HARDWARE_REQUIREMENT = 4;

	/**
	 * The feature id for the '<em><b>Os Or Image Requirement</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VM_REQUIREMENT_SET__OS_OR_IMAGE_REQUIREMENT = 5;

	/**
	 * The number of structural features of the '<em>VM Requirement Set</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VM_REQUIREMENT_SET_FEATURE_COUNT = 6;

	/**
	 * The number of operations of the '<em>VM Requirement Set</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VM_REQUIREMENT_SET_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link org.camel_dsl.deployment.impl.ConfigurationImpl <em>Configuration</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.camel_dsl.deployment.impl.ConfigurationImpl
	 * @see org.camel_dsl.deployment.impl.DeploymentPackageImpl#getConfiguration()
	 * @generated
	 */
	int CONFIGURATION = 6;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONFIGURATION__NAME = DEPLOYMENT_ELEMENT__NAME;

	/**
	 * The feature id for the '<em><b>Download Command</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONFIGURATION__DOWNLOAD_COMMAND = DEPLOYMENT_ELEMENT_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Upload Command</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONFIGURATION__UPLOAD_COMMAND = DEPLOYMENT_ELEMENT_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Install Command</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONFIGURATION__INSTALL_COMMAND = DEPLOYMENT_ELEMENT_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>Configure Command</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONFIGURATION__CONFIGURE_COMMAND = DEPLOYMENT_ELEMENT_FEATURE_COUNT + 3;

	/**
	 * The feature id for the '<em><b>Start Command</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONFIGURATION__START_COMMAND = DEPLOYMENT_ELEMENT_FEATURE_COUNT + 4;

	/**
	 * The feature id for the '<em><b>Stop Command</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONFIGURATION__STOP_COMMAND = DEPLOYMENT_ELEMENT_FEATURE_COUNT + 5;

	/**
	 * The feature id for the '<em><b>Conf Manager</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONFIGURATION__CONF_MANAGER = DEPLOYMENT_ELEMENT_FEATURE_COUNT + 6;

	/**
	 * The number of structural features of the '<em>Configuration</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONFIGURATION_FEATURE_COUNT = DEPLOYMENT_ELEMENT_FEATURE_COUNT + 7;

	/**
	 * The number of operations of the '<em>Configuration</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONFIGURATION_OPERATION_COUNT = DEPLOYMENT_ELEMENT_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.camel_dsl.deployment.impl.CommunicationImpl <em>Communication</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.camel_dsl.deployment.impl.CommunicationImpl
	 * @see org.camel_dsl.deployment.impl.DeploymentPackageImpl#getCommunication()
	 * @generated
	 */
	int COMMUNICATION = 7;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMMUNICATION__NAME = DEPLOYMENT_ELEMENT__NAME;

	/**
	 * The feature id for the '<em><b>Type</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMMUNICATION__TYPE = DEPLOYMENT_ELEMENT_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Provided Communication</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMMUNICATION__PROVIDED_COMMUNICATION = DEPLOYMENT_ELEMENT_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Required Communication</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMMUNICATION__REQUIRED_COMMUNICATION = DEPLOYMENT_ELEMENT_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>Provided Port Configuration</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMMUNICATION__PROVIDED_PORT_CONFIGURATION = DEPLOYMENT_ELEMENT_FEATURE_COUNT + 3;

	/**
	 * The feature id for the '<em><b>Required Port Configuration</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMMUNICATION__REQUIRED_PORT_CONFIGURATION = DEPLOYMENT_ELEMENT_FEATURE_COUNT + 4;

	/**
	 * The feature id for the '<em><b>Protocol</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMMUNICATION__PROTOCOL = DEPLOYMENT_ELEMENT_FEATURE_COUNT + 5;

	/**
	 * The number of structural features of the '<em>Communication</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMMUNICATION_FEATURE_COUNT = DEPLOYMENT_ELEMENT_FEATURE_COUNT + 6;

	/**
	 * The number of operations of the '<em>Communication</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMMUNICATION_OPERATION_COUNT = DEPLOYMENT_ELEMENT_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.camel_dsl.deployment.impl.CommunicationPortImpl <em>Communication Port</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.camel_dsl.deployment.impl.CommunicationPortImpl
	 * @see org.camel_dsl.deployment.impl.DeploymentPackageImpl#getCommunicationPort()
	 * @generated
	 */
	int COMMUNICATION_PORT = 8;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMMUNICATION_PORT__NAME = DEPLOYMENT_ELEMENT__NAME;

	/**
	 * The feature id for the '<em><b>Port Number</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMMUNICATION_PORT__PORT_NUMBER = DEPLOYMENT_ELEMENT_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Min Port Number</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMMUNICATION_PORT__MIN_PORT_NUMBER = DEPLOYMENT_ELEMENT_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Max Port Number</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMMUNICATION_PORT__MAX_PORT_NUMBER = DEPLOYMENT_ELEMENT_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>Context Path</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMMUNICATION_PORT__CONTEXT_PATH = DEPLOYMENT_ELEMENT_FEATURE_COUNT + 3;

	/**
	 * The number of structural features of the '<em>Communication Port</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMMUNICATION_PORT_FEATURE_COUNT = DEPLOYMENT_ELEMENT_FEATURE_COUNT + 4;

	/**
	 * The number of operations of the '<em>Communication Port</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMMUNICATION_PORT_OPERATION_COUNT = DEPLOYMENT_ELEMENT_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.camel_dsl.deployment.impl.ProvidedCommunicationImpl <em>Provided Communication</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.camel_dsl.deployment.impl.ProvidedCommunicationImpl
	 * @see org.camel_dsl.deployment.impl.DeploymentPackageImpl#getProvidedCommunication()
	 * @generated
	 */
	int PROVIDED_COMMUNICATION = 9;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PROVIDED_COMMUNICATION__NAME = COMMUNICATION_PORT__NAME;

	/**
	 * The feature id for the '<em><b>Port Number</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PROVIDED_COMMUNICATION__PORT_NUMBER = COMMUNICATION_PORT__PORT_NUMBER;

	/**
	 * The feature id for the '<em><b>Min Port Number</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PROVIDED_COMMUNICATION__MIN_PORT_NUMBER = COMMUNICATION_PORT__MIN_PORT_NUMBER;

	/**
	 * The feature id for the '<em><b>Max Port Number</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PROVIDED_COMMUNICATION__MAX_PORT_NUMBER = COMMUNICATION_PORT__MAX_PORT_NUMBER;

	/**
	 * The feature id for the '<em><b>Context Path</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PROVIDED_COMMUNICATION__CONTEXT_PATH = COMMUNICATION_PORT__CONTEXT_PATH;

	/**
	 * The number of structural features of the '<em>Provided Communication</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PROVIDED_COMMUNICATION_FEATURE_COUNT = COMMUNICATION_PORT_FEATURE_COUNT + 0;

	/**
	 * The number of operations of the '<em>Provided Communication</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PROVIDED_COMMUNICATION_OPERATION_COUNT = COMMUNICATION_PORT_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.camel_dsl.deployment.impl.RequiredCommunicationImpl <em>Required Communication</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.camel_dsl.deployment.impl.RequiredCommunicationImpl
	 * @see org.camel_dsl.deployment.impl.DeploymentPackageImpl#getRequiredCommunication()
	 * @generated
	 */
	int REQUIRED_COMMUNICATION = 10;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REQUIRED_COMMUNICATION__NAME = COMMUNICATION_PORT__NAME;

	/**
	 * The feature id for the '<em><b>Port Number</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REQUIRED_COMMUNICATION__PORT_NUMBER = COMMUNICATION_PORT__PORT_NUMBER;

	/**
	 * The feature id for the '<em><b>Min Port Number</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REQUIRED_COMMUNICATION__MIN_PORT_NUMBER = COMMUNICATION_PORT__MIN_PORT_NUMBER;

	/**
	 * The feature id for the '<em><b>Max Port Number</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REQUIRED_COMMUNICATION__MAX_PORT_NUMBER = COMMUNICATION_PORT__MAX_PORT_NUMBER;

	/**
	 * The feature id for the '<em><b>Context Path</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REQUIRED_COMMUNICATION__CONTEXT_PATH = COMMUNICATION_PORT__CONTEXT_PATH;

	/**
	 * The feature id for the '<em><b>Is Mandatory</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REQUIRED_COMMUNICATION__IS_MANDATORY = COMMUNICATION_PORT_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Required Communication</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REQUIRED_COMMUNICATION_FEATURE_COUNT = COMMUNICATION_PORT_FEATURE_COUNT + 1;

	/**
	 * The number of operations of the '<em>Required Communication</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REQUIRED_COMMUNICATION_OPERATION_COUNT = COMMUNICATION_PORT_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.camel_dsl.deployment.impl.HostingImpl <em>Hosting</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.camel_dsl.deployment.impl.HostingImpl
	 * @see org.camel_dsl.deployment.impl.DeploymentPackageImpl#getHosting()
	 * @generated
	 */
	int HOSTING = 11;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HOSTING__NAME = DEPLOYMENT_ELEMENT__NAME;

	/**
	 * The feature id for the '<em><b>Pool Provided Host</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HOSTING__POOL_PROVIDED_HOST = DEPLOYMENT_ELEMENT_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Provided Host</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HOSTING__PROVIDED_HOST = DEPLOYMENT_ELEMENT_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Required Host</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HOSTING__REQUIRED_HOST = DEPLOYMENT_ELEMENT_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>Provided Host Configuration</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HOSTING__PROVIDED_HOST_CONFIGURATION = DEPLOYMENT_ELEMENT_FEATURE_COUNT + 3;

	/**
	 * The feature id for the '<em><b>Required Host Configuration</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HOSTING__REQUIRED_HOST_CONFIGURATION = DEPLOYMENT_ELEMENT_FEATURE_COUNT + 4;

	/**
	 * The number of structural features of the '<em>Hosting</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HOSTING_FEATURE_COUNT = DEPLOYMENT_ELEMENT_FEATURE_COUNT + 5;

	/**
	 * The number of operations of the '<em>Hosting</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HOSTING_OPERATION_COUNT = DEPLOYMENT_ELEMENT_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.camel_dsl.deployment.impl.HostingPortImpl <em>Hosting Port</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.camel_dsl.deployment.impl.HostingPortImpl
	 * @see org.camel_dsl.deployment.impl.DeploymentPackageImpl#getHostingPort()
	 * @generated
	 */
	int HOSTING_PORT = 12;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HOSTING_PORT__NAME = DEPLOYMENT_ELEMENT__NAME;

	/**
	 * The number of structural features of the '<em>Hosting Port</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HOSTING_PORT_FEATURE_COUNT = DEPLOYMENT_ELEMENT_FEATURE_COUNT + 0;

	/**
	 * The number of operations of the '<em>Hosting Port</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HOSTING_PORT_OPERATION_COUNT = DEPLOYMENT_ELEMENT_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.camel_dsl.deployment.impl.ProvidedHostImpl <em>Provided Host</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.camel_dsl.deployment.impl.ProvidedHostImpl
	 * @see org.camel_dsl.deployment.impl.DeploymentPackageImpl#getProvidedHost()
	 * @generated
	 */
	int PROVIDED_HOST = 13;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PROVIDED_HOST__NAME = HOSTING_PORT__NAME;

	/**
	 * The number of structural features of the '<em>Provided Host</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PROVIDED_HOST_FEATURE_COUNT = HOSTING_PORT_FEATURE_COUNT + 0;

	/**
	 * The number of operations of the '<em>Provided Host</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PROVIDED_HOST_OPERATION_COUNT = HOSTING_PORT_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.camel_dsl.deployment.impl.MUSAPoolProvidedHostImpl <em>MUSA Pool Provided Host</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.camel_dsl.deployment.impl.MUSAPoolProvidedHostImpl
	 * @see org.camel_dsl.deployment.impl.DeploymentPackageImpl#getMUSAPoolProvidedHost()
	 * @generated
	 */
	int MUSA_POOL_PROVIDED_HOST = 14;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MUSA_POOL_PROVIDED_HOST__NAME = HOSTING_PORT__NAME;

	/**
	 * The number of structural features of the '<em>MUSA Pool Provided Host</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MUSA_POOL_PROVIDED_HOST_FEATURE_COUNT = HOSTING_PORT_FEATURE_COUNT + 0;

	/**
	 * The number of operations of the '<em>MUSA Pool Provided Host</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MUSA_POOL_PROVIDED_HOST_OPERATION_COUNT = HOSTING_PORT_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.camel_dsl.deployment.impl.RequiredHostImpl <em>Required Host</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.camel_dsl.deployment.impl.RequiredHostImpl
	 * @see org.camel_dsl.deployment.impl.DeploymentPackageImpl#getRequiredHost()
	 * @generated
	 */
	int REQUIRED_HOST = 15;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REQUIRED_HOST__NAME = HOSTING_PORT__NAME;

	/**
	 * The number of structural features of the '<em>Required Host</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REQUIRED_HOST_FEATURE_COUNT = HOSTING_PORT_FEATURE_COUNT + 0;

	/**
	 * The number of operations of the '<em>Required Host</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REQUIRED_HOST_OPERATION_COUNT = HOSTING_PORT_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.camel_dsl.deployment.impl.ComponentInstanceImpl <em>Component Instance</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.camel_dsl.deployment.impl.ComponentInstanceImpl
	 * @see org.camel_dsl.deployment.impl.DeploymentPackageImpl#getComponentInstance()
	 * @generated
	 */
	int COMPONENT_INSTANCE = 16;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPONENT_INSTANCE__NAME = DEPLOYMENT_ELEMENT__NAME;

	/**
	 * The feature id for the '<em><b>Type</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPONENT_INSTANCE__TYPE = DEPLOYMENT_ELEMENT_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Provided Communication Instances</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPONENT_INSTANCE__PROVIDED_COMMUNICATION_INSTANCES = DEPLOYMENT_ELEMENT_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Provided Host Instances</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPONENT_INSTANCE__PROVIDED_HOST_INSTANCES = DEPLOYMENT_ELEMENT_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>Instantiated On</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPONENT_INSTANCE__INSTANTIATED_ON = DEPLOYMENT_ELEMENT_FEATURE_COUNT + 3;

	/**
	 * The feature id for the '<em><b>Destroyed On</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPONENT_INSTANCE__DESTROYED_ON = DEPLOYMENT_ELEMENT_FEATURE_COUNT + 4;

	/**
	 * The number of structural features of the '<em>Component Instance</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPONENT_INSTANCE_FEATURE_COUNT = DEPLOYMENT_ELEMENT_FEATURE_COUNT + 5;

	/**
	 * The number of operations of the '<em>Component Instance</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPONENT_INSTANCE_OPERATION_COUNT = DEPLOYMENT_ELEMENT_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.camel_dsl.deployment.impl.InternalComponentInstanceImpl <em>Internal Component Instance</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.camel_dsl.deployment.impl.InternalComponentInstanceImpl
	 * @see org.camel_dsl.deployment.impl.DeploymentPackageImpl#getInternalComponentInstance()
	 * @generated
	 */
	int INTERNAL_COMPONENT_INSTANCE = 17;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INTERNAL_COMPONENT_INSTANCE__NAME = COMPONENT_INSTANCE__NAME;

	/**
	 * The feature id for the '<em><b>Type</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INTERNAL_COMPONENT_INSTANCE__TYPE = COMPONENT_INSTANCE__TYPE;

	/**
	 * The feature id for the '<em><b>Provided Communication Instances</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INTERNAL_COMPONENT_INSTANCE__PROVIDED_COMMUNICATION_INSTANCES = COMPONENT_INSTANCE__PROVIDED_COMMUNICATION_INSTANCES;

	/**
	 * The feature id for the '<em><b>Provided Host Instances</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INTERNAL_COMPONENT_INSTANCE__PROVIDED_HOST_INSTANCES = COMPONENT_INSTANCE__PROVIDED_HOST_INSTANCES;

	/**
	 * The feature id for the '<em><b>Instantiated On</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INTERNAL_COMPONENT_INSTANCE__INSTANTIATED_ON = COMPONENT_INSTANCE__INSTANTIATED_ON;

	/**
	 * The feature id for the '<em><b>Destroyed On</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INTERNAL_COMPONENT_INSTANCE__DESTROYED_ON = COMPONENT_INSTANCE__DESTROYED_ON;

	/**
	 * The feature id for the '<em><b>Required Communication Instances</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INTERNAL_COMPONENT_INSTANCE__REQUIRED_COMMUNICATION_INSTANCES = COMPONENT_INSTANCE_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Required Host Instance</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INTERNAL_COMPONENT_INSTANCE__REQUIRED_HOST_INSTANCE = COMPONENT_INSTANCE_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>Internal Component Instance</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INTERNAL_COMPONENT_INSTANCE_FEATURE_COUNT = COMPONENT_INSTANCE_FEATURE_COUNT + 2;

	/**
	 * The number of operations of the '<em>Internal Component Instance</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INTERNAL_COMPONENT_INSTANCE_OPERATION_COUNT = COMPONENT_INSTANCE_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.camel_dsl.deployment.impl.VMInstanceImpl <em>VM Instance</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.camel_dsl.deployment.impl.VMInstanceImpl
	 * @see org.camel_dsl.deployment.impl.DeploymentPackageImpl#getVMInstance()
	 * @generated
	 */
	int VM_INSTANCE = 18;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VM_INSTANCE__NAME = COMPONENT_INSTANCE__NAME;

	/**
	 * The feature id for the '<em><b>Type</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VM_INSTANCE__TYPE = COMPONENT_INSTANCE__TYPE;

	/**
	 * The feature id for the '<em><b>Provided Communication Instances</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VM_INSTANCE__PROVIDED_COMMUNICATION_INSTANCES = COMPONENT_INSTANCE__PROVIDED_COMMUNICATION_INSTANCES;

	/**
	 * The feature id for the '<em><b>Provided Host Instances</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VM_INSTANCE__PROVIDED_HOST_INSTANCES = COMPONENT_INSTANCE__PROVIDED_HOST_INSTANCES;

	/**
	 * The feature id for the '<em><b>Instantiated On</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VM_INSTANCE__INSTANTIATED_ON = COMPONENT_INSTANCE__INSTANTIATED_ON;

	/**
	 * The feature id for the '<em><b>Destroyed On</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VM_INSTANCE__DESTROYED_ON = COMPONENT_INSTANCE__DESTROYED_ON;

	/**
	 * The feature id for the '<em><b>Vm Type</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VM_INSTANCE__VM_TYPE = COMPONENT_INSTANCE_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Vm Type Value</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VM_INSTANCE__VM_TYPE_VALUE = COMPONENT_INSTANCE_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Ip</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VM_INSTANCE__IP = COMPONENT_INSTANCE_FEATURE_COUNT + 2;

	/**
	 * The number of structural features of the '<em>VM Instance</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VM_INSTANCE_FEATURE_COUNT = COMPONENT_INSTANCE_FEATURE_COUNT + 3;

	/**
	 * The operation id for the '<em>Check Dates</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VM_INSTANCE___CHECK_DATES__VMINSTANCE = COMPONENT_INSTANCE_OPERATION_COUNT + 0;

	/**
	 * The number of operations of the '<em>VM Instance</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VM_INSTANCE_OPERATION_COUNT = COMPONENT_INSTANCE_OPERATION_COUNT + 1;

	/**
	 * The meta object id for the '{@link org.camel_dsl.deployment.impl.CommunicationInstanceImpl <em>Communication Instance</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.camel_dsl.deployment.impl.CommunicationInstanceImpl
	 * @see org.camel_dsl.deployment.impl.DeploymentPackageImpl#getCommunicationInstance()
	 * @generated
	 */
	int COMMUNICATION_INSTANCE = 19;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMMUNICATION_INSTANCE__NAME = DEPLOYMENT_ELEMENT__NAME;

	/**
	 * The feature id for the '<em><b>Type</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMMUNICATION_INSTANCE__TYPE = DEPLOYMENT_ELEMENT_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Provided Communication Instance</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMMUNICATION_INSTANCE__PROVIDED_COMMUNICATION_INSTANCE = DEPLOYMENT_ELEMENT_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Required Communication Instance</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMMUNICATION_INSTANCE__REQUIRED_COMMUNICATION_INSTANCE = DEPLOYMENT_ELEMENT_FEATURE_COUNT + 2;

	/**
	 * The number of structural features of the '<em>Communication Instance</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMMUNICATION_INSTANCE_FEATURE_COUNT = DEPLOYMENT_ELEMENT_FEATURE_COUNT + 3;

	/**
	 * The number of operations of the '<em>Communication Instance</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMMUNICATION_INSTANCE_OPERATION_COUNT = DEPLOYMENT_ELEMENT_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.camel_dsl.deployment.impl.CommunicationPortInstanceImpl <em>Communication Port Instance</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.camel_dsl.deployment.impl.CommunicationPortInstanceImpl
	 * @see org.camel_dsl.deployment.impl.DeploymentPackageImpl#getCommunicationPortInstance()
	 * @generated
	 */
	int COMMUNICATION_PORT_INSTANCE = 20;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMMUNICATION_PORT_INSTANCE__NAME = DEPLOYMENT_ELEMENT__NAME;

	/**
	 * The feature id for the '<em><b>Type</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMMUNICATION_PORT_INSTANCE__TYPE = DEPLOYMENT_ELEMENT_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Communication Port Instance</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMMUNICATION_PORT_INSTANCE_FEATURE_COUNT = DEPLOYMENT_ELEMENT_FEATURE_COUNT + 1;

	/**
	 * The number of operations of the '<em>Communication Port Instance</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMMUNICATION_PORT_INSTANCE_OPERATION_COUNT = DEPLOYMENT_ELEMENT_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.camel_dsl.deployment.impl.ProvidedCommunicationInstanceImpl <em>Provided Communication Instance</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.camel_dsl.deployment.impl.ProvidedCommunicationInstanceImpl
	 * @see org.camel_dsl.deployment.impl.DeploymentPackageImpl#getProvidedCommunicationInstance()
	 * @generated
	 */
	int PROVIDED_COMMUNICATION_INSTANCE = 21;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PROVIDED_COMMUNICATION_INSTANCE__NAME = COMMUNICATION_PORT_INSTANCE__NAME;

	/**
	 * The feature id for the '<em><b>Type</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PROVIDED_COMMUNICATION_INSTANCE__TYPE = COMMUNICATION_PORT_INSTANCE__TYPE;

	/**
	 * The number of structural features of the '<em>Provided Communication Instance</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PROVIDED_COMMUNICATION_INSTANCE_FEATURE_COUNT = COMMUNICATION_PORT_INSTANCE_FEATURE_COUNT + 0;

	/**
	 * The number of operations of the '<em>Provided Communication Instance</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PROVIDED_COMMUNICATION_INSTANCE_OPERATION_COUNT = COMMUNICATION_PORT_INSTANCE_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.camel_dsl.deployment.impl.RequiredCommunicationInstanceImpl <em>Required Communication Instance</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.camel_dsl.deployment.impl.RequiredCommunicationInstanceImpl
	 * @see org.camel_dsl.deployment.impl.DeploymentPackageImpl#getRequiredCommunicationInstance()
	 * @generated
	 */
	int REQUIRED_COMMUNICATION_INSTANCE = 22;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REQUIRED_COMMUNICATION_INSTANCE__NAME = COMMUNICATION_PORT_INSTANCE__NAME;

	/**
	 * The feature id for the '<em><b>Type</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REQUIRED_COMMUNICATION_INSTANCE__TYPE = COMMUNICATION_PORT_INSTANCE__TYPE;

	/**
	 * The number of structural features of the '<em>Required Communication Instance</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REQUIRED_COMMUNICATION_INSTANCE_FEATURE_COUNT = COMMUNICATION_PORT_INSTANCE_FEATURE_COUNT + 0;

	/**
	 * The number of operations of the '<em>Required Communication Instance</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REQUIRED_COMMUNICATION_INSTANCE_OPERATION_COUNT = COMMUNICATION_PORT_INSTANCE_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.camel_dsl.deployment.impl.HostingInstanceImpl <em>Hosting Instance</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.camel_dsl.deployment.impl.HostingInstanceImpl
	 * @see org.camel_dsl.deployment.impl.DeploymentPackageImpl#getHostingInstance()
	 * @generated
	 */
	int HOSTING_INSTANCE = 23;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HOSTING_INSTANCE__NAME = DEPLOYMENT_ELEMENT__NAME;

	/**
	 * The feature id for the '<em><b>Type</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HOSTING_INSTANCE__TYPE = DEPLOYMENT_ELEMENT_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Provided Host Instance</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HOSTING_INSTANCE__PROVIDED_HOST_INSTANCE = DEPLOYMENT_ELEMENT_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Required Host Instance</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HOSTING_INSTANCE__REQUIRED_HOST_INSTANCE = DEPLOYMENT_ELEMENT_FEATURE_COUNT + 2;

	/**
	 * The number of structural features of the '<em>Hosting Instance</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HOSTING_INSTANCE_FEATURE_COUNT = DEPLOYMENT_ELEMENT_FEATURE_COUNT + 3;

	/**
	 * The number of operations of the '<em>Hosting Instance</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HOSTING_INSTANCE_OPERATION_COUNT = DEPLOYMENT_ELEMENT_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.camel_dsl.deployment.impl.HostingPortInstanceImpl <em>Hosting Port Instance</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.camel_dsl.deployment.impl.HostingPortInstanceImpl
	 * @see org.camel_dsl.deployment.impl.DeploymentPackageImpl#getHostingPortInstance()
	 * @generated
	 */
	int HOSTING_PORT_INSTANCE = 24;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HOSTING_PORT_INSTANCE__NAME = DEPLOYMENT_ELEMENT__NAME;

	/**
	 * The feature id for the '<em><b>Type</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HOSTING_PORT_INSTANCE__TYPE = DEPLOYMENT_ELEMENT_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Hosting Port Instance</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HOSTING_PORT_INSTANCE_FEATURE_COUNT = DEPLOYMENT_ELEMENT_FEATURE_COUNT + 1;

	/**
	 * The number of operations of the '<em>Hosting Port Instance</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HOSTING_PORT_INSTANCE_OPERATION_COUNT = DEPLOYMENT_ELEMENT_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.camel_dsl.deployment.impl.ProvidedHostInstanceImpl <em>Provided Host Instance</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.camel_dsl.deployment.impl.ProvidedHostInstanceImpl
	 * @see org.camel_dsl.deployment.impl.DeploymentPackageImpl#getProvidedHostInstance()
	 * @generated
	 */
	int PROVIDED_HOST_INSTANCE = 25;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PROVIDED_HOST_INSTANCE__NAME = HOSTING_PORT_INSTANCE__NAME;

	/**
	 * The feature id for the '<em><b>Type</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PROVIDED_HOST_INSTANCE__TYPE = HOSTING_PORT_INSTANCE__TYPE;

	/**
	 * The number of structural features of the '<em>Provided Host Instance</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PROVIDED_HOST_INSTANCE_FEATURE_COUNT = HOSTING_PORT_INSTANCE_FEATURE_COUNT + 0;

	/**
	 * The number of operations of the '<em>Provided Host Instance</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PROVIDED_HOST_INSTANCE_OPERATION_COUNT = HOSTING_PORT_INSTANCE_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.camel_dsl.deployment.impl.RequiredHostInstanceImpl <em>Required Host Instance</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.camel_dsl.deployment.impl.RequiredHostInstanceImpl
	 * @see org.camel_dsl.deployment.impl.DeploymentPackageImpl#getRequiredHostInstance()
	 * @generated
	 */
	int REQUIRED_HOST_INSTANCE = 26;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REQUIRED_HOST_INSTANCE__NAME = HOSTING_PORT_INSTANCE__NAME;

	/**
	 * The feature id for the '<em><b>Type</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REQUIRED_HOST_INSTANCE__TYPE = HOSTING_PORT_INSTANCE__TYPE;

	/**
	 * The number of structural features of the '<em>Required Host Instance</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REQUIRED_HOST_INSTANCE_FEATURE_COUNT = HOSTING_PORT_INSTANCE_FEATURE_COUNT + 0;

	/**
	 * The number of operations of the '<em>Required Host Instance</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REQUIRED_HOST_INSTANCE_OPERATION_COUNT = HOSTING_PORT_INSTANCE_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.camel_dsl.deployment.impl.ConfiguratorCHEFImpl <em>Configurator CHEF</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.camel_dsl.deployment.impl.ConfiguratorCHEFImpl
	 * @see org.camel_dsl.deployment.impl.DeploymentPackageImpl#getConfiguratorCHEF()
	 * @generated
	 */
	int CONFIGURATOR_CHEF = 27;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONFIGURATOR_CHEF__NAME = DEPLOYMENT_ELEMENT__NAME;

	/**
	 * The feature id for the '<em><b>Cookbook</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONFIGURATOR_CHEF__COOKBOOK = DEPLOYMENT_ELEMENT_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Recipe</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONFIGURATOR_CHEF__RECIPE = DEPLOYMENT_ELEMENT_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>Configurator CHEF</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONFIGURATOR_CHEF_FEATURE_COUNT = DEPLOYMENT_ELEMENT_FEATURE_COUNT + 2;

	/**
	 * The number of operations of the '<em>Configurator CHEF</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONFIGURATOR_CHEF_OPERATION_COUNT = DEPLOYMENT_ELEMENT_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.camel_dsl.deployment.impl.MUSAAgentComponentImpl <em>MUSA Agent Component</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.camel_dsl.deployment.impl.MUSAAgentComponentImpl
	 * @see org.camel_dsl.deployment.impl.DeploymentPackageImpl#getMUSAAgentComponent()
	 * @generated
	 */
	int MUSA_AGENT_COMPONENT = 28;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MUSA_AGENT_COMPONENT__NAME = COMPONENT__NAME;

	/**
	 * The feature id for the '<em><b>Provided Communications</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MUSA_AGENT_COMPONENT__PROVIDED_COMMUNICATIONS = COMPONENT__PROVIDED_COMMUNICATIONS;

	/**
	 * The feature id for the '<em><b>Provided Hosts</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MUSA_AGENT_COMPONENT__PROVIDED_HOSTS = COMPONENT__PROVIDED_HOSTS;

	/**
	 * The feature id for the '<em><b>Configurations</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MUSA_AGENT_COMPONENT__CONFIGURATIONS = COMPONENT__CONFIGURATIONS;

	/**
	 * The feature id for the '<em><b>Composite Internal Components</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MUSA_AGENT_COMPONENT__COMPOSITE_INTERNAL_COMPONENTS = COMPONENT_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Required Communications</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MUSA_AGENT_COMPONENT__REQUIRED_COMMUNICATIONS = COMPONENT_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Required Host</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MUSA_AGENT_COMPONENT__REQUIRED_HOST = COMPONENT_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>Version</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MUSA_AGENT_COMPONENT__VERSION = COMPONENT_FEATURE_COUNT + 3;

	/**
	 * The feature id for the '<em><b>Context Path</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MUSA_AGENT_COMPONENT__CONTEXT_PATH = COMPONENT_FEATURE_COUNT + 4;

	/**
	 * The number of structural features of the '<em>MUSA Agent Component</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MUSA_AGENT_COMPONENT_FEATURE_COUNT = COMPONENT_FEATURE_COUNT + 5;

	/**
	 * The number of operations of the '<em>MUSA Agent Component</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MUSA_AGENT_COMPONENT_OPERATION_COUNT = COMPONENT_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.camel_dsl.deployment.impl.CapabilityLinkImpl <em>Capability Link</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.camel_dsl.deployment.impl.CapabilityLinkImpl
	 * @see org.camel_dsl.deployment.impl.DeploymentPackageImpl#getCapabilityLink()
	 * @generated
	 */
	int CAPABILITY_LINK = 29;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CAPABILITY_LINK__NAME = DEPLOYMENT_ELEMENT__NAME;

	/**
	 * The feature id for the '<em><b>Required MUSA Capability</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CAPABILITY_LINK__REQUIRED_MUSA_CAPABILITY = DEPLOYMENT_ELEMENT_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Provided MUSA Capability</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CAPABILITY_LINK__PROVIDED_MUSA_CAPABILITY = DEPLOYMENT_ELEMENT_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>Capability Link</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CAPABILITY_LINK_FEATURE_COUNT = DEPLOYMENT_ELEMENT_FEATURE_COUNT + 2;

	/**
	 * The number of operations of the '<em>Capability Link</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CAPABILITY_LINK_OPERATION_COUNT = DEPLOYMENT_ELEMENT_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.camel_dsl.deployment.impl.ProvidedMUSACapabilityImpl <em>Provided MUSA Capability</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.camel_dsl.deployment.impl.ProvidedMUSACapabilityImpl
	 * @see org.camel_dsl.deployment.impl.DeploymentPackageImpl#getProvidedMUSACapability()
	 * @generated
	 */
	int PROVIDED_MUSA_CAPABILITY = 30;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PROVIDED_MUSA_CAPABILITY__NAME = DEPLOYMENT_ELEMENT__NAME;

	/**
	 * The feature id for the '<em><b>Prov MUS Asecurity Capability</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PROVIDED_MUSA_CAPABILITY__PROV_MUS_ASECURITY_CAPABILITY = DEPLOYMENT_ELEMENT_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Provided MUSA Capability</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PROVIDED_MUSA_CAPABILITY_FEATURE_COUNT = DEPLOYMENT_ELEMENT_FEATURE_COUNT + 1;

	/**
	 * The number of operations of the '<em>Provided MUSA Capability</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PROVIDED_MUSA_CAPABILITY_OPERATION_COUNT = DEPLOYMENT_ELEMENT_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.camel_dsl.deployment.impl.RequiredMUSACapabilityImpl <em>Required MUSA Capability</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.camel_dsl.deployment.impl.RequiredMUSACapabilityImpl
	 * @see org.camel_dsl.deployment.impl.DeploymentPackageImpl#getRequiredMUSACapability()
	 * @generated
	 */
	int REQUIRED_MUSA_CAPABILITY = 31;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REQUIRED_MUSA_CAPABILITY__NAME = DEPLOYMENT_ELEMENT__NAME;

	/**
	 * The feature id for the '<em><b>Req MUS Asecurity Capability</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REQUIRED_MUSA_CAPABILITY__REQ_MUS_ASECURITY_CAPABILITY = DEPLOYMENT_ELEMENT_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Required MUSA Capability</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REQUIRED_MUSA_CAPABILITY_FEATURE_COUNT = DEPLOYMENT_ELEMENT_FEATURE_COUNT + 1;

	/**
	 * The number of operations of the '<em>Required MUSA Capability</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REQUIRED_MUSA_CAPABILITY_OPERATION_COUNT = DEPLOYMENT_ELEMENT_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.camel_dsl.deployment.impl.MUSAPoolImpl <em>MUSA Pool</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.camel_dsl.deployment.impl.MUSAPoolImpl
	 * @see org.camel_dsl.deployment.impl.DeploymentPackageImpl#getMUSAPool()
	 * @generated
	 */
	int MUSA_POOL = 32;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MUSA_POOL__NAME = DEPLOYMENT_ELEMENT__NAME;

	/**
	 * The feature id for the '<em><b>Type</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MUSA_POOL__TYPE = DEPLOYMENT_ELEMENT_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Policy Enum</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MUSA_POOL__POLICY_ENUM = DEPLOYMENT_ELEMENT_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Required Hosts</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MUSA_POOL__REQUIRED_HOSTS = DEPLOYMENT_ELEMENT_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>Provided Host</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MUSA_POOL__PROVIDED_HOST = DEPLOYMENT_ELEMENT_FEATURE_COUNT + 3;

	/**
	 * The number of structural features of the '<em>MUSA Pool</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MUSA_POOL_FEATURE_COUNT = DEPLOYMENT_ELEMENT_FEATURE_COUNT + 4;

	/**
	 * The number of operations of the '<em>MUSA Pool</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MUSA_POOL_OPERATION_COUNT = DEPLOYMENT_ELEMENT_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.camel_dsl.deployment.impl.MUSAContainerImpl <em>MUSA Container</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.camel_dsl.deployment.impl.MUSAContainerImpl
	 * @see org.camel_dsl.deployment.impl.DeploymentPackageImpl#getMUSAContainer()
	 * @generated
	 */
	int MUSA_CONTAINER = 33;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MUSA_CONTAINER__NAME = COMPONENT__NAME;

	/**
	 * The feature id for the '<em><b>Provided Communications</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MUSA_CONTAINER__PROVIDED_COMMUNICATIONS = COMPONENT__PROVIDED_COMMUNICATIONS;

	/**
	 * The feature id for the '<em><b>Provided Hosts</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MUSA_CONTAINER__PROVIDED_HOSTS = COMPONENT__PROVIDED_HOSTS;

	/**
	 * The feature id for the '<em><b>Configurations</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MUSA_CONTAINER__CONFIGURATIONS = COMPONENT__CONFIGURATIONS;

	/**
	 * The feature id for the '<em><b>Type</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MUSA_CONTAINER__TYPE = COMPONENT_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Allocation Strategy Enum</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MUSA_CONTAINER__ALLOCATION_STRATEGY_ENUM = COMPONENT_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Required Pool Host</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MUSA_CONTAINER__REQUIRED_POOL_HOST = COMPONENT_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>Required Pool VM Host</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MUSA_CONTAINER__REQUIRED_POOL_VM_HOST = COMPONENT_FEATURE_COUNT + 3;

	/**
	 * The number of structural features of the '<em>MUSA Container</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MUSA_CONTAINER_FEATURE_COUNT = COMPONENT_FEATURE_COUNT + 4;

	/**
	 * The number of operations of the '<em>MUSA Container</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MUSA_CONTAINER_OPERATION_COUNT = COMPONENT_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.camel_dsl.deployment.CommunicationType <em>Communication Type</em>}' enum.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.camel_dsl.deployment.CommunicationType
	 * @see org.camel_dsl.deployment.impl.DeploymentPackageImpl#getCommunicationType()
	 * @generated
	 */
	int COMMUNICATION_TYPE = 34;

	/**
	 * The meta object id for the '{@link org.camel_dsl.deployment.ProtocolType <em>Protocol Type</em>}' enum.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.camel_dsl.deployment.ProtocolType
	 * @see org.camel_dsl.deployment.impl.DeploymentPackageImpl#getProtocolType()
	 * @generated
	 */
	int PROTOCOL_TYPE = 35;

	/**
	 * The meta object id for the '{@link org.camel_dsl.deployment.LinkType <em>Link Type</em>}' enum.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.camel_dsl.deployment.LinkType
	 * @see org.camel_dsl.deployment.impl.DeploymentPackageImpl#getLinkType()
	 * @generated
	 */
	int LINK_TYPE = 36;


	/**
	 * Returns the meta object for class '{@link org.camel_dsl.deployment.DeploymentElement <em>Element</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Element</em>'.
	 * @see org.camel_dsl.deployment.DeploymentElement
	 * @generated
	 */
	EClass getDeploymentElement();

	/**
	 * Returns the meta object for the attribute '{@link org.camel_dsl.deployment.DeploymentElement#getName <em>Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Name</em>'.
	 * @see org.camel_dsl.deployment.DeploymentElement#getName()
	 * @see #getDeploymentElement()
	 * @generated
	 */
	EAttribute getDeploymentElement_Name();

	/**
	 * Returns the meta object for class '{@link org.camel_dsl.deployment.DeploymentModel <em>Model</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Model</em>'.
	 * @see org.camel_dsl.deployment.DeploymentModel
	 * @generated
	 */
	EClass getDeploymentModel();

	/**
	 * Returns the meta object for the containment reference list '{@link org.camel_dsl.deployment.DeploymentModel#getPools <em>Pools</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Pools</em>'.
	 * @see org.camel_dsl.deployment.DeploymentModel#getPools()
	 * @see #getDeploymentModel()
	 * @generated
	 */
	EReference getDeploymentModel_Pools();

	/**
	 * Returns the meta object for the containment reference list '{@link org.camel_dsl.deployment.DeploymentModel#getContainers <em>Containers</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Containers</em>'.
	 * @see org.camel_dsl.deployment.DeploymentModel#getContainers()
	 * @see #getDeploymentModel()
	 * @generated
	 */
	EReference getDeploymentModel_Containers();

	/**
	 * Returns the meta object for the containment reference list '{@link org.camel_dsl.deployment.DeploymentModel#getInternalComponents <em>Internal Components</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Internal Components</em>'.
	 * @see org.camel_dsl.deployment.DeploymentModel#getInternalComponents()
	 * @see #getDeploymentModel()
	 * @generated
	 */
	EReference getDeploymentModel_InternalComponents();

	/**
	 * Returns the meta object for the containment reference list '{@link org.camel_dsl.deployment.DeploymentModel#getInternalComponentInstances <em>Internal Component Instances</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Internal Component Instances</em>'.
	 * @see org.camel_dsl.deployment.DeploymentModel#getInternalComponentInstances()
	 * @see #getDeploymentModel()
	 * @generated
	 */
	EReference getDeploymentModel_InternalComponentInstances();

	/**
	 * Returns the meta object for the containment reference list '{@link org.camel_dsl.deployment.DeploymentModel#getVms <em>Vms</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Vms</em>'.
	 * @see org.camel_dsl.deployment.DeploymentModel#getVms()
	 * @see #getDeploymentModel()
	 * @generated
	 */
	EReference getDeploymentModel_Vms();

	/**
	 * Returns the meta object for the containment reference list '{@link org.camel_dsl.deployment.DeploymentModel#getVmInstances <em>Vm Instances</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Vm Instances</em>'.
	 * @see org.camel_dsl.deployment.DeploymentModel#getVmInstances()
	 * @see #getDeploymentModel()
	 * @generated
	 */
	EReference getDeploymentModel_VmInstances();

	/**
	 * Returns the meta object for the containment reference list '{@link org.camel_dsl.deployment.DeploymentModel#getCommunications <em>Communications</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Communications</em>'.
	 * @see org.camel_dsl.deployment.DeploymentModel#getCommunications()
	 * @see #getDeploymentModel()
	 * @generated
	 */
	EReference getDeploymentModel_Communications();

	/**
	 * Returns the meta object for the containment reference list '{@link org.camel_dsl.deployment.DeploymentModel#getCommunicationInstances <em>Communication Instances</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Communication Instances</em>'.
	 * @see org.camel_dsl.deployment.DeploymentModel#getCommunicationInstances()
	 * @see #getDeploymentModel()
	 * @generated
	 */
	EReference getDeploymentModel_CommunicationInstances();

	/**
	 * Returns the meta object for the containment reference list '{@link org.camel_dsl.deployment.DeploymentModel#getHostings <em>Hostings</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Hostings</em>'.
	 * @see org.camel_dsl.deployment.DeploymentModel#getHostings()
	 * @see #getDeploymentModel()
	 * @generated
	 */
	EReference getDeploymentModel_Hostings();

	/**
	 * Returns the meta object for the containment reference list '{@link org.camel_dsl.deployment.DeploymentModel#getHostingInstances <em>Hosting Instances</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Hosting Instances</em>'.
	 * @see org.camel_dsl.deployment.DeploymentModel#getHostingInstances()
	 * @see #getDeploymentModel()
	 * @generated
	 */
	EReference getDeploymentModel_HostingInstances();

	/**
	 * Returns the meta object for the containment reference list '{@link org.camel_dsl.deployment.DeploymentModel#getVmRequirementSets <em>Vm Requirement Sets</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Vm Requirement Sets</em>'.
	 * @see org.camel_dsl.deployment.DeploymentModel#getVmRequirementSets()
	 * @see #getDeploymentModel()
	 * @generated
	 */
	EReference getDeploymentModel_VmRequirementSets();

	/**
	 * Returns the meta object for the reference '{@link org.camel_dsl.deployment.DeploymentModel#getGlobalVMRequirementSet <em>Global VM Requirement Set</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Global VM Requirement Set</em>'.
	 * @see org.camel_dsl.deployment.DeploymentModel#getGlobalVMRequirementSet()
	 * @see #getDeploymentModel()
	 * @generated
	 */
	EReference getDeploymentModel_GlobalVMRequirementSet();

	/**
	 * Returns the meta object for the containment reference list '{@link org.camel_dsl.deployment.DeploymentModel#getMusaAgentComponents <em>Musa Agent Components</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Musa Agent Components</em>'.
	 * @see org.camel_dsl.deployment.DeploymentModel#getMusaAgentComponents()
	 * @see #getDeploymentModel()
	 * @generated
	 */
	EReference getDeploymentModel_MusaAgentComponents();

	/**
	 * Returns the meta object for the containment reference list '{@link org.camel_dsl.deployment.DeploymentModel#getCapabilityLinks <em>Capability Links</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Capability Links</em>'.
	 * @see org.camel_dsl.deployment.DeploymentModel#getCapabilityLinks()
	 * @see #getDeploymentModel()
	 * @generated
	 */
	EReference getDeploymentModel_CapabilityLinks();

	/**
	 * Returns the meta object for class '{@link org.camel_dsl.deployment.Component <em>Component</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Component</em>'.
	 * @see org.camel_dsl.deployment.Component
	 * @generated
	 */
	EClass getComponent();

	/**
	 * Returns the meta object for the containment reference list '{@link org.camel_dsl.deployment.Component#getProvidedCommunications <em>Provided Communications</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Provided Communications</em>'.
	 * @see org.camel_dsl.deployment.Component#getProvidedCommunications()
	 * @see #getComponent()
	 * @generated
	 */
	EReference getComponent_ProvidedCommunications();

	/**
	 * Returns the meta object for the containment reference list '{@link org.camel_dsl.deployment.Component#getProvidedHosts <em>Provided Hosts</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Provided Hosts</em>'.
	 * @see org.camel_dsl.deployment.Component#getProvidedHosts()
	 * @see #getComponent()
	 * @generated
	 */
	EReference getComponent_ProvidedHosts();

	/**
	 * Returns the meta object for the containment reference list '{@link org.camel_dsl.deployment.Component#getConfigurations <em>Configurations</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Configurations</em>'.
	 * @see org.camel_dsl.deployment.Component#getConfigurations()
	 * @see #getComponent()
	 * @generated
	 */
	EReference getComponent_Configurations();

	/**
	 * Returns the meta object for class '{@link org.camel_dsl.deployment.InternalComponent <em>Internal Component</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Internal Component</em>'.
	 * @see org.camel_dsl.deployment.InternalComponent
	 * @generated
	 */
	EClass getInternalComponent();

	/**
	 * Returns the meta object for the containment reference list '{@link org.camel_dsl.deployment.InternalComponent#getCompositeInternalComponents <em>Composite Internal Components</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Composite Internal Components</em>'.
	 * @see org.camel_dsl.deployment.InternalComponent#getCompositeInternalComponents()
	 * @see #getInternalComponent()
	 * @generated
	 */
	EReference getInternalComponent_CompositeInternalComponents();

	/**
	 * Returns the meta object for the containment reference list '{@link org.camel_dsl.deployment.InternalComponent#getRequiredCommunications <em>Required Communications</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Required Communications</em>'.
	 * @see org.camel_dsl.deployment.InternalComponent#getRequiredCommunications()
	 * @see #getInternalComponent()
	 * @generated
	 */
	EReference getInternalComponent_RequiredCommunications();

	/**
	 * Returns the meta object for the containment reference '{@link org.camel_dsl.deployment.InternalComponent#getRequiredHost <em>Required Host</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Required Host</em>'.
	 * @see org.camel_dsl.deployment.InternalComponent#getRequiredHost()
	 * @see #getInternalComponent()
	 * @generated
	 */
	EReference getInternalComponent_RequiredHost();

	/**
	 * Returns the meta object for the reference '{@link org.camel_dsl.deployment.InternalComponent#getRequiredContainer <em>Required Container</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Required Container</em>'.
	 * @see org.camel_dsl.deployment.InternalComponent#getRequiredContainer()
	 * @see #getInternalComponent()
	 * @generated
	 */
	EReference getInternalComponent_RequiredContainer();

	/**
	 * Returns the meta object for the reference '{@link org.camel_dsl.deployment.InternalComponent#getRequiredContainerVMHost <em>Required Container VM Host</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Required Container VM Host</em>'.
	 * @see org.camel_dsl.deployment.InternalComponent#getRequiredContainerVMHost()
	 * @see #getInternalComponent()
	 * @generated
	 */
	EReference getInternalComponent_RequiredContainerVMHost();

	/**
	 * Returns the meta object for the attribute '{@link org.camel_dsl.deployment.InternalComponent#getVersion <em>Version</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Version</em>'.
	 * @see org.camel_dsl.deployment.InternalComponent#getVersion()
	 * @see #getInternalComponent()
	 * @generated
	 */
	EAttribute getInternalComponent_Version();

	/**
	 * Returns the meta object for the attribute '{@link org.camel_dsl.deployment.InternalComponent#getContextPath <em>Context Path</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Context Path</em>'.
	 * @see org.camel_dsl.deployment.InternalComponent#getContextPath()
	 * @see #getInternalComponent()
	 * @generated
	 */
	EAttribute getInternalComponent_ContextPath();

	/**
	 * Returns the meta object for the reference '{@link org.camel_dsl.deployment.InternalComponent#getType <em>Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Type</em>'.
	 * @see org.camel_dsl.deployment.InternalComponent#getType()
	 * @see #getInternalComponent()
	 * @generated
	 */
	EReference getInternalComponent_Type();

	/**
	 * Returns the meta object for the attribute '{@link org.camel_dsl.deployment.InternalComponent#getOrder <em>Order</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Order</em>'.
	 * @see org.camel_dsl.deployment.InternalComponent#getOrder()
	 * @see #getInternalComponent()
	 * @generated
	 */
	EAttribute getInternalComponent_Order();

	/**
	 * Returns the meta object for the attribute '{@link org.camel_dsl.deployment.InternalComponent#isIPpublic <em>IPpublic</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>IPpublic</em>'.
	 * @see org.camel_dsl.deployment.InternalComponent#isIPpublic()
	 * @see #getInternalComponent()
	 * @generated
	 */
	EAttribute getInternalComponent_IPpublic();

	/**
	 * Returns the meta object for the attribute '{@link org.camel_dsl.deployment.InternalComponent#getTypeEnum <em>Type Enum</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Type Enum</em>'.
	 * @see org.camel_dsl.deployment.InternalComponent#getTypeEnum()
	 * @see #getInternalComponent()
	 * @generated
	 */
	EAttribute getInternalComponent_TypeEnum();

	/**
	 * Returns the meta object for the containment reference list '{@link org.camel_dsl.deployment.InternalComponent#getRequiredMUSACapabilities <em>Required MUSA Capabilities</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Required MUSA Capabilities</em>'.
	 * @see org.camel_dsl.deployment.InternalComponent#getRequiredMUSACapabilities()
	 * @see #getInternalComponent()
	 * @generated
	 */
	EReference getInternalComponent_RequiredMUSACapabilities();

	/**
	 * Returns the meta object for the containment reference list '{@link org.camel_dsl.deployment.InternalComponent#getProvidedMUSACapabilities <em>Provided MUSA Capabilities</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Provided MUSA Capabilities</em>'.
	 * @see org.camel_dsl.deployment.InternalComponent#getProvidedMUSACapabilities()
	 * @see #getInternalComponent()
	 * @generated
	 */
	EReference getInternalComponent_ProvidedMUSACapabilities();

	/**
	 * Returns the meta object for the '{@link org.camel_dsl.deployment.InternalComponent#contains(org.camel_dsl.deployment.InternalComponent, org.camel_dsl.deployment.InternalComponent) <em>Contains</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Contains</em>' operation.
	 * @see org.camel_dsl.deployment.InternalComponent#contains(org.camel_dsl.deployment.InternalComponent, org.camel_dsl.deployment.InternalComponent)
	 * @generated
	 */
	EOperation getInternalComponent__Contains__InternalComponent_InternalComponent();

	/**
	 * Returns the meta object for class '{@link org.camel_dsl.deployment.VM <em>VM</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>VM</em>'.
	 * @see org.camel_dsl.deployment.VM
	 * @generated
	 */
	EClass getVM();

	/**
	 * Returns the meta object for the reference '{@link org.camel_dsl.deployment.VM#getVmRequirementSet <em>Vm Requirement Set</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Vm Requirement Set</em>'.
	 * @see org.camel_dsl.deployment.VM#getVmRequirementSet()
	 * @see #getVM()
	 * @generated
	 */
	EReference getVM_VmRequirementSet();

	/**
	 * Returns the meta object for class '{@link org.camel_dsl.deployment.VMRequirementSet <em>VM Requirement Set</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>VM Requirement Set</em>'.
	 * @see org.camel_dsl.deployment.VMRequirementSet
	 * @generated
	 */
	EClass getVMRequirementSet();

	/**
	 * Returns the meta object for the attribute '{@link org.camel_dsl.deployment.VMRequirementSet#getName <em>Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Name</em>'.
	 * @see org.camel_dsl.deployment.VMRequirementSet#getName()
	 * @see #getVMRequirementSet()
	 * @generated
	 */
	EAttribute getVMRequirementSet_Name();

	/**
	 * Returns the meta object for the reference '{@link org.camel_dsl.deployment.VMRequirementSet#getLocationRequirement <em>Location Requirement</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Location Requirement</em>'.
	 * @see org.camel_dsl.deployment.VMRequirementSet#getLocationRequirement()
	 * @see #getVMRequirementSet()
	 * @generated
	 */
	EReference getVMRequirementSet_LocationRequirement();

	/**
	 * Returns the meta object for the reference '{@link org.camel_dsl.deployment.VMRequirementSet#getProviderRequirement <em>Provider Requirement</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Provider Requirement</em>'.
	 * @see org.camel_dsl.deployment.VMRequirementSet#getProviderRequirement()
	 * @see #getVMRequirementSet()
	 * @generated
	 */
	EReference getVMRequirementSet_ProviderRequirement();

	/**
	 * Returns the meta object for the reference '{@link org.camel_dsl.deployment.VMRequirementSet#getQualitativeHardwareRequirement <em>Qualitative Hardware Requirement</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Qualitative Hardware Requirement</em>'.
	 * @see org.camel_dsl.deployment.VMRequirementSet#getQualitativeHardwareRequirement()
	 * @see #getVMRequirementSet()
	 * @generated
	 */
	EReference getVMRequirementSet_QualitativeHardwareRequirement();

	/**
	 * Returns the meta object for the reference '{@link org.camel_dsl.deployment.VMRequirementSet#getQuantitativeHardwareRequirement <em>Quantitative Hardware Requirement</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Quantitative Hardware Requirement</em>'.
	 * @see org.camel_dsl.deployment.VMRequirementSet#getQuantitativeHardwareRequirement()
	 * @see #getVMRequirementSet()
	 * @generated
	 */
	EReference getVMRequirementSet_QuantitativeHardwareRequirement();

	/**
	 * Returns the meta object for the reference '{@link org.camel_dsl.deployment.VMRequirementSet#getOsOrImageRequirement <em>Os Or Image Requirement</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Os Or Image Requirement</em>'.
	 * @see org.camel_dsl.deployment.VMRequirementSet#getOsOrImageRequirement()
	 * @see #getVMRequirementSet()
	 * @generated
	 */
	EReference getVMRequirementSet_OsOrImageRequirement();

	/**
	 * Returns the meta object for class '{@link org.camel_dsl.deployment.Configuration <em>Configuration</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Configuration</em>'.
	 * @see org.camel_dsl.deployment.Configuration
	 * @generated
	 */
	EClass getConfiguration();

	/**
	 * Returns the meta object for the attribute '{@link org.camel_dsl.deployment.Configuration#getDownloadCommand <em>Download Command</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Download Command</em>'.
	 * @see org.camel_dsl.deployment.Configuration#getDownloadCommand()
	 * @see #getConfiguration()
	 * @generated
	 */
	EAttribute getConfiguration_DownloadCommand();

	/**
	 * Returns the meta object for the attribute '{@link org.camel_dsl.deployment.Configuration#getUploadCommand <em>Upload Command</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Upload Command</em>'.
	 * @see org.camel_dsl.deployment.Configuration#getUploadCommand()
	 * @see #getConfiguration()
	 * @generated
	 */
	EAttribute getConfiguration_UploadCommand();

	/**
	 * Returns the meta object for the attribute '{@link org.camel_dsl.deployment.Configuration#getInstallCommand <em>Install Command</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Install Command</em>'.
	 * @see org.camel_dsl.deployment.Configuration#getInstallCommand()
	 * @see #getConfiguration()
	 * @generated
	 */
	EAttribute getConfiguration_InstallCommand();

	/**
	 * Returns the meta object for the attribute '{@link org.camel_dsl.deployment.Configuration#getConfigureCommand <em>Configure Command</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Configure Command</em>'.
	 * @see org.camel_dsl.deployment.Configuration#getConfigureCommand()
	 * @see #getConfiguration()
	 * @generated
	 */
	EAttribute getConfiguration_ConfigureCommand();

	/**
	 * Returns the meta object for the attribute '{@link org.camel_dsl.deployment.Configuration#getStartCommand <em>Start Command</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Start Command</em>'.
	 * @see org.camel_dsl.deployment.Configuration#getStartCommand()
	 * @see #getConfiguration()
	 * @generated
	 */
	EAttribute getConfiguration_StartCommand();

	/**
	 * Returns the meta object for the attribute '{@link org.camel_dsl.deployment.Configuration#getStopCommand <em>Stop Command</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Stop Command</em>'.
	 * @see org.camel_dsl.deployment.Configuration#getStopCommand()
	 * @see #getConfiguration()
	 * @generated
	 */
	EAttribute getConfiguration_StopCommand();

	/**
	 * Returns the meta object for the containment reference '{@link org.camel_dsl.deployment.Configuration#getConfManager <em>Conf Manager</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Conf Manager</em>'.
	 * @see org.camel_dsl.deployment.Configuration#getConfManager()
	 * @see #getConfiguration()
	 * @generated
	 */
	EReference getConfiguration_ConfManager();

	/**
	 * Returns the meta object for class '{@link org.camel_dsl.deployment.Communication <em>Communication</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Communication</em>'.
	 * @see org.camel_dsl.deployment.Communication
	 * @generated
	 */
	EClass getCommunication();

	/**
	 * Returns the meta object for the attribute '{@link org.camel_dsl.deployment.Communication#getType <em>Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Type</em>'.
	 * @see org.camel_dsl.deployment.Communication#getType()
	 * @see #getCommunication()
	 * @generated
	 */
	EAttribute getCommunication_Type();

	/**
	 * Returns the meta object for the reference '{@link org.camel_dsl.deployment.Communication#getProvidedCommunication <em>Provided Communication</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Provided Communication</em>'.
	 * @see org.camel_dsl.deployment.Communication#getProvidedCommunication()
	 * @see #getCommunication()
	 * @generated
	 */
	EReference getCommunication_ProvidedCommunication();

	/**
	 * Returns the meta object for the reference '{@link org.camel_dsl.deployment.Communication#getRequiredCommunication <em>Required Communication</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Required Communication</em>'.
	 * @see org.camel_dsl.deployment.Communication#getRequiredCommunication()
	 * @see #getCommunication()
	 * @generated
	 */
	EReference getCommunication_RequiredCommunication();

	/**
	 * Returns the meta object for the containment reference '{@link org.camel_dsl.deployment.Communication#getProvidedPortConfiguration <em>Provided Port Configuration</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Provided Port Configuration</em>'.
	 * @see org.camel_dsl.deployment.Communication#getProvidedPortConfiguration()
	 * @see #getCommunication()
	 * @generated
	 */
	EReference getCommunication_ProvidedPortConfiguration();

	/**
	 * Returns the meta object for the containment reference '{@link org.camel_dsl.deployment.Communication#getRequiredPortConfiguration <em>Required Port Configuration</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Required Port Configuration</em>'.
	 * @see org.camel_dsl.deployment.Communication#getRequiredPortConfiguration()
	 * @see #getCommunication()
	 * @generated
	 */
	EReference getCommunication_RequiredPortConfiguration();

	/**
	 * Returns the meta object for the attribute '{@link org.camel_dsl.deployment.Communication#getProtocol <em>Protocol</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Protocol</em>'.
	 * @see org.camel_dsl.deployment.Communication#getProtocol()
	 * @see #getCommunication()
	 * @generated
	 */
	EAttribute getCommunication_Protocol();

	/**
	 * Returns the meta object for class '{@link org.camel_dsl.deployment.CommunicationPort <em>Communication Port</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Communication Port</em>'.
	 * @see org.camel_dsl.deployment.CommunicationPort
	 * @generated
	 */
	EClass getCommunicationPort();

	/**
	 * Returns the meta object for the attribute '{@link org.camel_dsl.deployment.CommunicationPort#getPortNumber <em>Port Number</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Port Number</em>'.
	 * @see org.camel_dsl.deployment.CommunicationPort#getPortNumber()
	 * @see #getCommunicationPort()
	 * @generated
	 */
	EAttribute getCommunicationPort_PortNumber();

	/**
	 * Returns the meta object for the attribute '{@link org.camel_dsl.deployment.CommunicationPort#getMinPortNumber <em>Min Port Number</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Min Port Number</em>'.
	 * @see org.camel_dsl.deployment.CommunicationPort#getMinPortNumber()
	 * @see #getCommunicationPort()
	 * @generated
	 */
	EAttribute getCommunicationPort_MinPortNumber();

	/**
	 * Returns the meta object for the attribute '{@link org.camel_dsl.deployment.CommunicationPort#getMaxPortNumber <em>Max Port Number</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Max Port Number</em>'.
	 * @see org.camel_dsl.deployment.CommunicationPort#getMaxPortNumber()
	 * @see #getCommunicationPort()
	 * @generated
	 */
	EAttribute getCommunicationPort_MaxPortNumber();

	/**
	 * Returns the meta object for the attribute '{@link org.camel_dsl.deployment.CommunicationPort#getContextPath <em>Context Path</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Context Path</em>'.
	 * @see org.camel_dsl.deployment.CommunicationPort#getContextPath()
	 * @see #getCommunicationPort()
	 * @generated
	 */
	EAttribute getCommunicationPort_ContextPath();

	/**
	 * Returns the meta object for class '{@link org.camel_dsl.deployment.ProvidedCommunication <em>Provided Communication</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Provided Communication</em>'.
	 * @see org.camel_dsl.deployment.ProvidedCommunication
	 * @generated
	 */
	EClass getProvidedCommunication();

	/**
	 * Returns the meta object for class '{@link org.camel_dsl.deployment.RequiredCommunication <em>Required Communication</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Required Communication</em>'.
	 * @see org.camel_dsl.deployment.RequiredCommunication
	 * @generated
	 */
	EClass getRequiredCommunication();

	/**
	 * Returns the meta object for the attribute '{@link org.camel_dsl.deployment.RequiredCommunication#isIsMandatory <em>Is Mandatory</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Is Mandatory</em>'.
	 * @see org.camel_dsl.deployment.RequiredCommunication#isIsMandatory()
	 * @see #getRequiredCommunication()
	 * @generated
	 */
	EAttribute getRequiredCommunication_IsMandatory();

	/**
	 * Returns the meta object for class '{@link org.camel_dsl.deployment.Hosting <em>Hosting</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Hosting</em>'.
	 * @see org.camel_dsl.deployment.Hosting
	 * @generated
	 */
	EClass getHosting();

	/**
	 * Returns the meta object for the reference '{@link org.camel_dsl.deployment.Hosting#getPoolProvidedHost <em>Pool Provided Host</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Pool Provided Host</em>'.
	 * @see org.camel_dsl.deployment.Hosting#getPoolProvidedHost()
	 * @see #getHosting()
	 * @generated
	 */
	EReference getHosting_PoolProvidedHost();

	/**
	 * Returns the meta object for the reference '{@link org.camel_dsl.deployment.Hosting#getProvidedHost <em>Provided Host</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Provided Host</em>'.
	 * @see org.camel_dsl.deployment.Hosting#getProvidedHost()
	 * @see #getHosting()
	 * @generated
	 */
	EReference getHosting_ProvidedHost();

	/**
	 * Returns the meta object for the reference '{@link org.camel_dsl.deployment.Hosting#getRequiredHost <em>Required Host</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Required Host</em>'.
	 * @see org.camel_dsl.deployment.Hosting#getRequiredHost()
	 * @see #getHosting()
	 * @generated
	 */
	EReference getHosting_RequiredHost();

	/**
	 * Returns the meta object for the containment reference '{@link org.camel_dsl.deployment.Hosting#getProvidedHostConfiguration <em>Provided Host Configuration</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Provided Host Configuration</em>'.
	 * @see org.camel_dsl.deployment.Hosting#getProvidedHostConfiguration()
	 * @see #getHosting()
	 * @generated
	 */
	EReference getHosting_ProvidedHostConfiguration();

	/**
	 * Returns the meta object for the containment reference '{@link org.camel_dsl.deployment.Hosting#getRequiredHostConfiguration <em>Required Host Configuration</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Required Host Configuration</em>'.
	 * @see org.camel_dsl.deployment.Hosting#getRequiredHostConfiguration()
	 * @see #getHosting()
	 * @generated
	 */
	EReference getHosting_RequiredHostConfiguration();

	/**
	 * Returns the meta object for class '{@link org.camel_dsl.deployment.HostingPort <em>Hosting Port</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Hosting Port</em>'.
	 * @see org.camel_dsl.deployment.HostingPort
	 * @generated
	 */
	EClass getHostingPort();

	/**
	 * Returns the meta object for class '{@link org.camel_dsl.deployment.ProvidedHost <em>Provided Host</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Provided Host</em>'.
	 * @see org.camel_dsl.deployment.ProvidedHost
	 * @generated
	 */
	EClass getProvidedHost();

	/**
	 * Returns the meta object for class '{@link org.camel_dsl.deployment.MUSAPoolProvidedHost <em>MUSA Pool Provided Host</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>MUSA Pool Provided Host</em>'.
	 * @see org.camel_dsl.deployment.MUSAPoolProvidedHost
	 * @generated
	 */
	EClass getMUSAPoolProvidedHost();

	/**
	 * Returns the meta object for class '{@link org.camel_dsl.deployment.RequiredHost <em>Required Host</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Required Host</em>'.
	 * @see org.camel_dsl.deployment.RequiredHost
	 * @generated
	 */
	EClass getRequiredHost();

	/**
	 * Returns the meta object for class '{@link org.camel_dsl.deployment.ComponentInstance <em>Component Instance</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Component Instance</em>'.
	 * @see org.camel_dsl.deployment.ComponentInstance
	 * @generated
	 */
	EClass getComponentInstance();

	/**
	 * Returns the meta object for the reference '{@link org.camel_dsl.deployment.ComponentInstance#getType <em>Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Type</em>'.
	 * @see org.camel_dsl.deployment.ComponentInstance#getType()
	 * @see #getComponentInstance()
	 * @generated
	 */
	EReference getComponentInstance_Type();

	/**
	 * Returns the meta object for the containment reference list '{@link org.camel_dsl.deployment.ComponentInstance#getProvidedCommunicationInstances <em>Provided Communication Instances</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Provided Communication Instances</em>'.
	 * @see org.camel_dsl.deployment.ComponentInstance#getProvidedCommunicationInstances()
	 * @see #getComponentInstance()
	 * @generated
	 */
	EReference getComponentInstance_ProvidedCommunicationInstances();

	/**
	 * Returns the meta object for the containment reference list '{@link org.camel_dsl.deployment.ComponentInstance#getProvidedHostInstances <em>Provided Host Instances</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Provided Host Instances</em>'.
	 * @see org.camel_dsl.deployment.ComponentInstance#getProvidedHostInstances()
	 * @see #getComponentInstance()
	 * @generated
	 */
	EReference getComponentInstance_ProvidedHostInstances();

	/**
	 * Returns the meta object for the attribute '{@link org.camel_dsl.deployment.ComponentInstance#getInstantiatedOn <em>Instantiated On</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Instantiated On</em>'.
	 * @see org.camel_dsl.deployment.ComponentInstance#getInstantiatedOn()
	 * @see #getComponentInstance()
	 * @generated
	 */
	EAttribute getComponentInstance_InstantiatedOn();

	/**
	 * Returns the meta object for the attribute '{@link org.camel_dsl.deployment.ComponentInstance#getDestroyedOn <em>Destroyed On</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Destroyed On</em>'.
	 * @see org.camel_dsl.deployment.ComponentInstance#getDestroyedOn()
	 * @see #getComponentInstance()
	 * @generated
	 */
	EAttribute getComponentInstance_DestroyedOn();

	/**
	 * Returns the meta object for class '{@link org.camel_dsl.deployment.InternalComponentInstance <em>Internal Component Instance</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Internal Component Instance</em>'.
	 * @see org.camel_dsl.deployment.InternalComponentInstance
	 * @generated
	 */
	EClass getInternalComponentInstance();

	/**
	 * Returns the meta object for the containment reference list '{@link org.camel_dsl.deployment.InternalComponentInstance#getRequiredCommunicationInstances <em>Required Communication Instances</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Required Communication Instances</em>'.
	 * @see org.camel_dsl.deployment.InternalComponentInstance#getRequiredCommunicationInstances()
	 * @see #getInternalComponentInstance()
	 * @generated
	 */
	EReference getInternalComponentInstance_RequiredCommunicationInstances();

	/**
	 * Returns the meta object for the containment reference '{@link org.camel_dsl.deployment.InternalComponentInstance#getRequiredHostInstance <em>Required Host Instance</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Required Host Instance</em>'.
	 * @see org.camel_dsl.deployment.InternalComponentInstance#getRequiredHostInstance()
	 * @see #getInternalComponentInstance()
	 * @generated
	 */
	EReference getInternalComponentInstance_RequiredHostInstance();

	/**
	 * Returns the meta object for class '{@link org.camel_dsl.deployment.VMInstance <em>VM Instance</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>VM Instance</em>'.
	 * @see org.camel_dsl.deployment.VMInstance
	 * @generated
	 */
	EClass getVMInstance();

	/**
	 * Returns the meta object for the reference '{@link org.camel_dsl.deployment.VMInstance#getVmType <em>Vm Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Vm Type</em>'.
	 * @see org.camel_dsl.deployment.VMInstance#getVmType()
	 * @see #getVMInstance()
	 * @generated
	 */
	EReference getVMInstance_VmType();

	/**
	 * Returns the meta object for the reference '{@link org.camel_dsl.deployment.VMInstance#getVmTypeValue <em>Vm Type Value</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Vm Type Value</em>'.
	 * @see org.camel_dsl.deployment.VMInstance#getVmTypeValue()
	 * @see #getVMInstance()
	 * @generated
	 */
	EReference getVMInstance_VmTypeValue();

	/**
	 * Returns the meta object for the attribute '{@link org.camel_dsl.deployment.VMInstance#getIp <em>Ip</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Ip</em>'.
	 * @see org.camel_dsl.deployment.VMInstance#getIp()
	 * @see #getVMInstance()
	 * @generated
	 */
	EAttribute getVMInstance_Ip();

	/**
	 * Returns the meta object for the '{@link org.camel_dsl.deployment.VMInstance#checkDates(org.camel_dsl.deployment.VMInstance) <em>Check Dates</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Check Dates</em>' operation.
	 * @see org.camel_dsl.deployment.VMInstance#checkDates(org.camel_dsl.deployment.VMInstance)
	 * @generated
	 */
	EOperation getVMInstance__CheckDates__VMInstance();

	/**
	 * Returns the meta object for class '{@link org.camel_dsl.deployment.CommunicationInstance <em>Communication Instance</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Communication Instance</em>'.
	 * @see org.camel_dsl.deployment.CommunicationInstance
	 * @generated
	 */
	EClass getCommunicationInstance();

	/**
	 * Returns the meta object for the reference '{@link org.camel_dsl.deployment.CommunicationInstance#getType <em>Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Type</em>'.
	 * @see org.camel_dsl.deployment.CommunicationInstance#getType()
	 * @see #getCommunicationInstance()
	 * @generated
	 */
	EReference getCommunicationInstance_Type();

	/**
	 * Returns the meta object for the reference '{@link org.camel_dsl.deployment.CommunicationInstance#getProvidedCommunicationInstance <em>Provided Communication Instance</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Provided Communication Instance</em>'.
	 * @see org.camel_dsl.deployment.CommunicationInstance#getProvidedCommunicationInstance()
	 * @see #getCommunicationInstance()
	 * @generated
	 */
	EReference getCommunicationInstance_ProvidedCommunicationInstance();

	/**
	 * Returns the meta object for the reference '{@link org.camel_dsl.deployment.CommunicationInstance#getRequiredCommunicationInstance <em>Required Communication Instance</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Required Communication Instance</em>'.
	 * @see org.camel_dsl.deployment.CommunicationInstance#getRequiredCommunicationInstance()
	 * @see #getCommunicationInstance()
	 * @generated
	 */
	EReference getCommunicationInstance_RequiredCommunicationInstance();

	/**
	 * Returns the meta object for class '{@link org.camel_dsl.deployment.CommunicationPortInstance <em>Communication Port Instance</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Communication Port Instance</em>'.
	 * @see org.camel_dsl.deployment.CommunicationPortInstance
	 * @generated
	 */
	EClass getCommunicationPortInstance();

	/**
	 * Returns the meta object for the reference '{@link org.camel_dsl.deployment.CommunicationPortInstance#getType <em>Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Type</em>'.
	 * @see org.camel_dsl.deployment.CommunicationPortInstance#getType()
	 * @see #getCommunicationPortInstance()
	 * @generated
	 */
	EReference getCommunicationPortInstance_Type();

	/**
	 * Returns the meta object for class '{@link org.camel_dsl.deployment.ProvidedCommunicationInstance <em>Provided Communication Instance</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Provided Communication Instance</em>'.
	 * @see org.camel_dsl.deployment.ProvidedCommunicationInstance
	 * @generated
	 */
	EClass getProvidedCommunicationInstance();

	/**
	 * Returns the meta object for class '{@link org.camel_dsl.deployment.RequiredCommunicationInstance <em>Required Communication Instance</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Required Communication Instance</em>'.
	 * @see org.camel_dsl.deployment.RequiredCommunicationInstance
	 * @generated
	 */
	EClass getRequiredCommunicationInstance();

	/**
	 * Returns the meta object for class '{@link org.camel_dsl.deployment.HostingInstance <em>Hosting Instance</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Hosting Instance</em>'.
	 * @see org.camel_dsl.deployment.HostingInstance
	 * @generated
	 */
	EClass getHostingInstance();

	/**
	 * Returns the meta object for the reference '{@link org.camel_dsl.deployment.HostingInstance#getType <em>Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Type</em>'.
	 * @see org.camel_dsl.deployment.HostingInstance#getType()
	 * @see #getHostingInstance()
	 * @generated
	 */
	EReference getHostingInstance_Type();

	/**
	 * Returns the meta object for the reference '{@link org.camel_dsl.deployment.HostingInstance#getProvidedHostInstance <em>Provided Host Instance</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Provided Host Instance</em>'.
	 * @see org.camel_dsl.deployment.HostingInstance#getProvidedHostInstance()
	 * @see #getHostingInstance()
	 * @generated
	 */
	EReference getHostingInstance_ProvidedHostInstance();

	/**
	 * Returns the meta object for the reference '{@link org.camel_dsl.deployment.HostingInstance#getRequiredHostInstance <em>Required Host Instance</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Required Host Instance</em>'.
	 * @see org.camel_dsl.deployment.HostingInstance#getRequiredHostInstance()
	 * @see #getHostingInstance()
	 * @generated
	 */
	EReference getHostingInstance_RequiredHostInstance();

	/**
	 * Returns the meta object for class '{@link org.camel_dsl.deployment.HostingPortInstance <em>Hosting Port Instance</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Hosting Port Instance</em>'.
	 * @see org.camel_dsl.deployment.HostingPortInstance
	 * @generated
	 */
	EClass getHostingPortInstance();

	/**
	 * Returns the meta object for the reference '{@link org.camel_dsl.deployment.HostingPortInstance#getType <em>Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Type</em>'.
	 * @see org.camel_dsl.deployment.HostingPortInstance#getType()
	 * @see #getHostingPortInstance()
	 * @generated
	 */
	EReference getHostingPortInstance_Type();

	/**
	 * Returns the meta object for class '{@link org.camel_dsl.deployment.ProvidedHostInstance <em>Provided Host Instance</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Provided Host Instance</em>'.
	 * @see org.camel_dsl.deployment.ProvidedHostInstance
	 * @generated
	 */
	EClass getProvidedHostInstance();

	/**
	 * Returns the meta object for class '{@link org.camel_dsl.deployment.RequiredHostInstance <em>Required Host Instance</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Required Host Instance</em>'.
	 * @see org.camel_dsl.deployment.RequiredHostInstance
	 * @generated
	 */
	EClass getRequiredHostInstance();

	/**
	 * Returns the meta object for class '{@link org.camel_dsl.deployment.ConfiguratorCHEF <em>Configurator CHEF</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Configurator CHEF</em>'.
	 * @see org.camel_dsl.deployment.ConfiguratorCHEF
	 * @generated
	 */
	EClass getConfiguratorCHEF();

	/**
	 * Returns the meta object for the attribute '{@link org.camel_dsl.deployment.ConfiguratorCHEF#getCookbook <em>Cookbook</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Cookbook</em>'.
	 * @see org.camel_dsl.deployment.ConfiguratorCHEF#getCookbook()
	 * @see #getConfiguratorCHEF()
	 * @generated
	 */
	EAttribute getConfiguratorCHEF_Cookbook();

	/**
	 * Returns the meta object for the attribute '{@link org.camel_dsl.deployment.ConfiguratorCHEF#getRecipe <em>Recipe</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Recipe</em>'.
	 * @see org.camel_dsl.deployment.ConfiguratorCHEF#getRecipe()
	 * @see #getConfiguratorCHEF()
	 * @generated
	 */
	EAttribute getConfiguratorCHEF_Recipe();

	/**
	 * Returns the meta object for class '{@link org.camel_dsl.deployment.MUSAAgentComponent <em>MUSA Agent Component</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>MUSA Agent Component</em>'.
	 * @see org.camel_dsl.deployment.MUSAAgentComponent
	 * @generated
	 */
	EClass getMUSAAgentComponent();

	/**
	 * Returns the meta object for the containment reference list '{@link org.camel_dsl.deployment.MUSAAgentComponent#getCompositeInternalComponents <em>Composite Internal Components</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Composite Internal Components</em>'.
	 * @see org.camel_dsl.deployment.MUSAAgentComponent#getCompositeInternalComponents()
	 * @see #getMUSAAgentComponent()
	 * @generated
	 */
	EReference getMUSAAgentComponent_CompositeInternalComponents();

	/**
	 * Returns the meta object for the containment reference list '{@link org.camel_dsl.deployment.MUSAAgentComponent#getRequiredCommunications <em>Required Communications</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Required Communications</em>'.
	 * @see org.camel_dsl.deployment.MUSAAgentComponent#getRequiredCommunications()
	 * @see #getMUSAAgentComponent()
	 * @generated
	 */
	EReference getMUSAAgentComponent_RequiredCommunications();

	/**
	 * Returns the meta object for the containment reference '{@link org.camel_dsl.deployment.MUSAAgentComponent#getRequiredHost <em>Required Host</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Required Host</em>'.
	 * @see org.camel_dsl.deployment.MUSAAgentComponent#getRequiredHost()
	 * @see #getMUSAAgentComponent()
	 * @generated
	 */
	EReference getMUSAAgentComponent_RequiredHost();

	/**
	 * Returns the meta object for the attribute '{@link org.camel_dsl.deployment.MUSAAgentComponent#getVersion <em>Version</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Version</em>'.
	 * @see org.camel_dsl.deployment.MUSAAgentComponent#getVersion()
	 * @see #getMUSAAgentComponent()
	 * @generated
	 */
	EAttribute getMUSAAgentComponent_Version();

	/**
	 * Returns the meta object for the attribute '{@link org.camel_dsl.deployment.MUSAAgentComponent#getContextPath <em>Context Path</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Context Path</em>'.
	 * @see org.camel_dsl.deployment.MUSAAgentComponent#getContextPath()
	 * @see #getMUSAAgentComponent()
	 * @generated
	 */
	EAttribute getMUSAAgentComponent_ContextPath();

	/**
	 * Returns the meta object for class '{@link org.camel_dsl.deployment.CapabilityLink <em>Capability Link</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Capability Link</em>'.
	 * @see org.camel_dsl.deployment.CapabilityLink
	 * @generated
	 */
	EClass getCapabilityLink();

	/**
	 * Returns the meta object for the reference '{@link org.camel_dsl.deployment.CapabilityLink#getRequiredMUSACapability <em>Required MUSA Capability</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Required MUSA Capability</em>'.
	 * @see org.camel_dsl.deployment.CapabilityLink#getRequiredMUSACapability()
	 * @see #getCapabilityLink()
	 * @generated
	 */
	EReference getCapabilityLink_RequiredMUSACapability();

	/**
	 * Returns the meta object for the reference '{@link org.camel_dsl.deployment.CapabilityLink#getProvidedMUSACapability <em>Provided MUSA Capability</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Provided MUSA Capability</em>'.
	 * @see org.camel_dsl.deployment.CapabilityLink#getProvidedMUSACapability()
	 * @see #getCapabilityLink()
	 * @generated
	 */
	EReference getCapabilityLink_ProvidedMUSACapability();

	/**
	 * Returns the meta object for class '{@link org.camel_dsl.deployment.ProvidedMUSACapability <em>Provided MUSA Capability</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Provided MUSA Capability</em>'.
	 * @see org.camel_dsl.deployment.ProvidedMUSACapability
	 * @generated
	 */
	EClass getProvidedMUSACapability();

	/**
	 * Returns the meta object for the reference '{@link org.camel_dsl.deployment.ProvidedMUSACapability#getProvMUSAsecurityCapability <em>Prov MUS Asecurity Capability</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Prov MUS Asecurity Capability</em>'.
	 * @see org.camel_dsl.deployment.ProvidedMUSACapability#getProvMUSAsecurityCapability()
	 * @see #getProvidedMUSACapability()
	 * @generated
	 */
	EReference getProvidedMUSACapability_ProvMUSAsecurityCapability();

	/**
	 * Returns the meta object for class '{@link org.camel_dsl.deployment.RequiredMUSACapability <em>Required MUSA Capability</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Required MUSA Capability</em>'.
	 * @see org.camel_dsl.deployment.RequiredMUSACapability
	 * @generated
	 */
	EClass getRequiredMUSACapability();

	/**
	 * Returns the meta object for the reference '{@link org.camel_dsl.deployment.RequiredMUSACapability#getReqMUSAsecurityCapability <em>Req MUS Asecurity Capability</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Req MUS Asecurity Capability</em>'.
	 * @see org.camel_dsl.deployment.RequiredMUSACapability#getReqMUSAsecurityCapability()
	 * @see #getRequiredMUSACapability()
	 * @generated
	 */
	EReference getRequiredMUSACapability_ReqMUSAsecurityCapability();

	/**
	 * Returns the meta object for class '{@link org.camel_dsl.deployment.MUSAPool <em>MUSA Pool</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>MUSA Pool</em>'.
	 * @see org.camel_dsl.deployment.MUSAPool
	 * @generated
	 */
	EClass getMUSAPool();

	/**
	 * Returns the meta object for the attribute '{@link org.camel_dsl.deployment.MUSAPool#getType <em>Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Type</em>'.
	 * @see org.camel_dsl.deployment.MUSAPool#getType()
	 * @see #getMUSAPool()
	 * @generated
	 */
	EAttribute getMUSAPool_Type();

	/**
	 * Returns the meta object for the attribute '{@link org.camel_dsl.deployment.MUSAPool#getPolicyEnum <em>Policy Enum</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Policy Enum</em>'.
	 * @see org.camel_dsl.deployment.MUSAPool#getPolicyEnum()
	 * @see #getMUSAPool()
	 * @generated
	 */
	EAttribute getMUSAPool_PolicyEnum();

	/**
	 * Returns the meta object for the reference list '{@link org.camel_dsl.deployment.MUSAPool#getRequiredHosts <em>Required Hosts</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Required Hosts</em>'.
	 * @see org.camel_dsl.deployment.MUSAPool#getRequiredHosts()
	 * @see #getMUSAPool()
	 * @generated
	 */
	EReference getMUSAPool_RequiredHosts();

	/**
	 * Returns the meta object for the containment reference '{@link org.camel_dsl.deployment.MUSAPool#getProvidedHost <em>Provided Host</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Provided Host</em>'.
	 * @see org.camel_dsl.deployment.MUSAPool#getProvidedHost()
	 * @see #getMUSAPool()
	 * @generated
	 */
	EReference getMUSAPool_ProvidedHost();

	/**
	 * Returns the meta object for class '{@link org.camel_dsl.deployment.MUSAContainer <em>MUSA Container</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>MUSA Container</em>'.
	 * @see org.camel_dsl.deployment.MUSAContainer
	 * @generated
	 */
	EClass getMUSAContainer();

	/**
	 * Returns the meta object for the attribute '{@link org.camel_dsl.deployment.MUSAContainer#getType <em>Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Type</em>'.
	 * @see org.camel_dsl.deployment.MUSAContainer#getType()
	 * @see #getMUSAContainer()
	 * @generated
	 */
	EAttribute getMUSAContainer_Type();

	/**
	 * Returns the meta object for the attribute '{@link org.camel_dsl.deployment.MUSAContainer#getAllocationStrategyEnum <em>Allocation Strategy Enum</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Allocation Strategy Enum</em>'.
	 * @see org.camel_dsl.deployment.MUSAContainer#getAllocationStrategyEnum()
	 * @see #getMUSAContainer()
	 * @generated
	 */
	EAttribute getMUSAContainer_AllocationStrategyEnum();

	/**
	 * Returns the meta object for the reference '{@link org.camel_dsl.deployment.MUSAContainer#getRequiredPoolHost <em>Required Pool Host</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Required Pool Host</em>'.
	 * @see org.camel_dsl.deployment.MUSAContainer#getRequiredPoolHost()
	 * @see #getMUSAContainer()
	 * @generated
	 */
	EReference getMUSAContainer_RequiredPoolHost();

	/**
	 * Returns the meta object for the reference '{@link org.camel_dsl.deployment.MUSAContainer#getRequiredPoolVMHost <em>Required Pool VM Host</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Required Pool VM Host</em>'.
	 * @see org.camel_dsl.deployment.MUSAContainer#getRequiredPoolVMHost()
	 * @see #getMUSAContainer()
	 * @generated
	 */
	EReference getMUSAContainer_RequiredPoolVMHost();

	/**
	 * Returns the meta object for enum '{@link org.camel_dsl.deployment.CommunicationType <em>Communication Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for enum '<em>Communication Type</em>'.
	 * @see org.camel_dsl.deployment.CommunicationType
	 * @generated
	 */
	EEnum getCommunicationType();

	/**
	 * Returns the meta object for enum '{@link org.camel_dsl.deployment.ProtocolType <em>Protocol Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for enum '<em>Protocol Type</em>'.
	 * @see org.camel_dsl.deployment.ProtocolType
	 * @generated
	 */
	EEnum getProtocolType();

	/**
	 * Returns the meta object for enum '{@link org.camel_dsl.deployment.LinkType <em>Link Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for enum '<em>Link Type</em>'.
	 * @see org.camel_dsl.deployment.LinkType
	 * @generated
	 */
	EEnum getLinkType();

	/**
	 * Returns the factory that creates the instances of the model.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the factory that creates the instances of the model.
	 * @generated
	 */
	DeploymentFactory getDeploymentFactory();

	/**
	 * <!-- begin-user-doc -->
	 * Defines literals for the meta objects that represent
	 * <ul>
	 *   <li>each class,</li>
	 *   <li>each feature of each class,</li>
	 *   <li>each operation of each class,</li>
	 *   <li>each enum,</li>
	 *   <li>and each data type</li>
	 * </ul>
	 * <!-- end-user-doc -->
	 * @generated
	 */
	interface Literals {
		/**
		 * The meta object literal for the '{@link org.camel_dsl.deployment.impl.DeploymentElementImpl <em>Element</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.camel_dsl.deployment.impl.DeploymentElementImpl
		 * @see org.camel_dsl.deployment.impl.DeploymentPackageImpl#getDeploymentElement()
		 * @generated
		 */
		EClass DEPLOYMENT_ELEMENT = eINSTANCE.getDeploymentElement();

		/**
		 * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute DEPLOYMENT_ELEMENT__NAME = eINSTANCE.getDeploymentElement_Name();

		/**
		 * The meta object literal for the '{@link org.camel_dsl.deployment.impl.DeploymentModelImpl <em>Model</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.camel_dsl.deployment.impl.DeploymentModelImpl
		 * @see org.camel_dsl.deployment.impl.DeploymentPackageImpl#getDeploymentModel()
		 * @generated
		 */
		EClass DEPLOYMENT_MODEL = eINSTANCE.getDeploymentModel();

		/**
		 * The meta object literal for the '<em><b>Pools</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference DEPLOYMENT_MODEL__POOLS = eINSTANCE.getDeploymentModel_Pools();

		/**
		 * The meta object literal for the '<em><b>Containers</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference DEPLOYMENT_MODEL__CONTAINERS = eINSTANCE.getDeploymentModel_Containers();

		/**
		 * The meta object literal for the '<em><b>Internal Components</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference DEPLOYMENT_MODEL__INTERNAL_COMPONENTS = eINSTANCE.getDeploymentModel_InternalComponents();

		/**
		 * The meta object literal for the '<em><b>Internal Component Instances</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference DEPLOYMENT_MODEL__INTERNAL_COMPONENT_INSTANCES = eINSTANCE.getDeploymentModel_InternalComponentInstances();

		/**
		 * The meta object literal for the '<em><b>Vms</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference DEPLOYMENT_MODEL__VMS = eINSTANCE.getDeploymentModel_Vms();

		/**
		 * The meta object literal for the '<em><b>Vm Instances</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference DEPLOYMENT_MODEL__VM_INSTANCES = eINSTANCE.getDeploymentModel_VmInstances();

		/**
		 * The meta object literal for the '<em><b>Communications</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference DEPLOYMENT_MODEL__COMMUNICATIONS = eINSTANCE.getDeploymentModel_Communications();

		/**
		 * The meta object literal for the '<em><b>Communication Instances</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference DEPLOYMENT_MODEL__COMMUNICATION_INSTANCES = eINSTANCE.getDeploymentModel_CommunicationInstances();

		/**
		 * The meta object literal for the '<em><b>Hostings</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference DEPLOYMENT_MODEL__HOSTINGS = eINSTANCE.getDeploymentModel_Hostings();

		/**
		 * The meta object literal for the '<em><b>Hosting Instances</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference DEPLOYMENT_MODEL__HOSTING_INSTANCES = eINSTANCE.getDeploymentModel_HostingInstances();

		/**
		 * The meta object literal for the '<em><b>Vm Requirement Sets</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference DEPLOYMENT_MODEL__VM_REQUIREMENT_SETS = eINSTANCE.getDeploymentModel_VmRequirementSets();

		/**
		 * The meta object literal for the '<em><b>Global VM Requirement Set</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference DEPLOYMENT_MODEL__GLOBAL_VM_REQUIREMENT_SET = eINSTANCE.getDeploymentModel_GlobalVMRequirementSet();

		/**
		 * The meta object literal for the '<em><b>Musa Agent Components</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference DEPLOYMENT_MODEL__MUSA_AGENT_COMPONENTS = eINSTANCE.getDeploymentModel_MusaAgentComponents();

		/**
		 * The meta object literal for the '<em><b>Capability Links</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference DEPLOYMENT_MODEL__CAPABILITY_LINKS = eINSTANCE.getDeploymentModel_CapabilityLinks();

		/**
		 * The meta object literal for the '{@link org.camel_dsl.deployment.impl.ComponentImpl <em>Component</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.camel_dsl.deployment.impl.ComponentImpl
		 * @see org.camel_dsl.deployment.impl.DeploymentPackageImpl#getComponent()
		 * @generated
		 */
		EClass COMPONENT = eINSTANCE.getComponent();

		/**
		 * The meta object literal for the '<em><b>Provided Communications</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference COMPONENT__PROVIDED_COMMUNICATIONS = eINSTANCE.getComponent_ProvidedCommunications();

		/**
		 * The meta object literal for the '<em><b>Provided Hosts</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference COMPONENT__PROVIDED_HOSTS = eINSTANCE.getComponent_ProvidedHosts();

		/**
		 * The meta object literal for the '<em><b>Configurations</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference COMPONENT__CONFIGURATIONS = eINSTANCE.getComponent_Configurations();

		/**
		 * The meta object literal for the '{@link org.camel_dsl.deployment.impl.InternalComponentImpl <em>Internal Component</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.camel_dsl.deployment.impl.InternalComponentImpl
		 * @see org.camel_dsl.deployment.impl.DeploymentPackageImpl#getInternalComponent()
		 * @generated
		 */
		EClass INTERNAL_COMPONENT = eINSTANCE.getInternalComponent();

		/**
		 * The meta object literal for the '<em><b>Composite Internal Components</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference INTERNAL_COMPONENT__COMPOSITE_INTERNAL_COMPONENTS = eINSTANCE.getInternalComponent_CompositeInternalComponents();

		/**
		 * The meta object literal for the '<em><b>Required Communications</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference INTERNAL_COMPONENT__REQUIRED_COMMUNICATIONS = eINSTANCE.getInternalComponent_RequiredCommunications();

		/**
		 * The meta object literal for the '<em><b>Required Host</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference INTERNAL_COMPONENT__REQUIRED_HOST = eINSTANCE.getInternalComponent_RequiredHost();

		/**
		 * The meta object literal for the '<em><b>Required Container</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference INTERNAL_COMPONENT__REQUIRED_CONTAINER = eINSTANCE.getInternalComponent_RequiredContainer();

		/**
		 * The meta object literal for the '<em><b>Required Container VM Host</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference INTERNAL_COMPONENT__REQUIRED_CONTAINER_VM_HOST = eINSTANCE.getInternalComponent_RequiredContainerVMHost();

		/**
		 * The meta object literal for the '<em><b>Version</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute INTERNAL_COMPONENT__VERSION = eINSTANCE.getInternalComponent_Version();

		/**
		 * The meta object literal for the '<em><b>Context Path</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute INTERNAL_COMPONENT__CONTEXT_PATH = eINSTANCE.getInternalComponent_ContextPath();

		/**
		 * The meta object literal for the '<em><b>Type</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference INTERNAL_COMPONENT__TYPE = eINSTANCE.getInternalComponent_Type();

		/**
		 * The meta object literal for the '<em><b>Order</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute INTERNAL_COMPONENT__ORDER = eINSTANCE.getInternalComponent_Order();

		/**
		 * The meta object literal for the '<em><b>IPpublic</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute INTERNAL_COMPONENT__IPPUBLIC = eINSTANCE.getInternalComponent_IPpublic();

		/**
		 * The meta object literal for the '<em><b>Type Enum</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute INTERNAL_COMPONENT__TYPE_ENUM = eINSTANCE.getInternalComponent_TypeEnum();

		/**
		 * The meta object literal for the '<em><b>Required MUSA Capabilities</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference INTERNAL_COMPONENT__REQUIRED_MUSA_CAPABILITIES = eINSTANCE.getInternalComponent_RequiredMUSACapabilities();

		/**
		 * The meta object literal for the '<em><b>Provided MUSA Capabilities</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference INTERNAL_COMPONENT__PROVIDED_MUSA_CAPABILITIES = eINSTANCE.getInternalComponent_ProvidedMUSACapabilities();

		/**
		 * The meta object literal for the '<em><b>Contains</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation INTERNAL_COMPONENT___CONTAINS__INTERNALCOMPONENT_INTERNALCOMPONENT = eINSTANCE.getInternalComponent__Contains__InternalComponent_InternalComponent();

		/**
		 * The meta object literal for the '{@link org.camel_dsl.deployment.impl.VMImpl <em>VM</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.camel_dsl.deployment.impl.VMImpl
		 * @see org.camel_dsl.deployment.impl.DeploymentPackageImpl#getVM()
		 * @generated
		 */
		EClass VM = eINSTANCE.getVM();

		/**
		 * The meta object literal for the '<em><b>Vm Requirement Set</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference VM__VM_REQUIREMENT_SET = eINSTANCE.getVM_VmRequirementSet();

		/**
		 * The meta object literal for the '{@link org.camel_dsl.deployment.impl.VMRequirementSetImpl <em>VM Requirement Set</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.camel_dsl.deployment.impl.VMRequirementSetImpl
		 * @see org.camel_dsl.deployment.impl.DeploymentPackageImpl#getVMRequirementSet()
		 * @generated
		 */
		EClass VM_REQUIREMENT_SET = eINSTANCE.getVMRequirementSet();

		/**
		 * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute VM_REQUIREMENT_SET__NAME = eINSTANCE.getVMRequirementSet_Name();

		/**
		 * The meta object literal for the '<em><b>Location Requirement</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference VM_REQUIREMENT_SET__LOCATION_REQUIREMENT = eINSTANCE.getVMRequirementSet_LocationRequirement();

		/**
		 * The meta object literal for the '<em><b>Provider Requirement</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference VM_REQUIREMENT_SET__PROVIDER_REQUIREMENT = eINSTANCE.getVMRequirementSet_ProviderRequirement();

		/**
		 * The meta object literal for the '<em><b>Qualitative Hardware Requirement</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference VM_REQUIREMENT_SET__QUALITATIVE_HARDWARE_REQUIREMENT = eINSTANCE.getVMRequirementSet_QualitativeHardwareRequirement();

		/**
		 * The meta object literal for the '<em><b>Quantitative Hardware Requirement</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference VM_REQUIREMENT_SET__QUANTITATIVE_HARDWARE_REQUIREMENT = eINSTANCE.getVMRequirementSet_QuantitativeHardwareRequirement();

		/**
		 * The meta object literal for the '<em><b>Os Or Image Requirement</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference VM_REQUIREMENT_SET__OS_OR_IMAGE_REQUIREMENT = eINSTANCE.getVMRequirementSet_OsOrImageRequirement();

		/**
		 * The meta object literal for the '{@link org.camel_dsl.deployment.impl.ConfigurationImpl <em>Configuration</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.camel_dsl.deployment.impl.ConfigurationImpl
		 * @see org.camel_dsl.deployment.impl.DeploymentPackageImpl#getConfiguration()
		 * @generated
		 */
		EClass CONFIGURATION = eINSTANCE.getConfiguration();

		/**
		 * The meta object literal for the '<em><b>Download Command</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute CONFIGURATION__DOWNLOAD_COMMAND = eINSTANCE.getConfiguration_DownloadCommand();

		/**
		 * The meta object literal for the '<em><b>Upload Command</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute CONFIGURATION__UPLOAD_COMMAND = eINSTANCE.getConfiguration_UploadCommand();

		/**
		 * The meta object literal for the '<em><b>Install Command</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute CONFIGURATION__INSTALL_COMMAND = eINSTANCE.getConfiguration_InstallCommand();

		/**
		 * The meta object literal for the '<em><b>Configure Command</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute CONFIGURATION__CONFIGURE_COMMAND = eINSTANCE.getConfiguration_ConfigureCommand();

		/**
		 * The meta object literal for the '<em><b>Start Command</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute CONFIGURATION__START_COMMAND = eINSTANCE.getConfiguration_StartCommand();

		/**
		 * The meta object literal for the '<em><b>Stop Command</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute CONFIGURATION__STOP_COMMAND = eINSTANCE.getConfiguration_StopCommand();

		/**
		 * The meta object literal for the '<em><b>Conf Manager</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference CONFIGURATION__CONF_MANAGER = eINSTANCE.getConfiguration_ConfManager();

		/**
		 * The meta object literal for the '{@link org.camel_dsl.deployment.impl.CommunicationImpl <em>Communication</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.camel_dsl.deployment.impl.CommunicationImpl
		 * @see org.camel_dsl.deployment.impl.DeploymentPackageImpl#getCommunication()
		 * @generated
		 */
		EClass COMMUNICATION = eINSTANCE.getCommunication();

		/**
		 * The meta object literal for the '<em><b>Type</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute COMMUNICATION__TYPE = eINSTANCE.getCommunication_Type();

		/**
		 * The meta object literal for the '<em><b>Provided Communication</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference COMMUNICATION__PROVIDED_COMMUNICATION = eINSTANCE.getCommunication_ProvidedCommunication();

		/**
		 * The meta object literal for the '<em><b>Required Communication</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference COMMUNICATION__REQUIRED_COMMUNICATION = eINSTANCE.getCommunication_RequiredCommunication();

		/**
		 * The meta object literal for the '<em><b>Provided Port Configuration</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference COMMUNICATION__PROVIDED_PORT_CONFIGURATION = eINSTANCE.getCommunication_ProvidedPortConfiguration();

		/**
		 * The meta object literal for the '<em><b>Required Port Configuration</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference COMMUNICATION__REQUIRED_PORT_CONFIGURATION = eINSTANCE.getCommunication_RequiredPortConfiguration();

		/**
		 * The meta object literal for the '<em><b>Protocol</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute COMMUNICATION__PROTOCOL = eINSTANCE.getCommunication_Protocol();

		/**
		 * The meta object literal for the '{@link org.camel_dsl.deployment.impl.CommunicationPortImpl <em>Communication Port</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.camel_dsl.deployment.impl.CommunicationPortImpl
		 * @see org.camel_dsl.deployment.impl.DeploymentPackageImpl#getCommunicationPort()
		 * @generated
		 */
		EClass COMMUNICATION_PORT = eINSTANCE.getCommunicationPort();

		/**
		 * The meta object literal for the '<em><b>Port Number</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute COMMUNICATION_PORT__PORT_NUMBER = eINSTANCE.getCommunicationPort_PortNumber();

		/**
		 * The meta object literal for the '<em><b>Min Port Number</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute COMMUNICATION_PORT__MIN_PORT_NUMBER = eINSTANCE.getCommunicationPort_MinPortNumber();

		/**
		 * The meta object literal for the '<em><b>Max Port Number</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute COMMUNICATION_PORT__MAX_PORT_NUMBER = eINSTANCE.getCommunicationPort_MaxPortNumber();

		/**
		 * The meta object literal for the '<em><b>Context Path</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute COMMUNICATION_PORT__CONTEXT_PATH = eINSTANCE.getCommunicationPort_ContextPath();

		/**
		 * The meta object literal for the '{@link org.camel_dsl.deployment.impl.ProvidedCommunicationImpl <em>Provided Communication</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.camel_dsl.deployment.impl.ProvidedCommunicationImpl
		 * @see org.camel_dsl.deployment.impl.DeploymentPackageImpl#getProvidedCommunication()
		 * @generated
		 */
		EClass PROVIDED_COMMUNICATION = eINSTANCE.getProvidedCommunication();

		/**
		 * The meta object literal for the '{@link org.camel_dsl.deployment.impl.RequiredCommunicationImpl <em>Required Communication</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.camel_dsl.deployment.impl.RequiredCommunicationImpl
		 * @see org.camel_dsl.deployment.impl.DeploymentPackageImpl#getRequiredCommunication()
		 * @generated
		 */
		EClass REQUIRED_COMMUNICATION = eINSTANCE.getRequiredCommunication();

		/**
		 * The meta object literal for the '<em><b>Is Mandatory</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute REQUIRED_COMMUNICATION__IS_MANDATORY = eINSTANCE.getRequiredCommunication_IsMandatory();

		/**
		 * The meta object literal for the '{@link org.camel_dsl.deployment.impl.HostingImpl <em>Hosting</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.camel_dsl.deployment.impl.HostingImpl
		 * @see org.camel_dsl.deployment.impl.DeploymentPackageImpl#getHosting()
		 * @generated
		 */
		EClass HOSTING = eINSTANCE.getHosting();

		/**
		 * The meta object literal for the '<em><b>Pool Provided Host</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference HOSTING__POOL_PROVIDED_HOST = eINSTANCE.getHosting_PoolProvidedHost();

		/**
		 * The meta object literal for the '<em><b>Provided Host</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference HOSTING__PROVIDED_HOST = eINSTANCE.getHosting_ProvidedHost();

		/**
		 * The meta object literal for the '<em><b>Required Host</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference HOSTING__REQUIRED_HOST = eINSTANCE.getHosting_RequiredHost();

		/**
		 * The meta object literal for the '<em><b>Provided Host Configuration</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference HOSTING__PROVIDED_HOST_CONFIGURATION = eINSTANCE.getHosting_ProvidedHostConfiguration();

		/**
		 * The meta object literal for the '<em><b>Required Host Configuration</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference HOSTING__REQUIRED_HOST_CONFIGURATION = eINSTANCE.getHosting_RequiredHostConfiguration();

		/**
		 * The meta object literal for the '{@link org.camel_dsl.deployment.impl.HostingPortImpl <em>Hosting Port</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.camel_dsl.deployment.impl.HostingPortImpl
		 * @see org.camel_dsl.deployment.impl.DeploymentPackageImpl#getHostingPort()
		 * @generated
		 */
		EClass HOSTING_PORT = eINSTANCE.getHostingPort();

		/**
		 * The meta object literal for the '{@link org.camel_dsl.deployment.impl.ProvidedHostImpl <em>Provided Host</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.camel_dsl.deployment.impl.ProvidedHostImpl
		 * @see org.camel_dsl.deployment.impl.DeploymentPackageImpl#getProvidedHost()
		 * @generated
		 */
		EClass PROVIDED_HOST = eINSTANCE.getProvidedHost();

		/**
		 * The meta object literal for the '{@link org.camel_dsl.deployment.impl.MUSAPoolProvidedHostImpl <em>MUSA Pool Provided Host</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.camel_dsl.deployment.impl.MUSAPoolProvidedHostImpl
		 * @see org.camel_dsl.deployment.impl.DeploymentPackageImpl#getMUSAPoolProvidedHost()
		 * @generated
		 */
		EClass MUSA_POOL_PROVIDED_HOST = eINSTANCE.getMUSAPoolProvidedHost();

		/**
		 * The meta object literal for the '{@link org.camel_dsl.deployment.impl.RequiredHostImpl <em>Required Host</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.camel_dsl.deployment.impl.RequiredHostImpl
		 * @see org.camel_dsl.deployment.impl.DeploymentPackageImpl#getRequiredHost()
		 * @generated
		 */
		EClass REQUIRED_HOST = eINSTANCE.getRequiredHost();

		/**
		 * The meta object literal for the '{@link org.camel_dsl.deployment.impl.ComponentInstanceImpl <em>Component Instance</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.camel_dsl.deployment.impl.ComponentInstanceImpl
		 * @see org.camel_dsl.deployment.impl.DeploymentPackageImpl#getComponentInstance()
		 * @generated
		 */
		EClass COMPONENT_INSTANCE = eINSTANCE.getComponentInstance();

		/**
		 * The meta object literal for the '<em><b>Type</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference COMPONENT_INSTANCE__TYPE = eINSTANCE.getComponentInstance_Type();

		/**
		 * The meta object literal for the '<em><b>Provided Communication Instances</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference COMPONENT_INSTANCE__PROVIDED_COMMUNICATION_INSTANCES = eINSTANCE.getComponentInstance_ProvidedCommunicationInstances();

		/**
		 * The meta object literal for the '<em><b>Provided Host Instances</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference COMPONENT_INSTANCE__PROVIDED_HOST_INSTANCES = eINSTANCE.getComponentInstance_ProvidedHostInstances();

		/**
		 * The meta object literal for the '<em><b>Instantiated On</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute COMPONENT_INSTANCE__INSTANTIATED_ON = eINSTANCE.getComponentInstance_InstantiatedOn();

		/**
		 * The meta object literal for the '<em><b>Destroyed On</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute COMPONENT_INSTANCE__DESTROYED_ON = eINSTANCE.getComponentInstance_DestroyedOn();

		/**
		 * The meta object literal for the '{@link org.camel_dsl.deployment.impl.InternalComponentInstanceImpl <em>Internal Component Instance</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.camel_dsl.deployment.impl.InternalComponentInstanceImpl
		 * @see org.camel_dsl.deployment.impl.DeploymentPackageImpl#getInternalComponentInstance()
		 * @generated
		 */
		EClass INTERNAL_COMPONENT_INSTANCE = eINSTANCE.getInternalComponentInstance();

		/**
		 * The meta object literal for the '<em><b>Required Communication Instances</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference INTERNAL_COMPONENT_INSTANCE__REQUIRED_COMMUNICATION_INSTANCES = eINSTANCE.getInternalComponentInstance_RequiredCommunicationInstances();

		/**
		 * The meta object literal for the '<em><b>Required Host Instance</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference INTERNAL_COMPONENT_INSTANCE__REQUIRED_HOST_INSTANCE = eINSTANCE.getInternalComponentInstance_RequiredHostInstance();

		/**
		 * The meta object literal for the '{@link org.camel_dsl.deployment.impl.VMInstanceImpl <em>VM Instance</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.camel_dsl.deployment.impl.VMInstanceImpl
		 * @see org.camel_dsl.deployment.impl.DeploymentPackageImpl#getVMInstance()
		 * @generated
		 */
		EClass VM_INSTANCE = eINSTANCE.getVMInstance();

		/**
		 * The meta object literal for the '<em><b>Vm Type</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference VM_INSTANCE__VM_TYPE = eINSTANCE.getVMInstance_VmType();

		/**
		 * The meta object literal for the '<em><b>Vm Type Value</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference VM_INSTANCE__VM_TYPE_VALUE = eINSTANCE.getVMInstance_VmTypeValue();

		/**
		 * The meta object literal for the '<em><b>Ip</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute VM_INSTANCE__IP = eINSTANCE.getVMInstance_Ip();

		/**
		 * The meta object literal for the '<em><b>Check Dates</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation VM_INSTANCE___CHECK_DATES__VMINSTANCE = eINSTANCE.getVMInstance__CheckDates__VMInstance();

		/**
		 * The meta object literal for the '{@link org.camel_dsl.deployment.impl.CommunicationInstanceImpl <em>Communication Instance</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.camel_dsl.deployment.impl.CommunicationInstanceImpl
		 * @see org.camel_dsl.deployment.impl.DeploymentPackageImpl#getCommunicationInstance()
		 * @generated
		 */
		EClass COMMUNICATION_INSTANCE = eINSTANCE.getCommunicationInstance();

		/**
		 * The meta object literal for the '<em><b>Type</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference COMMUNICATION_INSTANCE__TYPE = eINSTANCE.getCommunicationInstance_Type();

		/**
		 * The meta object literal for the '<em><b>Provided Communication Instance</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference COMMUNICATION_INSTANCE__PROVIDED_COMMUNICATION_INSTANCE = eINSTANCE.getCommunicationInstance_ProvidedCommunicationInstance();

		/**
		 * The meta object literal for the '<em><b>Required Communication Instance</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference COMMUNICATION_INSTANCE__REQUIRED_COMMUNICATION_INSTANCE = eINSTANCE.getCommunicationInstance_RequiredCommunicationInstance();

		/**
		 * The meta object literal for the '{@link org.camel_dsl.deployment.impl.CommunicationPortInstanceImpl <em>Communication Port Instance</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.camel_dsl.deployment.impl.CommunicationPortInstanceImpl
		 * @see org.camel_dsl.deployment.impl.DeploymentPackageImpl#getCommunicationPortInstance()
		 * @generated
		 */
		EClass COMMUNICATION_PORT_INSTANCE = eINSTANCE.getCommunicationPortInstance();

		/**
		 * The meta object literal for the '<em><b>Type</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference COMMUNICATION_PORT_INSTANCE__TYPE = eINSTANCE.getCommunicationPortInstance_Type();

		/**
		 * The meta object literal for the '{@link org.camel_dsl.deployment.impl.ProvidedCommunicationInstanceImpl <em>Provided Communication Instance</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.camel_dsl.deployment.impl.ProvidedCommunicationInstanceImpl
		 * @see org.camel_dsl.deployment.impl.DeploymentPackageImpl#getProvidedCommunicationInstance()
		 * @generated
		 */
		EClass PROVIDED_COMMUNICATION_INSTANCE = eINSTANCE.getProvidedCommunicationInstance();

		/**
		 * The meta object literal for the '{@link org.camel_dsl.deployment.impl.RequiredCommunicationInstanceImpl <em>Required Communication Instance</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.camel_dsl.deployment.impl.RequiredCommunicationInstanceImpl
		 * @see org.camel_dsl.deployment.impl.DeploymentPackageImpl#getRequiredCommunicationInstance()
		 * @generated
		 */
		EClass REQUIRED_COMMUNICATION_INSTANCE = eINSTANCE.getRequiredCommunicationInstance();

		/**
		 * The meta object literal for the '{@link org.camel_dsl.deployment.impl.HostingInstanceImpl <em>Hosting Instance</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.camel_dsl.deployment.impl.HostingInstanceImpl
		 * @see org.camel_dsl.deployment.impl.DeploymentPackageImpl#getHostingInstance()
		 * @generated
		 */
		EClass HOSTING_INSTANCE = eINSTANCE.getHostingInstance();

		/**
		 * The meta object literal for the '<em><b>Type</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference HOSTING_INSTANCE__TYPE = eINSTANCE.getHostingInstance_Type();

		/**
		 * The meta object literal for the '<em><b>Provided Host Instance</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference HOSTING_INSTANCE__PROVIDED_HOST_INSTANCE = eINSTANCE.getHostingInstance_ProvidedHostInstance();

		/**
		 * The meta object literal for the '<em><b>Required Host Instance</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference HOSTING_INSTANCE__REQUIRED_HOST_INSTANCE = eINSTANCE.getHostingInstance_RequiredHostInstance();

		/**
		 * The meta object literal for the '{@link org.camel_dsl.deployment.impl.HostingPortInstanceImpl <em>Hosting Port Instance</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.camel_dsl.deployment.impl.HostingPortInstanceImpl
		 * @see org.camel_dsl.deployment.impl.DeploymentPackageImpl#getHostingPortInstance()
		 * @generated
		 */
		EClass HOSTING_PORT_INSTANCE = eINSTANCE.getHostingPortInstance();

		/**
		 * The meta object literal for the '<em><b>Type</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference HOSTING_PORT_INSTANCE__TYPE = eINSTANCE.getHostingPortInstance_Type();

		/**
		 * The meta object literal for the '{@link org.camel_dsl.deployment.impl.ProvidedHostInstanceImpl <em>Provided Host Instance</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.camel_dsl.deployment.impl.ProvidedHostInstanceImpl
		 * @see org.camel_dsl.deployment.impl.DeploymentPackageImpl#getProvidedHostInstance()
		 * @generated
		 */
		EClass PROVIDED_HOST_INSTANCE = eINSTANCE.getProvidedHostInstance();

		/**
		 * The meta object literal for the '{@link org.camel_dsl.deployment.impl.RequiredHostInstanceImpl <em>Required Host Instance</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.camel_dsl.deployment.impl.RequiredHostInstanceImpl
		 * @see org.camel_dsl.deployment.impl.DeploymentPackageImpl#getRequiredHostInstance()
		 * @generated
		 */
		EClass REQUIRED_HOST_INSTANCE = eINSTANCE.getRequiredHostInstance();

		/**
		 * The meta object literal for the '{@link org.camel_dsl.deployment.impl.ConfiguratorCHEFImpl <em>Configurator CHEF</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.camel_dsl.deployment.impl.ConfiguratorCHEFImpl
		 * @see org.camel_dsl.deployment.impl.DeploymentPackageImpl#getConfiguratorCHEF()
		 * @generated
		 */
		EClass CONFIGURATOR_CHEF = eINSTANCE.getConfiguratorCHEF();

		/**
		 * The meta object literal for the '<em><b>Cookbook</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute CONFIGURATOR_CHEF__COOKBOOK = eINSTANCE.getConfiguratorCHEF_Cookbook();

		/**
		 * The meta object literal for the '<em><b>Recipe</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute CONFIGURATOR_CHEF__RECIPE = eINSTANCE.getConfiguratorCHEF_Recipe();

		/**
		 * The meta object literal for the '{@link org.camel_dsl.deployment.impl.MUSAAgentComponentImpl <em>MUSA Agent Component</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.camel_dsl.deployment.impl.MUSAAgentComponentImpl
		 * @see org.camel_dsl.deployment.impl.DeploymentPackageImpl#getMUSAAgentComponent()
		 * @generated
		 */
		EClass MUSA_AGENT_COMPONENT = eINSTANCE.getMUSAAgentComponent();

		/**
		 * The meta object literal for the '<em><b>Composite Internal Components</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference MUSA_AGENT_COMPONENT__COMPOSITE_INTERNAL_COMPONENTS = eINSTANCE.getMUSAAgentComponent_CompositeInternalComponents();

		/**
		 * The meta object literal for the '<em><b>Required Communications</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference MUSA_AGENT_COMPONENT__REQUIRED_COMMUNICATIONS = eINSTANCE.getMUSAAgentComponent_RequiredCommunications();

		/**
		 * The meta object literal for the '<em><b>Required Host</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference MUSA_AGENT_COMPONENT__REQUIRED_HOST = eINSTANCE.getMUSAAgentComponent_RequiredHost();

		/**
		 * The meta object literal for the '<em><b>Version</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute MUSA_AGENT_COMPONENT__VERSION = eINSTANCE.getMUSAAgentComponent_Version();

		/**
		 * The meta object literal for the '<em><b>Context Path</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute MUSA_AGENT_COMPONENT__CONTEXT_PATH = eINSTANCE.getMUSAAgentComponent_ContextPath();

		/**
		 * The meta object literal for the '{@link org.camel_dsl.deployment.impl.CapabilityLinkImpl <em>Capability Link</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.camel_dsl.deployment.impl.CapabilityLinkImpl
		 * @see org.camel_dsl.deployment.impl.DeploymentPackageImpl#getCapabilityLink()
		 * @generated
		 */
		EClass CAPABILITY_LINK = eINSTANCE.getCapabilityLink();

		/**
		 * The meta object literal for the '<em><b>Required MUSA Capability</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference CAPABILITY_LINK__REQUIRED_MUSA_CAPABILITY = eINSTANCE.getCapabilityLink_RequiredMUSACapability();

		/**
		 * The meta object literal for the '<em><b>Provided MUSA Capability</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference CAPABILITY_LINK__PROVIDED_MUSA_CAPABILITY = eINSTANCE.getCapabilityLink_ProvidedMUSACapability();

		/**
		 * The meta object literal for the '{@link org.camel_dsl.deployment.impl.ProvidedMUSACapabilityImpl <em>Provided MUSA Capability</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.camel_dsl.deployment.impl.ProvidedMUSACapabilityImpl
		 * @see org.camel_dsl.deployment.impl.DeploymentPackageImpl#getProvidedMUSACapability()
		 * @generated
		 */
		EClass PROVIDED_MUSA_CAPABILITY = eINSTANCE.getProvidedMUSACapability();

		/**
		 * The meta object literal for the '<em><b>Prov MUS Asecurity Capability</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference PROVIDED_MUSA_CAPABILITY__PROV_MUS_ASECURITY_CAPABILITY = eINSTANCE.getProvidedMUSACapability_ProvMUSAsecurityCapability();

		/**
		 * The meta object literal for the '{@link org.camel_dsl.deployment.impl.RequiredMUSACapabilityImpl <em>Required MUSA Capability</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.camel_dsl.deployment.impl.RequiredMUSACapabilityImpl
		 * @see org.camel_dsl.deployment.impl.DeploymentPackageImpl#getRequiredMUSACapability()
		 * @generated
		 */
		EClass REQUIRED_MUSA_CAPABILITY = eINSTANCE.getRequiredMUSACapability();

		/**
		 * The meta object literal for the '<em><b>Req MUS Asecurity Capability</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference REQUIRED_MUSA_CAPABILITY__REQ_MUS_ASECURITY_CAPABILITY = eINSTANCE.getRequiredMUSACapability_ReqMUSAsecurityCapability();

		/**
		 * The meta object literal for the '{@link org.camel_dsl.deployment.impl.MUSAPoolImpl <em>MUSA Pool</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.camel_dsl.deployment.impl.MUSAPoolImpl
		 * @see org.camel_dsl.deployment.impl.DeploymentPackageImpl#getMUSAPool()
		 * @generated
		 */
		EClass MUSA_POOL = eINSTANCE.getMUSAPool();

		/**
		 * The meta object literal for the '<em><b>Type</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute MUSA_POOL__TYPE = eINSTANCE.getMUSAPool_Type();

		/**
		 * The meta object literal for the '<em><b>Policy Enum</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute MUSA_POOL__POLICY_ENUM = eINSTANCE.getMUSAPool_PolicyEnum();

		/**
		 * The meta object literal for the '<em><b>Required Hosts</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference MUSA_POOL__REQUIRED_HOSTS = eINSTANCE.getMUSAPool_RequiredHosts();

		/**
		 * The meta object literal for the '<em><b>Provided Host</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference MUSA_POOL__PROVIDED_HOST = eINSTANCE.getMUSAPool_ProvidedHost();

		/**
		 * The meta object literal for the '{@link org.camel_dsl.deployment.impl.MUSAContainerImpl <em>MUSA Container</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.camel_dsl.deployment.impl.MUSAContainerImpl
		 * @see org.camel_dsl.deployment.impl.DeploymentPackageImpl#getMUSAContainer()
		 * @generated
		 */
		EClass MUSA_CONTAINER = eINSTANCE.getMUSAContainer();

		/**
		 * The meta object literal for the '<em><b>Type</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute MUSA_CONTAINER__TYPE = eINSTANCE.getMUSAContainer_Type();

		/**
		 * The meta object literal for the '<em><b>Allocation Strategy Enum</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute MUSA_CONTAINER__ALLOCATION_STRATEGY_ENUM = eINSTANCE.getMUSAContainer_AllocationStrategyEnum();

		/**
		 * The meta object literal for the '<em><b>Required Pool Host</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference MUSA_CONTAINER__REQUIRED_POOL_HOST = eINSTANCE.getMUSAContainer_RequiredPoolHost();

		/**
		 * The meta object literal for the '<em><b>Required Pool VM Host</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference MUSA_CONTAINER__REQUIRED_POOL_VM_HOST = eINSTANCE.getMUSAContainer_RequiredPoolVMHost();

		/**
		 * The meta object literal for the '{@link org.camel_dsl.deployment.CommunicationType <em>Communication Type</em>}' enum.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.camel_dsl.deployment.CommunicationType
		 * @see org.camel_dsl.deployment.impl.DeploymentPackageImpl#getCommunicationType()
		 * @generated
		 */
		EEnum COMMUNICATION_TYPE = eINSTANCE.getCommunicationType();

		/**
		 * The meta object literal for the '{@link org.camel_dsl.deployment.ProtocolType <em>Protocol Type</em>}' enum.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.camel_dsl.deployment.ProtocolType
		 * @see org.camel_dsl.deployment.impl.DeploymentPackageImpl#getProtocolType()
		 * @generated
		 */
		EEnum PROTOCOL_TYPE = eINSTANCE.getProtocolType();

		/**
		 * The meta object literal for the '{@link org.camel_dsl.deployment.LinkType <em>Link Type</em>}' enum.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.camel_dsl.deployment.LinkType
		 * @see org.camel_dsl.deployment.impl.DeploymentPackageImpl#getLinkType()
		 * @generated
		 */
		EEnum LINK_TYPE = eINSTANCE.getLinkType();

	}

} //DeploymentPackage
