/**
 */
package org.camel_dsl.deployment;

import org.camel_dsl.camel.COTSAServiceType;
import org.camel_dsl.camel.MUSAComponentType;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Internal Component</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.camel_dsl.deployment.InternalComponent#getCompositeInternalComponents <em>Composite Internal Components</em>}</li>
 *   <li>{@link org.camel_dsl.deployment.InternalComponent#getRequiredCommunications <em>Required Communications</em>}</li>
 *   <li>{@link org.camel_dsl.deployment.InternalComponent#getRequiredHost <em>Required Host</em>}</li>
 *   <li>{@link org.camel_dsl.deployment.InternalComponent#getRequiredContainer <em>Required Container</em>}</li>
 *   <li>{@link org.camel_dsl.deployment.InternalComponent#getRequiredContainerVMHost <em>Required Container VM Host</em>}</li>
 *   <li>{@link org.camel_dsl.deployment.InternalComponent#getVersion <em>Version</em>}</li>
 *   <li>{@link org.camel_dsl.deployment.InternalComponent#getContextPath <em>Context Path</em>}</li>
 *   <li>{@link org.camel_dsl.deployment.InternalComponent#getType <em>Type</em>}</li>
 *   <li>{@link org.camel_dsl.deployment.InternalComponent#getOrder <em>Order</em>}</li>
 *   <li>{@link org.camel_dsl.deployment.InternalComponent#isIPpublic <em>IPpublic</em>}</li>
 *   <li>{@link org.camel_dsl.deployment.InternalComponent#getTypeEnum <em>Type Enum</em>}</li>
 *   <li>{@link org.camel_dsl.deployment.InternalComponent#getRequiredMUSACapabilities <em>Required MUSA Capabilities</em>}</li>
 *   <li>{@link org.camel_dsl.deployment.InternalComponent#getProvidedMUSACapabilities <em>Provided MUSA Capabilities</em>}</li>
 * </ul>
 *
 * @see org.camel_dsl.deployment.DeploymentPackage#getInternalComponent()
 * @model annotation="http://www.eclipse.org/emf/2002/Ecore constraints='no_recursion_in_parts_of_internal_component'"
 *        annotation="http://www.eclipse.org/emf/2002/Ecore/OCL/Pivot no_recursion_in_parts_of_internal_component='Tuple {\n\tmessage : String = \'InternalComponent: \' + self.name +\n\t\t\t\t\' should not be recursively contained by itself via the compositeInternalComponent association\',\n\tstatus : Boolean = not\n\t\t\t\t(self.contains(self, self))\n}.status'"
 * @generated
 */
public interface InternalComponent extends Component {
	/**
	 * Returns the value of the '<em><b>Composite Internal Components</b></em>' containment reference list.
	 * The list contents are of type {@link org.camel_dsl.deployment.InternalComponent}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Composite Internal Components</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Composite Internal Components</em>' containment reference list.
	 * @see org.camel_dsl.deployment.DeploymentPackage#getInternalComponent_CompositeInternalComponents()
	 * @model containment="true"
	 * @generated
	 */
	EList<InternalComponent> getCompositeInternalComponents();

	/**
	 * Returns the value of the '<em><b>Required Communications</b></em>' containment reference list.
	 * The list contents are of type {@link org.camel_dsl.deployment.RequiredCommunication}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Required Communications</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Required Communications</em>' containment reference list.
	 * @see org.camel_dsl.deployment.DeploymentPackage#getInternalComponent_RequiredCommunications()
	 * @model containment="true"
	 * @generated
	 */
	EList<RequiredCommunication> getRequiredCommunications();

	/**
	 * Returns the value of the '<em><b>Required Host</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Required Host</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Required Host</em>' containment reference.
	 * @see #setRequiredHost(RequiredHost)
	 * @see org.camel_dsl.deployment.DeploymentPackage#getInternalComponent_RequiredHost()
	 * @model containment="true"
	 * @generated
	 */
	RequiredHost getRequiredHost();

	/**
	 * Sets the value of the '{@link org.camel_dsl.deployment.InternalComponent#getRequiredHost <em>Required Host</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Required Host</em>' containment reference.
	 * @see #getRequiredHost()
	 * @generated
	 */
	void setRequiredHost(RequiredHost value);

	/**
	 * Returns the value of the '<em><b>Required Container</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Required Container</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Required Container</em>' reference.
	 * @see #setRequiredContainer(MUSAContainer)
	 * @see org.camel_dsl.deployment.DeploymentPackage#getInternalComponent_RequiredContainer()
	 * @model
	 * @generated
	 */
	MUSAContainer getRequiredContainer();

	/**
	 * Sets the value of the '{@link org.camel_dsl.deployment.InternalComponent#getRequiredContainer <em>Required Container</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Required Container</em>' reference.
	 * @see #getRequiredContainer()
	 * @generated
	 */
	void setRequiredContainer(MUSAContainer value);

	/**
	 * Returns the value of the '<em><b>Required Container VM Host</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Required Container VM Host</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Required Container VM Host</em>' reference.
	 * @see #setRequiredContainerVMHost(ProvidedHost)
	 * @see org.camel_dsl.deployment.DeploymentPackage#getInternalComponent_RequiredContainerVMHost()
	 * @model
	 * @generated
	 */
	ProvidedHost getRequiredContainerVMHost();

	/**
	 * Sets the value of the '{@link org.camel_dsl.deployment.InternalComponent#getRequiredContainerVMHost <em>Required Container VM Host</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Required Container VM Host</em>' reference.
	 * @see #getRequiredContainerVMHost()
	 * @generated
	 */
	void setRequiredContainerVMHost(ProvidedHost value);

	/**
	 * Returns the value of the '<em><b>Version</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Version</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Version</em>' attribute.
	 * @see #setVersion(String)
	 * @see org.camel_dsl.deployment.DeploymentPackage#getInternalComponent_Version()
	 * @model
	 * @generated
	 */
	String getVersion();

	/**
	 * Sets the value of the '{@link org.camel_dsl.deployment.InternalComponent#getVersion <em>Version</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Version</em>' attribute.
	 * @see #getVersion()
	 * @generated
	 */
	void setVersion(String value);

	/**
	 * Returns the value of the '<em><b>Context Path</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Context Path</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Context Path</em>' attribute.
	 * @see #setContextPath(String)
	 * @see org.camel_dsl.deployment.DeploymentPackage#getInternalComponent_ContextPath()
	 * @model
	 * @generated
	 */
	String getContextPath();

	/**
	 * Sets the value of the '{@link org.camel_dsl.deployment.InternalComponent#getContextPath <em>Context Path</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Context Path</em>' attribute.
	 * @see #getContextPath()
	 * @generated
	 */
	void setContextPath(String value);

	/**
	 * Returns the value of the '<em><b>Type</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Type</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Type</em>' reference.
	 * @see #setType(MUSAComponentType)
	 * @see org.camel_dsl.deployment.DeploymentPackage#getInternalComponent_Type()
	 * @model
	 * @generated
	 */
	MUSAComponentType getType();

	/**
	 * Sets the value of the '{@link org.camel_dsl.deployment.InternalComponent#getType <em>Type</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Type</em>' reference.
	 * @see #getType()
	 * @generated
	 */
	void setType(MUSAComponentType value);

	/**
	 * Returns the value of the '<em><b>Order</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Order</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Order</em>' attribute.
	 * @see #setOrder(int)
	 * @see org.camel_dsl.deployment.DeploymentPackage#getInternalComponent_Order()
	 * @model annotation="http://schema.omg.org/spec/MOF/2.0/emof.xml#Property.oppositeRoleName body='BinaryEventPattern' unique='false' upper='*'"
	 * @generated
	 */
	int getOrder();

	/**
	 * Sets the value of the '{@link org.camel_dsl.deployment.InternalComponent#getOrder <em>Order</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Order</em>' attribute.
	 * @see #getOrder()
	 * @generated
	 */
	void setOrder(int value);

	/**
	 * Returns the value of the '<em><b>IPpublic</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>IPpublic</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>IPpublic</em>' attribute.
	 * @see #setIPpublic(boolean)
	 * @see org.camel_dsl.deployment.DeploymentPackage#getInternalComponent_IPpublic()
	 * @model
	 * @generated
	 */
	boolean isIPpublic();

	/**
	 * Sets the value of the '{@link org.camel_dsl.deployment.InternalComponent#isIPpublic <em>IPpublic</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>IPpublic</em>' attribute.
	 * @see #isIPpublic()
	 * @generated
	 */
	void setIPpublic(boolean value);

	/**
	 * Returns the value of the '<em><b>Type Enum</b></em>' attribute.
	 * The literals are from the enumeration {@link org.camel_dsl.camel.COTSAServiceType}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Type Enum</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Type Enum</em>' attribute.
	 * @see org.camel_dsl.camel.COTSAServiceType
	 * @see #setTypeEnum(COTSAServiceType)
	 * @see org.camel_dsl.deployment.DeploymentPackage#getInternalComponent_TypeEnum()
	 * @model
	 * @generated
	 */
	COTSAServiceType getTypeEnum();

	/**
	 * Sets the value of the '{@link org.camel_dsl.deployment.InternalComponent#getTypeEnum <em>Type Enum</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Type Enum</em>' attribute.
	 * @see org.camel_dsl.camel.COTSAServiceType
	 * @see #getTypeEnum()
	 * @generated
	 */
	void setTypeEnum(COTSAServiceType value);

	/**
	 * Returns the value of the '<em><b>Required MUSA Capabilities</b></em>' containment reference list.
	 * The list contents are of type {@link org.camel_dsl.deployment.RequiredMUSACapability}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Required MUSA Capabilities</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Required MUSA Capabilities</em>' containment reference list.
	 * @see org.camel_dsl.deployment.DeploymentPackage#getInternalComponent_RequiredMUSACapabilities()
	 * @model containment="true"
	 * @generated
	 */
	EList<RequiredMUSACapability> getRequiredMUSACapabilities();

	/**
	 * Returns the value of the '<em><b>Provided MUSA Capabilities</b></em>' containment reference list.
	 * The list contents are of type {@link org.camel_dsl.deployment.ProvidedMUSACapability}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Provided MUSA Capabilities</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Provided MUSA Capabilities</em>' containment reference list.
	 * @see org.camel_dsl.deployment.DeploymentPackage#getInternalComponent_ProvidedMUSACapabilities()
	 * @model containment="true"
	 * @generated
	 */
	EList<ProvidedMUSACapability> getProvidedMUSACapabilities();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model icRequired="true" rcRequired="true"
	 *        annotation="http://www.eclipse.org/emf/2002/Ecore/OCL/Pivot body='ic.compositeInternalComponents\n\t\t\t\t\t->exists(p | p.name = rc.name or p.contains(p, rc))'"
	 * @generated
	 */
	boolean contains(InternalComponent ic, InternalComponent rc);

} // InternalComponent
