/**
 */
package org.camel_dsl.deployment;

import org.camel_dsl.camel.MUSAContainerType;
import org.camel_dsl.camel.MUSADockerType;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>MUSA Container</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.camel_dsl.deployment.MUSAContainer#getType <em>Type</em>}</li>
 *   <li>{@link org.camel_dsl.deployment.MUSAContainer#getAllocationStrategyEnum <em>Allocation Strategy Enum</em>}</li>
 *   <li>{@link org.camel_dsl.deployment.MUSAContainer#getRequiredPoolHost <em>Required Pool Host</em>}</li>
 *   <li>{@link org.camel_dsl.deployment.MUSAContainer#getRequiredPoolVMHost <em>Required Pool VM Host</em>}</li>
 * </ul>
 *
 * @see org.camel_dsl.deployment.DeploymentPackage#getMUSAContainer()
 * @model
 * @generated
 */
public interface MUSAContainer extends Component {
	/**
	 * Returns the value of the '<em><b>Type</b></em>' attribute.
	 * The literals are from the enumeration {@link org.camel_dsl.camel.MUSAContainerType}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Type</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Type</em>' attribute.
	 * @see org.camel_dsl.camel.MUSAContainerType
	 * @see #setType(MUSAContainerType)
	 * @see org.camel_dsl.deployment.DeploymentPackage#getMUSAContainer_Type()
	 * @model
	 * @generated
	 */
	MUSAContainerType getType();

	/**
	 * Sets the value of the '{@link org.camel_dsl.deployment.MUSAContainer#getType <em>Type</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Type</em>' attribute.
	 * @see org.camel_dsl.camel.MUSAContainerType
	 * @see #getType()
	 * @generated
	 */
	void setType(MUSAContainerType value);

	/**
	 * Returns the value of the '<em><b>Allocation Strategy Enum</b></em>' attribute.
	 * The literals are from the enumeration {@link org.camel_dsl.camel.MUSADockerType}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Allocation Strategy Enum</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Allocation Strategy Enum</em>' attribute.
	 * @see org.camel_dsl.camel.MUSADockerType
	 * @see #setAllocationStrategyEnum(MUSADockerType)
	 * @see org.camel_dsl.deployment.DeploymentPackage#getMUSAContainer_AllocationStrategyEnum()
	 * @model
	 * @generated
	 */
	MUSADockerType getAllocationStrategyEnum();

	/**
	 * Sets the value of the '{@link org.camel_dsl.deployment.MUSAContainer#getAllocationStrategyEnum <em>Allocation Strategy Enum</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Allocation Strategy Enum</em>' attribute.
	 * @see org.camel_dsl.camel.MUSADockerType
	 * @see #getAllocationStrategyEnum()
	 * @generated
	 */
	void setAllocationStrategyEnum(MUSADockerType value);

	/**
	 * Returns the value of the '<em><b>Required Pool Host</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Required Pool Host</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Required Pool Host</em>' reference.
	 * @see #setRequiredPoolHost(MUSAPool)
	 * @see org.camel_dsl.deployment.DeploymentPackage#getMUSAContainer_RequiredPoolHost()
	 * @model required="true"
	 * @generated
	 */
	MUSAPool getRequiredPoolHost();

	/**
	 * Sets the value of the '{@link org.camel_dsl.deployment.MUSAContainer#getRequiredPoolHost <em>Required Pool Host</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Required Pool Host</em>' reference.
	 * @see #getRequiredPoolHost()
	 * @generated
	 */
	void setRequiredPoolHost(MUSAPool value);

	/**
	 * Returns the value of the '<em><b>Required Pool VM Host</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Required Pool VM Host</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Required Pool VM Host</em>' reference.
	 * @see #setRequiredPoolVMHost(ProvidedHost)
	 * @see org.camel_dsl.deployment.DeploymentPackage#getMUSAContainer_RequiredPoolVMHost()
	 * @model required="true"
	 * @generated
	 */
	ProvidedHost getRequiredPoolVMHost();

	/**
	 * Sets the value of the '{@link org.camel_dsl.deployment.MUSAContainer#getRequiredPoolVMHost <em>Required Pool VM Host</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Required Pool VM Host</em>' reference.
	 * @see #getRequiredPoolVMHost()
	 * @generated
	 */
	void setRequiredPoolVMHost(ProvidedHost value);

} // MUSAContainer
