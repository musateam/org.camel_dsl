/**
 */
package org.camel_dsl.deployment;

import org.camel_dsl.security.MUSASecurityCapability;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Required MUSA Capability</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.camel_dsl.deployment.RequiredMUSACapability#getReqMUSAsecurityCapability <em>Req MUS Asecurity Capability</em>}</li>
 * </ul>
 *
 * @see org.camel_dsl.deployment.DeploymentPackage#getRequiredMUSACapability()
 * @model
 * @generated
 */
public interface RequiredMUSACapability extends DeploymentElement {
	/**
	 * Returns the value of the '<em><b>Req MUS Asecurity Capability</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Req MUS Asecurity Capability</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Req MUS Asecurity Capability</em>' reference.
	 * @see #setReqMUSAsecurityCapability(MUSASecurityCapability)
	 * @see org.camel_dsl.deployment.DeploymentPackage#getRequiredMUSACapability_ReqMUSAsecurityCapability()
	 * @model
	 * @generated
	 */
	MUSASecurityCapability getReqMUSAsecurityCapability();

	/**
	 * Sets the value of the '{@link org.camel_dsl.deployment.RequiredMUSACapability#getReqMUSAsecurityCapability <em>Req MUS Asecurity Capability</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Req MUS Asecurity Capability</em>' reference.
	 * @see #getReqMUSAsecurityCapability()
	 * @generated
	 */
	void setReqMUSAsecurityCapability(MUSASecurityCapability value);

} // RequiredMUSACapability
