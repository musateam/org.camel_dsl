/**
 */
package org.camel_dsl.deployment;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>MUSA Pool Provided Host</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see org.camel_dsl.deployment.DeploymentPackage#getMUSAPoolProvidedHost()
 * @model
 * @generated
 */
public interface MUSAPoolProvidedHost extends HostingPort {
} // MUSAPoolProvidedHost
