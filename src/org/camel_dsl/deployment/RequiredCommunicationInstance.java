/**
 */
package org.camel_dsl.deployment;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Required Communication Instance</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see org.camel_dsl.deployment.DeploymentPackage#getRequiredCommunicationInstance()
 * @model
 * @generated
 */
public interface RequiredCommunicationInstance extends CommunicationPortInstance {
} // RequiredCommunicationInstance
