/**
 */
package org.camel_dsl.deployment;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Required Host</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see org.camel_dsl.deployment.DeploymentPackage#getRequiredHost()
 * @model
 * @generated
 */
public interface RequiredHost extends HostingPort {
} // RequiredHost
