/**
 */
package org.camel_dsl.deployment;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Provided Host</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see org.camel_dsl.deployment.DeploymentPackage#getProvidedHost()
 * @model
 * @generated
 */
public interface ProvidedHost extends HostingPort {
} // ProvidedHost
