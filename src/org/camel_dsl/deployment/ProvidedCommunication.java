/**
 */
package org.camel_dsl.deployment;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Provided Communication</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see org.camel_dsl.deployment.DeploymentPackage#getProvidedCommunication()
 * @model
 * @generated
 */
public interface ProvidedCommunication extends CommunicationPort {
} // ProvidedCommunication
