/**
 */
package org.camel_dsl.deployment;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Hosting Port</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see org.camel_dsl.deployment.DeploymentPackage#getHostingPort()
 * @model abstract="true"
 * @generated
 */
public interface HostingPort extends DeploymentElement {
} // HostingPort
