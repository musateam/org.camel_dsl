/**
 */
package org.camel_dsl.deployment;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>MUSA Agent Component</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.camel_dsl.deployment.MUSAAgentComponent#getCompositeInternalComponents <em>Composite Internal Components</em>}</li>
 *   <li>{@link org.camel_dsl.deployment.MUSAAgentComponent#getRequiredCommunications <em>Required Communications</em>}</li>
 *   <li>{@link org.camel_dsl.deployment.MUSAAgentComponent#getRequiredHost <em>Required Host</em>}</li>
 *   <li>{@link org.camel_dsl.deployment.MUSAAgentComponent#getVersion <em>Version</em>}</li>
 *   <li>{@link org.camel_dsl.deployment.MUSAAgentComponent#getContextPath <em>Context Path</em>}</li>
 * </ul>
 *
 * @see org.camel_dsl.deployment.DeploymentPackage#getMUSAAgentComponent()
 * @model
 * @generated
 */
public interface MUSAAgentComponent extends Component {
	/**
	 * Returns the value of the '<em><b>Composite Internal Components</b></em>' containment reference list.
	 * The list contents are of type {@link org.camel_dsl.deployment.MUSAAgentComponent}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Composite Internal Components</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Composite Internal Components</em>' containment reference list.
	 * @see org.camel_dsl.deployment.DeploymentPackage#getMUSAAgentComponent_CompositeInternalComponents()
	 * @model containment="true"
	 * @generated
	 */
	EList<MUSAAgentComponent> getCompositeInternalComponents();

	/**
	 * Returns the value of the '<em><b>Required Communications</b></em>' containment reference list.
	 * The list contents are of type {@link org.camel_dsl.deployment.RequiredCommunication}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Required Communications</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Required Communications</em>' containment reference list.
	 * @see org.camel_dsl.deployment.DeploymentPackage#getMUSAAgentComponent_RequiredCommunications()
	 * @model containment="true"
	 * @generated
	 */
	EList<RequiredCommunication> getRequiredCommunications();

	/**
	 * Returns the value of the '<em><b>Required Host</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Required Host</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Required Host</em>' containment reference.
	 * @see #setRequiredHost(RequiredHost)
	 * @see org.camel_dsl.deployment.DeploymentPackage#getMUSAAgentComponent_RequiredHost()
	 * @model containment="true"
	 * @generated
	 */
	RequiredHost getRequiredHost();

	/**
	 * Sets the value of the '{@link org.camel_dsl.deployment.MUSAAgentComponent#getRequiredHost <em>Required Host</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Required Host</em>' containment reference.
	 * @see #getRequiredHost()
	 * @generated
	 */
	void setRequiredHost(RequiredHost value);

	/**
	 * Returns the value of the '<em><b>Version</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Version</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Version</em>' attribute.
	 * @see #setVersion(String)
	 * @see org.camel_dsl.deployment.DeploymentPackage#getMUSAAgentComponent_Version()
	 * @model
	 * @generated
	 */
	String getVersion();

	/**
	 * Sets the value of the '{@link org.camel_dsl.deployment.MUSAAgentComponent#getVersion <em>Version</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Version</em>' attribute.
	 * @see #getVersion()
	 * @generated
	 */
	void setVersion(String value);

	/**
	 * Returns the value of the '<em><b>Context Path</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Context Path</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Context Path</em>' attribute.
	 * @see #setContextPath(String)
	 * @see org.camel_dsl.deployment.DeploymentPackage#getMUSAAgentComponent_ContextPath()
	 * @model
	 * @generated
	 */
	String getContextPath();

	/**
	 * Sets the value of the '{@link org.camel_dsl.deployment.MUSAAgentComponent#getContextPath <em>Context Path</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Context Path</em>' attribute.
	 * @see #getContextPath()
	 * @generated
	 */
	void setContextPath(String value);

} // MUSAAgentComponent
