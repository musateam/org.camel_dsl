/**
 */
package org.camel_dsl.deployment;

import org.camel_dsl.security.MUSASecurityCapability;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Provided MUSA Capability</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.camel_dsl.deployment.ProvidedMUSACapability#getProvMUSAsecurityCapability <em>Prov MUS Asecurity Capability</em>}</li>
 * </ul>
 *
 * @see org.camel_dsl.deployment.DeploymentPackage#getProvidedMUSACapability()
 * @model
 * @generated
 */
public interface ProvidedMUSACapability extends DeploymentElement {
	/**
	 * Returns the value of the '<em><b>Prov MUS Asecurity Capability</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Prov MUS Asecurity Capability</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Prov MUS Asecurity Capability</em>' reference.
	 * @see #setProvMUSAsecurityCapability(MUSASecurityCapability)
	 * @see org.camel_dsl.deployment.DeploymentPackage#getProvidedMUSACapability_ProvMUSAsecurityCapability()
	 * @model
	 * @generated
	 */
	MUSASecurityCapability getProvMUSAsecurityCapability();

	/**
	 * Sets the value of the '{@link org.camel_dsl.deployment.ProvidedMUSACapability#getProvMUSAsecurityCapability <em>Prov MUS Asecurity Capability</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Prov MUS Asecurity Capability</em>' reference.
	 * @see #getProvMUSAsecurityCapability()
	 * @generated
	 */
	void setProvMUSAsecurityCapability(MUSASecurityCapability value);

} // ProvidedMUSACapability
