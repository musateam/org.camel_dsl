/**
 */
package org.camel_dsl.unit;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Monetary Unit</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see org.camel_dsl.unit.UnitPackage#getMonetaryUnit()
 * @model
 * @generated
 */
public interface MonetaryUnit extends Unit {
} // MonetaryUnit
