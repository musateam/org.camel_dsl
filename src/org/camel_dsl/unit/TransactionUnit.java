/**
 */
package org.camel_dsl.unit;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Transaction Unit</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see org.camel_dsl.unit.UnitPackage#getTransactionUnit()
 * @model
 * @generated
 */
public interface TransactionUnit extends Unit {
} // TransactionUnit
