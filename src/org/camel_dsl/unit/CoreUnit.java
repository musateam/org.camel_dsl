/**
 */
package org.camel_dsl.unit;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Core Unit</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see org.camel_dsl.unit.UnitPackage#getCoreUnit()
 * @model
 * @generated
 */
public interface CoreUnit extends Unit {
} // CoreUnit
