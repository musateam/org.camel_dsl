/**
 */
package org.camel_dsl.unit;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Request Unit</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see org.camel_dsl.unit.UnitPackage#getRequestUnit()
 * @model
 * @generated
 */
public interface RequestUnit extends Unit {
} // RequestUnit
