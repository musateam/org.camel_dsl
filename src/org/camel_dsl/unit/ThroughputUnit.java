/**
 */
package org.camel_dsl.unit;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Throughput Unit</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see org.camel_dsl.unit.UnitPackage#getThroughputUnit()
 * @model
 * @generated
 */
public interface ThroughputUnit extends Unit {
} // ThroughputUnit
