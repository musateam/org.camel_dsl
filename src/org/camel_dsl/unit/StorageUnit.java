/**
 */
package org.camel_dsl.unit;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Storage Unit</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see org.camel_dsl.unit.UnitPackage#getStorageUnit()
 * @model
 * @generated
 */
public interface StorageUnit extends Unit {
} // StorageUnit
