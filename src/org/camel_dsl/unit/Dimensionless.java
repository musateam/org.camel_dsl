/**
 */
package org.camel_dsl.unit;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Dimensionless</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see org.camel_dsl.unit.UnitPackage#getDimensionless()
 * @model
 * @generated
 */
public interface Dimensionless extends Unit {
} // Dimensionless
