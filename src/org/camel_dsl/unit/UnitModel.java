/**
 */
package org.camel_dsl.unit;

import org.camel_dsl.camel.Model;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Model</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.camel_dsl.unit.UnitModel#getUnits <em>Units</em>}</li>
 * </ul>
 *
 * @see org.camel_dsl.unit.UnitPackage#getUnitModel()
 * @model
 * @generated
 */
public interface UnitModel extends Model {
	/**
	 * Returns the value of the '<em><b>Units</b></em>' containment reference list.
	 * The list contents are of type {@link org.camel_dsl.unit.Unit}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Units</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Units</em>' containment reference list.
	 * @see org.camel_dsl.unit.UnitPackage#getUnitModel_Units()
	 * @model containment="true"
	 * @generated
	 */
	EList<Unit> getUnits();

} // UnitModel
