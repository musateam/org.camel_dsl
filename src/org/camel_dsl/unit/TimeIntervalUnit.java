/**
 */
package org.camel_dsl.unit;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Time Interval Unit</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see org.camel_dsl.unit.UnitPackage#getTimeIntervalUnit()
 * @model
 * @generated
 */
public interface TimeIntervalUnit extends Unit {
} // TimeIntervalUnit
