/**
 */
package org.camel_dsl.unit.impl;

import org.camel_dsl.unit.StorageUnit;
import org.camel_dsl.unit.UnitPackage;

import org.eclipse.emf.ecore.EClass;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Storage Unit</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class StorageUnitImpl extends UnitImpl implements StorageUnit {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected StorageUnitImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return UnitPackage.Literals.STORAGE_UNIT;
	}

} //StorageUnitImpl
