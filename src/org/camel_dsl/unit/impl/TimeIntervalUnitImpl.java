/**
 */
package org.camel_dsl.unit.impl;

import org.camel_dsl.unit.TimeIntervalUnit;
import org.camel_dsl.unit.UnitPackage;

import org.eclipse.emf.ecore.EClass;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Time Interval Unit</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class TimeIntervalUnitImpl extends UnitImpl implements TimeIntervalUnit {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected TimeIntervalUnitImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return UnitPackage.Literals.TIME_INTERVAL_UNIT;
	}

} //TimeIntervalUnitImpl
