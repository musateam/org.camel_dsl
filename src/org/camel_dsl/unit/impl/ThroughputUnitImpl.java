/**
 */
package org.camel_dsl.unit.impl;

import org.camel_dsl.unit.ThroughputUnit;
import org.camel_dsl.unit.UnitPackage;

import org.eclipse.emf.ecore.EClass;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Throughput Unit</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class ThroughputUnitImpl extends UnitImpl implements ThroughputUnit {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ThroughputUnitImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return UnitPackage.Literals.THROUGHPUT_UNIT;
	}

} //ThroughputUnitImpl
