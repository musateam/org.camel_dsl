/**
 */
package org.camel_dsl.unit.impl;

import org.camel_dsl.unit.MonetaryUnit;
import org.camel_dsl.unit.UnitPackage;

import org.eclipse.emf.ecore.EClass;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Monetary Unit</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class MonetaryUnitImpl extends UnitImpl implements MonetaryUnit {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected MonetaryUnitImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return UnitPackage.Literals.MONETARY_UNIT;
	}

} //MonetaryUnitImpl
