/**
 */
package org.camel_dsl.unit.impl;

import org.camel_dsl.unit.RequestUnit;
import org.camel_dsl.unit.UnitPackage;

import org.eclipse.emf.ecore.EClass;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Request Unit</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class RequestUnitImpl extends UnitImpl implements RequestUnit {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected RequestUnitImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return UnitPackage.Literals.REQUEST_UNIT;
	}

} //RequestUnitImpl
