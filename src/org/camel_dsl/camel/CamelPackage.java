/**
 */
package org.camel_dsl.camel;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EEnum;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

/**
 * <!-- begin-user-doc -->
 * The <b>Package</b> for the model.
 * It contains accessors for the meta objects to represent
 * <ul>
 *   <li>each class,</li>
 *   <li>each feature of each class,</li>
 *   <li>each operation of each class,</li>
 *   <li>each enum,</li>
 *   <li>and each data type</li>
 * </ul>
 * <!-- end-user-doc -->
 * @see org.camel_dsl.camel.CamelFactory
 * @model kind="package"
 *        annotation="http://www.eclipse.org/OCL/Import ecore='http://www.eclipse.org/emf/2002/Ecore'"
 *        annotation="http://www.eclipse.org/emf/2002/Ecore invocationDelegates='http://www.eclipse.org/emf/2002/Ecore/OCL/Pivot' settingDelegates='http://www.eclipse.org/emf/2002/Ecore/OCL/Pivot' validationDelegates='http://www.eclipse.org/emf/2002/Ecore/OCL/Pivot'"
 * @generated
 */
public interface CamelPackage extends EPackage {
	/**
	 * The package name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNAME = "camel";

	/**
	 * The package namespace URI.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_URI = "http://www.camel-dsl.eu/2017/03/camel";

	/**
	 * The package namespace name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_PREFIX = "camel";

	/**
	 * The singleton instance of the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	CamelPackage eINSTANCE = org.camel_dsl.camel.impl.CamelPackageImpl.init();

	/**
	 * The meta object id for the '{@link org.camel_dsl.camel.impl.ModelImpl <em>Model</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.camel_dsl.camel.impl.ModelImpl
	 * @see org.camel_dsl.camel.impl.CamelPackageImpl#getModel()
	 * @generated
	 */
	int MODEL = 0;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MODEL__NAME = 0;

	/**
	 * The feature id for the '<em><b>Import URI</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MODEL__IMPORT_URI = 1;

	/**
	 * The number of structural features of the '<em>Model</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MODEL_FEATURE_COUNT = 2;

	/**
	 * The number of operations of the '<em>Model</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MODEL_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link org.camel_dsl.camel.impl.CamelModelImpl <em>Model</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.camel_dsl.camel.impl.CamelModelImpl
	 * @see org.camel_dsl.camel.impl.CamelPackageImpl#getCamelModel()
	 * @generated
	 */
	int CAMEL_MODEL = 1;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CAMEL_MODEL__NAME = MODEL__NAME;

	/**
	 * The feature id for the '<em><b>Import URI</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CAMEL_MODEL__IMPORT_URI = MODEL__IMPORT_URI;

	/**
	 * The feature id for the '<em><b>Actions</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CAMEL_MODEL__ACTIONS = MODEL_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Applications</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CAMEL_MODEL__APPLICATIONS = MODEL_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Deployment Models</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CAMEL_MODEL__DEPLOYMENT_MODELS = MODEL_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>Execution Models</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CAMEL_MODEL__EXECUTION_MODELS = MODEL_FEATURE_COUNT + 3;

	/**
	 * The feature id for the '<em><b>Location Models</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CAMEL_MODEL__LOCATION_MODELS = MODEL_FEATURE_COUNT + 4;

	/**
	 * The feature id for the '<em><b>Metric Models</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CAMEL_MODEL__METRIC_MODELS = MODEL_FEATURE_COUNT + 5;

	/**
	 * The feature id for the '<em><b>Organisation Models</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CAMEL_MODEL__ORGANISATION_MODELS = MODEL_FEATURE_COUNT + 6;

	/**
	 * The feature id for the '<em><b>Provider Models</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CAMEL_MODEL__PROVIDER_MODELS = MODEL_FEATURE_COUNT + 7;

	/**
	 * The feature id for the '<em><b>Requirement Models</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CAMEL_MODEL__REQUIREMENT_MODELS = MODEL_FEATURE_COUNT + 8;

	/**
	 * The feature id for the '<em><b>Scalability Models</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CAMEL_MODEL__SCALABILITY_MODELS = MODEL_FEATURE_COUNT + 9;

	/**
	 * The feature id for the '<em><b>Security Models</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CAMEL_MODEL__SECURITY_MODELS = MODEL_FEATURE_COUNT + 10;

	/**
	 * The feature id for the '<em><b>Type Models</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CAMEL_MODEL__TYPE_MODELS = MODEL_FEATURE_COUNT + 11;

	/**
	 * The feature id for the '<em><b>Unit Models</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CAMEL_MODEL__UNIT_MODELS = MODEL_FEATURE_COUNT + 12;

	/**
	 * The feature id for the '<em><b>Musa Component Types</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CAMEL_MODEL__MUSA_COMPONENT_TYPES = MODEL_FEATURE_COUNT + 13;

	/**
	 * The feature id for the '<em><b>Musa Agent Types</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CAMEL_MODEL__MUSA_AGENT_TYPES = MODEL_FEATURE_COUNT + 14;

	/**
	 * The number of structural features of the '<em>Model</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CAMEL_MODEL_FEATURE_COUNT = MODEL_FEATURE_COUNT + 15;

	/**
	 * The number of operations of the '<em>Model</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CAMEL_MODEL_OPERATION_COUNT = MODEL_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.camel_dsl.camel.impl.ActionImpl <em>Action</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.camel_dsl.camel.impl.ActionImpl
	 * @see org.camel_dsl.camel.impl.CamelPackageImpl#getAction()
	 * @generated
	 */
	int ACTION = 2;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTION__NAME = 0;

	/**
	 * The feature id for the '<em><b>Type</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTION__TYPE = 1;

	/**
	 * The number of structural features of the '<em>Action</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTION_FEATURE_COUNT = 2;

	/**
	 * The number of operations of the '<em>Action</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTION_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link org.camel_dsl.camel.impl.ApplicationImpl <em>Application</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.camel_dsl.camel.impl.ApplicationImpl
	 * @see org.camel_dsl.camel.impl.CamelPackageImpl#getApplication()
	 * @generated
	 */
	int APPLICATION = 3;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int APPLICATION__NAME = 0;

	/**
	 * The feature id for the '<em><b>Version</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int APPLICATION__VERSION = 1;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int APPLICATION__DESCRIPTION = 2;

	/**
	 * The feature id for the '<em><b>Owner</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int APPLICATION__OWNER = 3;

	/**
	 * The feature id for the '<em><b>Deployment Models</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int APPLICATION__DEPLOYMENT_MODELS = 4;

	/**
	 * The number of structural features of the '<em>Application</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int APPLICATION_FEATURE_COUNT = 5;

	/**
	 * The number of operations of the '<em>Application</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int APPLICATION_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link org.camel_dsl.camel.impl.MUSAAgentTypeImpl <em>MUSA Agent Type</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.camel_dsl.camel.impl.MUSAAgentTypeImpl
	 * @see org.camel_dsl.camel.impl.CamelPackageImpl#getMUSAAgentType()
	 * @generated
	 */
	int MUSA_AGENT_TYPE = 4;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MUSA_AGENT_TYPE__NAME = 0;

	/**
	 * The number of structural features of the '<em>MUSA Agent Type</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MUSA_AGENT_TYPE_FEATURE_COUNT = 1;

	/**
	 * The number of operations of the '<em>MUSA Agent Type</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MUSA_AGENT_TYPE_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link org.camel_dsl.camel.impl.MUSAComponentTypeImpl <em>MUSA Component Type</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.camel_dsl.camel.impl.MUSAComponentTypeImpl
	 * @see org.camel_dsl.camel.impl.CamelPackageImpl#getMUSAComponentType()
	 * @generated
	 */
	int MUSA_COMPONENT_TYPE = 5;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MUSA_COMPONENT_TYPE__NAME = 0;

	/**
	 * The number of structural features of the '<em>MUSA Component Type</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MUSA_COMPONENT_TYPE_FEATURE_COUNT = 1;

	/**
	 * The number of operations of the '<em>MUSA Component Type</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MUSA_COMPONENT_TYPE_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link org.camel_dsl.camel.ActionType <em>Action Type</em>}' enum.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.camel_dsl.camel.ActionType
	 * @see org.camel_dsl.camel.impl.CamelPackageImpl#getActionType()
	 * @generated
	 */
	int ACTION_TYPE = 6;

	/**
	 * The meta object id for the '{@link org.camel_dsl.camel.LayerType <em>Layer Type</em>}' enum.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.camel_dsl.camel.LayerType
	 * @see org.camel_dsl.camel.impl.CamelPackageImpl#getLayerType()
	 * @generated
	 */
	int LAYER_TYPE = 7;

	/**
	 * The meta object id for the '{@link org.camel_dsl.camel.COTSAServiceType <em>COTSA Service Type</em>}' enum.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.camel_dsl.camel.COTSAServiceType
	 * @see org.camel_dsl.camel.impl.CamelPackageImpl#getCOTSAServiceType()
	 * @generated
	 */
	int COTSA_SERVICE_TYPE = 8;

	/**
	 * The meta object id for the '{@link org.camel_dsl.camel.MUSAPoolType <em>MUSA Pool Type</em>}' enum.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.camel_dsl.camel.MUSAPoolType
	 * @see org.camel_dsl.camel.impl.CamelPackageImpl#getMUSAPoolType()
	 * @generated
	 */
	int MUSA_POOL_TYPE = 9;

	/**
	 * The meta object id for the '{@link org.camel_dsl.camel.MUSAPolicyType <em>MUSA Policy Type</em>}' enum.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.camel_dsl.camel.MUSAPolicyType
	 * @see org.camel_dsl.camel.impl.CamelPackageImpl#getMUSAPolicyType()
	 * @generated
	 */
	int MUSA_POLICY_TYPE = 10;

	/**
	 * The meta object id for the '{@link org.camel_dsl.camel.MUSAContainerType <em>MUSA Container Type</em>}' enum.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.camel_dsl.camel.MUSAContainerType
	 * @see org.camel_dsl.camel.impl.CamelPackageImpl#getMUSAContainerType()
	 * @generated
	 */
	int MUSA_CONTAINER_TYPE = 11;

	/**
	 * The meta object id for the '{@link org.camel_dsl.camel.MUSADockerType <em>MUSA Docker Type</em>}' enum.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.camel_dsl.camel.MUSADockerType
	 * @see org.camel_dsl.camel.impl.CamelPackageImpl#getMUSADockerType()
	 * @generated
	 */
	int MUSA_DOCKER_TYPE = 12;


	/**
	 * Returns the meta object for class '{@link org.camel_dsl.camel.Model <em>Model</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Model</em>'.
	 * @see org.camel_dsl.camel.Model
	 * @generated
	 */
	EClass getModel();

	/**
	 * Returns the meta object for the attribute '{@link org.camel_dsl.camel.Model#getName <em>Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Name</em>'.
	 * @see org.camel_dsl.camel.Model#getName()
	 * @see #getModel()
	 * @generated
	 */
	EAttribute getModel_Name();

	/**
	 * Returns the meta object for the attribute list '{@link org.camel_dsl.camel.Model#getImportURI <em>Import URI</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute list '<em>Import URI</em>'.
	 * @see org.camel_dsl.camel.Model#getImportURI()
	 * @see #getModel()
	 * @generated
	 */
	EAttribute getModel_ImportURI();

	/**
	 * Returns the meta object for class '{@link org.camel_dsl.camel.CamelModel <em>Model</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Model</em>'.
	 * @see org.camel_dsl.camel.CamelModel
	 * @generated
	 */
	EClass getCamelModel();

	/**
	 * Returns the meta object for the containment reference list '{@link org.camel_dsl.camel.CamelModel#getActions <em>Actions</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Actions</em>'.
	 * @see org.camel_dsl.camel.CamelModel#getActions()
	 * @see #getCamelModel()
	 * @generated
	 */
	EReference getCamelModel_Actions();

	/**
	 * Returns the meta object for the containment reference list '{@link org.camel_dsl.camel.CamelModel#getApplications <em>Applications</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Applications</em>'.
	 * @see org.camel_dsl.camel.CamelModel#getApplications()
	 * @see #getCamelModel()
	 * @generated
	 */
	EReference getCamelModel_Applications();

	/**
	 * Returns the meta object for the containment reference list '{@link org.camel_dsl.camel.CamelModel#getDeploymentModels <em>Deployment Models</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Deployment Models</em>'.
	 * @see org.camel_dsl.camel.CamelModel#getDeploymentModels()
	 * @see #getCamelModel()
	 * @generated
	 */
	EReference getCamelModel_DeploymentModels();

	/**
	 * Returns the meta object for the containment reference list '{@link org.camel_dsl.camel.CamelModel#getExecutionModels <em>Execution Models</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Execution Models</em>'.
	 * @see org.camel_dsl.camel.CamelModel#getExecutionModels()
	 * @see #getCamelModel()
	 * @generated
	 */
	EReference getCamelModel_ExecutionModels();

	/**
	 * Returns the meta object for the containment reference list '{@link org.camel_dsl.camel.CamelModel#getLocationModels <em>Location Models</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Location Models</em>'.
	 * @see org.camel_dsl.camel.CamelModel#getLocationModels()
	 * @see #getCamelModel()
	 * @generated
	 */
	EReference getCamelModel_LocationModels();

	/**
	 * Returns the meta object for the containment reference list '{@link org.camel_dsl.camel.CamelModel#getMetricModels <em>Metric Models</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Metric Models</em>'.
	 * @see org.camel_dsl.camel.CamelModel#getMetricModels()
	 * @see #getCamelModel()
	 * @generated
	 */
	EReference getCamelModel_MetricModels();

	/**
	 * Returns the meta object for the containment reference list '{@link org.camel_dsl.camel.CamelModel#getOrganisationModels <em>Organisation Models</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Organisation Models</em>'.
	 * @see org.camel_dsl.camel.CamelModel#getOrganisationModels()
	 * @see #getCamelModel()
	 * @generated
	 */
	EReference getCamelModel_OrganisationModels();

	/**
	 * Returns the meta object for the containment reference list '{@link org.camel_dsl.camel.CamelModel#getProviderModels <em>Provider Models</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Provider Models</em>'.
	 * @see org.camel_dsl.camel.CamelModel#getProviderModels()
	 * @see #getCamelModel()
	 * @generated
	 */
	EReference getCamelModel_ProviderModels();

	/**
	 * Returns the meta object for the containment reference list '{@link org.camel_dsl.camel.CamelModel#getRequirementModels <em>Requirement Models</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Requirement Models</em>'.
	 * @see org.camel_dsl.camel.CamelModel#getRequirementModels()
	 * @see #getCamelModel()
	 * @generated
	 */
	EReference getCamelModel_RequirementModels();

	/**
	 * Returns the meta object for the containment reference list '{@link org.camel_dsl.camel.CamelModel#getScalabilityModels <em>Scalability Models</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Scalability Models</em>'.
	 * @see org.camel_dsl.camel.CamelModel#getScalabilityModels()
	 * @see #getCamelModel()
	 * @generated
	 */
	EReference getCamelModel_ScalabilityModels();

	/**
	 * Returns the meta object for the containment reference list '{@link org.camel_dsl.camel.CamelModel#getSecurityModels <em>Security Models</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Security Models</em>'.
	 * @see org.camel_dsl.camel.CamelModel#getSecurityModels()
	 * @see #getCamelModel()
	 * @generated
	 */
	EReference getCamelModel_SecurityModels();

	/**
	 * Returns the meta object for the containment reference list '{@link org.camel_dsl.camel.CamelModel#getTypeModels <em>Type Models</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Type Models</em>'.
	 * @see org.camel_dsl.camel.CamelModel#getTypeModels()
	 * @see #getCamelModel()
	 * @generated
	 */
	EReference getCamelModel_TypeModels();

	/**
	 * Returns the meta object for the containment reference list '{@link org.camel_dsl.camel.CamelModel#getUnitModels <em>Unit Models</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Unit Models</em>'.
	 * @see org.camel_dsl.camel.CamelModel#getUnitModels()
	 * @see #getCamelModel()
	 * @generated
	 */
	EReference getCamelModel_UnitModels();

	/**
	 * Returns the meta object for the containment reference list '{@link org.camel_dsl.camel.CamelModel#getMusaComponentTypes <em>Musa Component Types</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Musa Component Types</em>'.
	 * @see org.camel_dsl.camel.CamelModel#getMusaComponentTypes()
	 * @see #getCamelModel()
	 * @generated
	 */
	EReference getCamelModel_MusaComponentTypes();

	/**
	 * Returns the meta object for the containment reference list '{@link org.camel_dsl.camel.CamelModel#getMusaAgentTypes <em>Musa Agent Types</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Musa Agent Types</em>'.
	 * @see org.camel_dsl.camel.CamelModel#getMusaAgentTypes()
	 * @see #getCamelModel()
	 * @generated
	 */
	EReference getCamelModel_MusaAgentTypes();

	/**
	 * Returns the meta object for class '{@link org.camel_dsl.camel.Action <em>Action</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Action</em>'.
	 * @see org.camel_dsl.camel.Action
	 * @generated
	 */
	EClass getAction();

	/**
	 * Returns the meta object for the attribute '{@link org.camel_dsl.camel.Action#getName <em>Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Name</em>'.
	 * @see org.camel_dsl.camel.Action#getName()
	 * @see #getAction()
	 * @generated
	 */
	EAttribute getAction_Name();

	/**
	 * Returns the meta object for the attribute '{@link org.camel_dsl.camel.Action#getType <em>Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Type</em>'.
	 * @see org.camel_dsl.camel.Action#getType()
	 * @see #getAction()
	 * @generated
	 */
	EAttribute getAction_Type();

	/**
	 * Returns the meta object for class '{@link org.camel_dsl.camel.Application <em>Application</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Application</em>'.
	 * @see org.camel_dsl.camel.Application
	 * @generated
	 */
	EClass getApplication();

	/**
	 * Returns the meta object for the attribute '{@link org.camel_dsl.camel.Application#getName <em>Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Name</em>'.
	 * @see org.camel_dsl.camel.Application#getName()
	 * @see #getApplication()
	 * @generated
	 */
	EAttribute getApplication_Name();

	/**
	 * Returns the meta object for the attribute '{@link org.camel_dsl.camel.Application#getVersion <em>Version</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Version</em>'.
	 * @see org.camel_dsl.camel.Application#getVersion()
	 * @see #getApplication()
	 * @generated
	 */
	EAttribute getApplication_Version();

	/**
	 * Returns the meta object for the attribute '{@link org.camel_dsl.camel.Application#getDescription <em>Description</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Description</em>'.
	 * @see org.camel_dsl.camel.Application#getDescription()
	 * @see #getApplication()
	 * @generated
	 */
	EAttribute getApplication_Description();

	/**
	 * Returns the meta object for the reference '{@link org.camel_dsl.camel.Application#getOwner <em>Owner</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Owner</em>'.
	 * @see org.camel_dsl.camel.Application#getOwner()
	 * @see #getApplication()
	 * @generated
	 */
	EReference getApplication_Owner();

	/**
	 * Returns the meta object for the reference list '{@link org.camel_dsl.camel.Application#getDeploymentModels <em>Deployment Models</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Deployment Models</em>'.
	 * @see org.camel_dsl.camel.Application#getDeploymentModels()
	 * @see #getApplication()
	 * @generated
	 */
	EReference getApplication_DeploymentModels();

	/**
	 * Returns the meta object for class '{@link org.camel_dsl.camel.MUSAAgentType <em>MUSA Agent Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>MUSA Agent Type</em>'.
	 * @see org.camel_dsl.camel.MUSAAgentType
	 * @generated
	 */
	EClass getMUSAAgentType();

	/**
	 * Returns the meta object for the attribute '{@link org.camel_dsl.camel.MUSAAgentType#getName <em>Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Name</em>'.
	 * @see org.camel_dsl.camel.MUSAAgentType#getName()
	 * @see #getMUSAAgentType()
	 * @generated
	 */
	EAttribute getMUSAAgentType_Name();

	/**
	 * Returns the meta object for class '{@link org.camel_dsl.camel.MUSAComponentType <em>MUSA Component Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>MUSA Component Type</em>'.
	 * @see org.camel_dsl.camel.MUSAComponentType
	 * @generated
	 */
	EClass getMUSAComponentType();

	/**
	 * Returns the meta object for the attribute '{@link org.camel_dsl.camel.MUSAComponentType#getName <em>Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Name</em>'.
	 * @see org.camel_dsl.camel.MUSAComponentType#getName()
	 * @see #getMUSAComponentType()
	 * @generated
	 */
	EAttribute getMUSAComponentType_Name();

	/**
	 * Returns the meta object for enum '{@link org.camel_dsl.camel.ActionType <em>Action Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for enum '<em>Action Type</em>'.
	 * @see org.camel_dsl.camel.ActionType
	 * @generated
	 */
	EEnum getActionType();

	/**
	 * Returns the meta object for enum '{@link org.camel_dsl.camel.LayerType <em>Layer Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for enum '<em>Layer Type</em>'.
	 * @see org.camel_dsl.camel.LayerType
	 * @generated
	 */
	EEnum getLayerType();

	/**
	 * Returns the meta object for enum '{@link org.camel_dsl.camel.COTSAServiceType <em>COTSA Service Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for enum '<em>COTSA Service Type</em>'.
	 * @see org.camel_dsl.camel.COTSAServiceType
	 * @generated
	 */
	EEnum getCOTSAServiceType();

	/**
	 * Returns the meta object for enum '{@link org.camel_dsl.camel.MUSAPoolType <em>MUSA Pool Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for enum '<em>MUSA Pool Type</em>'.
	 * @see org.camel_dsl.camel.MUSAPoolType
	 * @generated
	 */
	EEnum getMUSAPoolType();

	/**
	 * Returns the meta object for enum '{@link org.camel_dsl.camel.MUSAPolicyType <em>MUSA Policy Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for enum '<em>MUSA Policy Type</em>'.
	 * @see org.camel_dsl.camel.MUSAPolicyType
	 * @generated
	 */
	EEnum getMUSAPolicyType();

	/**
	 * Returns the meta object for enum '{@link org.camel_dsl.camel.MUSAContainerType <em>MUSA Container Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for enum '<em>MUSA Container Type</em>'.
	 * @see org.camel_dsl.camel.MUSAContainerType
	 * @generated
	 */
	EEnum getMUSAContainerType();

	/**
	 * Returns the meta object for enum '{@link org.camel_dsl.camel.MUSADockerType <em>MUSA Docker Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for enum '<em>MUSA Docker Type</em>'.
	 * @see org.camel_dsl.camel.MUSADockerType
	 * @generated
	 */
	EEnum getMUSADockerType();

	/**
	 * Returns the factory that creates the instances of the model.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the factory that creates the instances of the model.
	 * @generated
	 */
	CamelFactory getCamelFactory();

	/**
	 * <!-- begin-user-doc -->
	 * Defines literals for the meta objects that represent
	 * <ul>
	 *   <li>each class,</li>
	 *   <li>each feature of each class,</li>
	 *   <li>each operation of each class,</li>
	 *   <li>each enum,</li>
	 *   <li>and each data type</li>
	 * </ul>
	 * <!-- end-user-doc -->
	 * @generated
	 */
	interface Literals {
		/**
		 * The meta object literal for the '{@link org.camel_dsl.camel.impl.ModelImpl <em>Model</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.camel_dsl.camel.impl.ModelImpl
		 * @see org.camel_dsl.camel.impl.CamelPackageImpl#getModel()
		 * @generated
		 */
		EClass MODEL = eINSTANCE.getModel();

		/**
		 * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute MODEL__NAME = eINSTANCE.getModel_Name();

		/**
		 * The meta object literal for the '<em><b>Import URI</b></em>' attribute list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute MODEL__IMPORT_URI = eINSTANCE.getModel_ImportURI();

		/**
		 * The meta object literal for the '{@link org.camel_dsl.camel.impl.CamelModelImpl <em>Model</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.camel_dsl.camel.impl.CamelModelImpl
		 * @see org.camel_dsl.camel.impl.CamelPackageImpl#getCamelModel()
		 * @generated
		 */
		EClass CAMEL_MODEL = eINSTANCE.getCamelModel();

		/**
		 * The meta object literal for the '<em><b>Actions</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference CAMEL_MODEL__ACTIONS = eINSTANCE.getCamelModel_Actions();

		/**
		 * The meta object literal for the '<em><b>Applications</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference CAMEL_MODEL__APPLICATIONS = eINSTANCE.getCamelModel_Applications();

		/**
		 * The meta object literal for the '<em><b>Deployment Models</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference CAMEL_MODEL__DEPLOYMENT_MODELS = eINSTANCE.getCamelModel_DeploymentModels();

		/**
		 * The meta object literal for the '<em><b>Execution Models</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference CAMEL_MODEL__EXECUTION_MODELS = eINSTANCE.getCamelModel_ExecutionModels();

		/**
		 * The meta object literal for the '<em><b>Location Models</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference CAMEL_MODEL__LOCATION_MODELS = eINSTANCE.getCamelModel_LocationModels();

		/**
		 * The meta object literal for the '<em><b>Metric Models</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference CAMEL_MODEL__METRIC_MODELS = eINSTANCE.getCamelModel_MetricModels();

		/**
		 * The meta object literal for the '<em><b>Organisation Models</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference CAMEL_MODEL__ORGANISATION_MODELS = eINSTANCE.getCamelModel_OrganisationModels();

		/**
		 * The meta object literal for the '<em><b>Provider Models</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference CAMEL_MODEL__PROVIDER_MODELS = eINSTANCE.getCamelModel_ProviderModels();

		/**
		 * The meta object literal for the '<em><b>Requirement Models</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference CAMEL_MODEL__REQUIREMENT_MODELS = eINSTANCE.getCamelModel_RequirementModels();

		/**
		 * The meta object literal for the '<em><b>Scalability Models</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference CAMEL_MODEL__SCALABILITY_MODELS = eINSTANCE.getCamelModel_ScalabilityModels();

		/**
		 * The meta object literal for the '<em><b>Security Models</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference CAMEL_MODEL__SECURITY_MODELS = eINSTANCE.getCamelModel_SecurityModels();

		/**
		 * The meta object literal for the '<em><b>Type Models</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference CAMEL_MODEL__TYPE_MODELS = eINSTANCE.getCamelModel_TypeModels();

		/**
		 * The meta object literal for the '<em><b>Unit Models</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference CAMEL_MODEL__UNIT_MODELS = eINSTANCE.getCamelModel_UnitModels();

		/**
		 * The meta object literal for the '<em><b>Musa Component Types</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference CAMEL_MODEL__MUSA_COMPONENT_TYPES = eINSTANCE.getCamelModel_MusaComponentTypes();

		/**
		 * The meta object literal for the '<em><b>Musa Agent Types</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference CAMEL_MODEL__MUSA_AGENT_TYPES = eINSTANCE.getCamelModel_MusaAgentTypes();

		/**
		 * The meta object literal for the '{@link org.camel_dsl.camel.impl.ActionImpl <em>Action</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.camel_dsl.camel.impl.ActionImpl
		 * @see org.camel_dsl.camel.impl.CamelPackageImpl#getAction()
		 * @generated
		 */
		EClass ACTION = eINSTANCE.getAction();

		/**
		 * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ACTION__NAME = eINSTANCE.getAction_Name();

		/**
		 * The meta object literal for the '<em><b>Type</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ACTION__TYPE = eINSTANCE.getAction_Type();

		/**
		 * The meta object literal for the '{@link org.camel_dsl.camel.impl.ApplicationImpl <em>Application</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.camel_dsl.camel.impl.ApplicationImpl
		 * @see org.camel_dsl.camel.impl.CamelPackageImpl#getApplication()
		 * @generated
		 */
		EClass APPLICATION = eINSTANCE.getApplication();

		/**
		 * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute APPLICATION__NAME = eINSTANCE.getApplication_Name();

		/**
		 * The meta object literal for the '<em><b>Version</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute APPLICATION__VERSION = eINSTANCE.getApplication_Version();

		/**
		 * The meta object literal for the '<em><b>Description</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute APPLICATION__DESCRIPTION = eINSTANCE.getApplication_Description();

		/**
		 * The meta object literal for the '<em><b>Owner</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference APPLICATION__OWNER = eINSTANCE.getApplication_Owner();

		/**
		 * The meta object literal for the '<em><b>Deployment Models</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference APPLICATION__DEPLOYMENT_MODELS = eINSTANCE.getApplication_DeploymentModels();

		/**
		 * The meta object literal for the '{@link org.camel_dsl.camel.impl.MUSAAgentTypeImpl <em>MUSA Agent Type</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.camel_dsl.camel.impl.MUSAAgentTypeImpl
		 * @see org.camel_dsl.camel.impl.CamelPackageImpl#getMUSAAgentType()
		 * @generated
		 */
		EClass MUSA_AGENT_TYPE = eINSTANCE.getMUSAAgentType();

		/**
		 * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute MUSA_AGENT_TYPE__NAME = eINSTANCE.getMUSAAgentType_Name();

		/**
		 * The meta object literal for the '{@link org.camel_dsl.camel.impl.MUSAComponentTypeImpl <em>MUSA Component Type</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.camel_dsl.camel.impl.MUSAComponentTypeImpl
		 * @see org.camel_dsl.camel.impl.CamelPackageImpl#getMUSAComponentType()
		 * @generated
		 */
		EClass MUSA_COMPONENT_TYPE = eINSTANCE.getMUSAComponentType();

		/**
		 * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute MUSA_COMPONENT_TYPE__NAME = eINSTANCE.getMUSAComponentType_Name();

		/**
		 * The meta object literal for the '{@link org.camel_dsl.camel.ActionType <em>Action Type</em>}' enum.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.camel_dsl.camel.ActionType
		 * @see org.camel_dsl.camel.impl.CamelPackageImpl#getActionType()
		 * @generated
		 */
		EEnum ACTION_TYPE = eINSTANCE.getActionType();

		/**
		 * The meta object literal for the '{@link org.camel_dsl.camel.LayerType <em>Layer Type</em>}' enum.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.camel_dsl.camel.LayerType
		 * @see org.camel_dsl.camel.impl.CamelPackageImpl#getLayerType()
		 * @generated
		 */
		EEnum LAYER_TYPE = eINSTANCE.getLayerType();

		/**
		 * The meta object literal for the '{@link org.camel_dsl.camel.COTSAServiceType <em>COTSA Service Type</em>}' enum.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.camel_dsl.camel.COTSAServiceType
		 * @see org.camel_dsl.camel.impl.CamelPackageImpl#getCOTSAServiceType()
		 * @generated
		 */
		EEnum COTSA_SERVICE_TYPE = eINSTANCE.getCOTSAServiceType();

		/**
		 * The meta object literal for the '{@link org.camel_dsl.camel.MUSAPoolType <em>MUSA Pool Type</em>}' enum.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.camel_dsl.camel.MUSAPoolType
		 * @see org.camel_dsl.camel.impl.CamelPackageImpl#getMUSAPoolType()
		 * @generated
		 */
		EEnum MUSA_POOL_TYPE = eINSTANCE.getMUSAPoolType();

		/**
		 * The meta object literal for the '{@link org.camel_dsl.camel.MUSAPolicyType <em>MUSA Policy Type</em>}' enum.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.camel_dsl.camel.MUSAPolicyType
		 * @see org.camel_dsl.camel.impl.CamelPackageImpl#getMUSAPolicyType()
		 * @generated
		 */
		EEnum MUSA_POLICY_TYPE = eINSTANCE.getMUSAPolicyType();

		/**
		 * The meta object literal for the '{@link org.camel_dsl.camel.MUSAContainerType <em>MUSA Container Type</em>}' enum.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.camel_dsl.camel.MUSAContainerType
		 * @see org.camel_dsl.camel.impl.CamelPackageImpl#getMUSAContainerType()
		 * @generated
		 */
		EEnum MUSA_CONTAINER_TYPE = eINSTANCE.getMUSAContainerType();

		/**
		 * The meta object literal for the '{@link org.camel_dsl.camel.MUSADockerType <em>MUSA Docker Type</em>}' enum.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.camel_dsl.camel.MUSADockerType
		 * @see org.camel_dsl.camel.impl.CamelPackageImpl#getMUSADockerType()
		 * @generated
		 */
		EEnum MUSA_DOCKER_TYPE = eINSTANCE.getMUSADockerType();

	}

} //CamelPackage
