/**
 */
package org.camel_dsl.camel.impl;

import org.camel_dsl.camel.Action;
import org.camel_dsl.camel.Application;
import org.camel_dsl.camel.CamelModel;
import org.camel_dsl.camel.CamelPackage;
import org.camel_dsl.camel.MUSAAgentType;

import org.camel_dsl.deployment.DeploymentModel;

import org.camel_dsl.execution.ExecutionModel;

import org.camel_dsl.location.LocationModel;

import org.camel_dsl.metric.MetricModel;

import org.camel_dsl.organisation.OrganisationModel;

import org.camel_dsl.provider.ProviderModel;

import org.camel_dsl.requirement.RequirementModel;

import org.camel_dsl.scalability.ScalabilityModel;

import org.camel_dsl.security.SecurityModel;

import org.camel_dsl.type.TypeModel;

import org.camel_dsl.unit.UnitModel;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Model</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link org.camel_dsl.camel.impl.CamelModelImpl#getActions <em>Actions</em>}</li>
 *   <li>{@link org.camel_dsl.camel.impl.CamelModelImpl#getApplications <em>Applications</em>}</li>
 *   <li>{@link org.camel_dsl.camel.impl.CamelModelImpl#getDeploymentModels <em>Deployment Models</em>}</li>
 *   <li>{@link org.camel_dsl.camel.impl.CamelModelImpl#getExecutionModels <em>Execution Models</em>}</li>
 *   <li>{@link org.camel_dsl.camel.impl.CamelModelImpl#getLocationModels <em>Location Models</em>}</li>
 *   <li>{@link org.camel_dsl.camel.impl.CamelModelImpl#getMetricModels <em>Metric Models</em>}</li>
 *   <li>{@link org.camel_dsl.camel.impl.CamelModelImpl#getOrganisationModels <em>Organisation Models</em>}</li>
 *   <li>{@link org.camel_dsl.camel.impl.CamelModelImpl#getProviderModels <em>Provider Models</em>}</li>
 *   <li>{@link org.camel_dsl.camel.impl.CamelModelImpl#getRequirementModels <em>Requirement Models</em>}</li>
 *   <li>{@link org.camel_dsl.camel.impl.CamelModelImpl#getScalabilityModels <em>Scalability Models</em>}</li>
 *   <li>{@link org.camel_dsl.camel.impl.CamelModelImpl#getSecurityModels <em>Security Models</em>}</li>
 *   <li>{@link org.camel_dsl.camel.impl.CamelModelImpl#getTypeModels <em>Type Models</em>}</li>
 *   <li>{@link org.camel_dsl.camel.impl.CamelModelImpl#getUnitModels <em>Unit Models</em>}</li>
 *   <li>{@link org.camel_dsl.camel.impl.CamelModelImpl#getMusaComponentTypes <em>Musa Component Types</em>}</li>
 *   <li>{@link org.camel_dsl.camel.impl.CamelModelImpl#getMusaAgentTypes <em>Musa Agent Types</em>}</li>
 * </ul>
 *
 * @generated
 */
public class CamelModelImpl extends ModelImpl implements CamelModel {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected CamelModelImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return CamelPackage.Literals.CAMEL_MODEL;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	public EList<Action> getActions() {
		return (EList<Action>)eGet(CamelPackage.Literals.CAMEL_MODEL__ACTIONS, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	public EList<Application> getApplications() {
		return (EList<Application>)eGet(CamelPackage.Literals.CAMEL_MODEL__APPLICATIONS, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	public EList<DeploymentModel> getDeploymentModels() {
		return (EList<DeploymentModel>)eGet(CamelPackage.Literals.CAMEL_MODEL__DEPLOYMENT_MODELS, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	public EList<ExecutionModel> getExecutionModels() {
		return (EList<ExecutionModel>)eGet(CamelPackage.Literals.CAMEL_MODEL__EXECUTION_MODELS, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	public EList<LocationModel> getLocationModels() {
		return (EList<LocationModel>)eGet(CamelPackage.Literals.CAMEL_MODEL__LOCATION_MODELS, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	public EList<MetricModel> getMetricModels() {
		return (EList<MetricModel>)eGet(CamelPackage.Literals.CAMEL_MODEL__METRIC_MODELS, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	public EList<OrganisationModel> getOrganisationModels() {
		return (EList<OrganisationModel>)eGet(CamelPackage.Literals.CAMEL_MODEL__ORGANISATION_MODELS, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	public EList<ProviderModel> getProviderModels() {
		return (EList<ProviderModel>)eGet(CamelPackage.Literals.CAMEL_MODEL__PROVIDER_MODELS, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	public EList<RequirementModel> getRequirementModels() {
		return (EList<RequirementModel>)eGet(CamelPackage.Literals.CAMEL_MODEL__REQUIREMENT_MODELS, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	public EList<ScalabilityModel> getScalabilityModels() {
		return (EList<ScalabilityModel>)eGet(CamelPackage.Literals.CAMEL_MODEL__SCALABILITY_MODELS, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	public EList<SecurityModel> getSecurityModels() {
		return (EList<SecurityModel>)eGet(CamelPackage.Literals.CAMEL_MODEL__SECURITY_MODELS, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	public EList<TypeModel> getTypeModels() {
		return (EList<TypeModel>)eGet(CamelPackage.Literals.CAMEL_MODEL__TYPE_MODELS, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	public EList<UnitModel> getUnitModels() {
		return (EList<UnitModel>)eGet(CamelPackage.Literals.CAMEL_MODEL__UNIT_MODELS, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	public EList<MUSAAgentType> getMusaComponentTypes() {
		return (EList<MUSAAgentType>)eGet(CamelPackage.Literals.CAMEL_MODEL__MUSA_COMPONENT_TYPES, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	public EList<MUSAAgentType> getMusaAgentTypes() {
		return (EList<MUSAAgentType>)eGet(CamelPackage.Literals.CAMEL_MODEL__MUSA_AGENT_TYPES, true);
	}

} //CamelModelImpl
