/**
 */
package org.camel_dsl.camel.impl;

import org.camel_dsl.camel.*;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EDataType;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;

import org.eclipse.emf.ecore.impl.EFactoryImpl;

import org.eclipse.emf.ecore.plugin.EcorePlugin;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Factory</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class CamelFactoryImpl extends EFactoryImpl implements CamelFactory {
	/**
	 * Creates the default factory implementation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static CamelFactory init() {
		try {
			CamelFactory theCamelFactory = (CamelFactory)EPackage.Registry.INSTANCE.getEFactory(CamelPackage.eNS_URI);
			if (theCamelFactory != null) {
				return theCamelFactory;
			}
		}
		catch (Exception exception) {
			EcorePlugin.INSTANCE.log(exception);
		}
		return new CamelFactoryImpl();
	}

	/**
	 * Creates an instance of the factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public CamelFactoryImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EObject create(EClass eClass) {
		switch (eClass.getClassifierID()) {
			case CamelPackage.CAMEL_MODEL: return (EObject)createCamelModel();
			case CamelPackage.ACTION: return (EObject)createAction();
			case CamelPackage.APPLICATION: return (EObject)createApplication();
			case CamelPackage.MUSA_AGENT_TYPE: return (EObject)createMUSAAgentType();
			case CamelPackage.MUSA_COMPONENT_TYPE: return (EObject)createMUSAComponentType();
			default:
				throw new IllegalArgumentException("The class '" + eClass.getName() + "' is not a valid classifier");
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object createFromString(EDataType eDataType, String initialValue) {
		switch (eDataType.getClassifierID()) {
			case CamelPackage.ACTION_TYPE:
				return createActionTypeFromString(eDataType, initialValue);
			case CamelPackage.LAYER_TYPE:
				return createLayerTypeFromString(eDataType, initialValue);
			case CamelPackage.COTSA_SERVICE_TYPE:
				return createCOTSAServiceTypeFromString(eDataType, initialValue);
			case CamelPackage.MUSA_POOL_TYPE:
				return createMUSAPoolTypeFromString(eDataType, initialValue);
			case CamelPackage.MUSA_POLICY_TYPE:
				return createMUSAPolicyTypeFromString(eDataType, initialValue);
			case CamelPackage.MUSA_CONTAINER_TYPE:
				return createMUSAContainerTypeFromString(eDataType, initialValue);
			case CamelPackage.MUSA_DOCKER_TYPE:
				return createMUSADockerTypeFromString(eDataType, initialValue);
			default:
				throw new IllegalArgumentException("The datatype '" + eDataType.getName() + "' is not a valid classifier");
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String convertToString(EDataType eDataType, Object instanceValue) {
		switch (eDataType.getClassifierID()) {
			case CamelPackage.ACTION_TYPE:
				return convertActionTypeToString(eDataType, instanceValue);
			case CamelPackage.LAYER_TYPE:
				return convertLayerTypeToString(eDataType, instanceValue);
			case CamelPackage.COTSA_SERVICE_TYPE:
				return convertCOTSAServiceTypeToString(eDataType, instanceValue);
			case CamelPackage.MUSA_POOL_TYPE:
				return convertMUSAPoolTypeToString(eDataType, instanceValue);
			case CamelPackage.MUSA_POLICY_TYPE:
				return convertMUSAPolicyTypeToString(eDataType, instanceValue);
			case CamelPackage.MUSA_CONTAINER_TYPE:
				return convertMUSAContainerTypeToString(eDataType, instanceValue);
			case CamelPackage.MUSA_DOCKER_TYPE:
				return convertMUSADockerTypeToString(eDataType, instanceValue);
			default:
				throw new IllegalArgumentException("The datatype '" + eDataType.getName() + "' is not a valid classifier");
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public CamelModel createCamelModel() {
		CamelModelImpl camelModel = new CamelModelImpl();
		return camelModel;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Action createAction() {
		ActionImpl action = new ActionImpl();
		return action;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Application createApplication() {
		ApplicationImpl application = new ApplicationImpl();
		return application;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MUSAAgentType createMUSAAgentType() {
		MUSAAgentTypeImpl musaAgentType = new MUSAAgentTypeImpl();
		return musaAgentType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MUSAComponentType createMUSAComponentType() {
		MUSAComponentTypeImpl musaComponentType = new MUSAComponentTypeImpl();
		return musaComponentType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ActionType createActionTypeFromString(EDataType eDataType, String initialValue) {
		ActionType result = ActionType.get(initialValue);
		if (result == null) throw new IllegalArgumentException("The value '" + initialValue + "' is not a valid enumerator of '" + eDataType.getName() + "'");
		return result;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertActionTypeToString(EDataType eDataType, Object instanceValue) {
		return instanceValue == null ? null : instanceValue.toString();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public LayerType createLayerTypeFromString(EDataType eDataType, String initialValue) {
		LayerType result = LayerType.get(initialValue);
		if (result == null) throw new IllegalArgumentException("The value '" + initialValue + "' is not a valid enumerator of '" + eDataType.getName() + "'");
		return result;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertLayerTypeToString(EDataType eDataType, Object instanceValue) {
		return instanceValue == null ? null : instanceValue.toString();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public COTSAServiceType createCOTSAServiceTypeFromString(EDataType eDataType, String initialValue) {
		COTSAServiceType result = COTSAServiceType.get(initialValue);
		if (result == null) throw new IllegalArgumentException("The value '" + initialValue + "' is not a valid enumerator of '" + eDataType.getName() + "'");
		return result;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertCOTSAServiceTypeToString(EDataType eDataType, Object instanceValue) {
		return instanceValue == null ? null : instanceValue.toString();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MUSAPoolType createMUSAPoolTypeFromString(EDataType eDataType, String initialValue) {
		MUSAPoolType result = MUSAPoolType.get(initialValue);
		if (result == null) throw new IllegalArgumentException("The value '" + initialValue + "' is not a valid enumerator of '" + eDataType.getName() + "'");
		return result;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertMUSAPoolTypeToString(EDataType eDataType, Object instanceValue) {
		return instanceValue == null ? null : instanceValue.toString();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MUSAPolicyType createMUSAPolicyTypeFromString(EDataType eDataType, String initialValue) {
		MUSAPolicyType result = MUSAPolicyType.get(initialValue);
		if (result == null) throw new IllegalArgumentException("The value '" + initialValue + "' is not a valid enumerator of '" + eDataType.getName() + "'");
		return result;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertMUSAPolicyTypeToString(EDataType eDataType, Object instanceValue) {
		return instanceValue == null ? null : instanceValue.toString();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MUSAContainerType createMUSAContainerTypeFromString(EDataType eDataType, String initialValue) {
		MUSAContainerType result = MUSAContainerType.get(initialValue);
		if (result == null) throw new IllegalArgumentException("The value '" + initialValue + "' is not a valid enumerator of '" + eDataType.getName() + "'");
		return result;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertMUSAContainerTypeToString(EDataType eDataType, Object instanceValue) {
		return instanceValue == null ? null : instanceValue.toString();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MUSADockerType createMUSADockerTypeFromString(EDataType eDataType, String initialValue) {
		MUSADockerType result = MUSADockerType.get(initialValue);
		if (result == null) throw new IllegalArgumentException("The value '" + initialValue + "' is not a valid enumerator of '" + eDataType.getName() + "'");
		return result;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertMUSADockerTypeToString(EDataType eDataType, Object instanceValue) {
		return instanceValue == null ? null : instanceValue.toString();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public CamelPackage getCamelPackage() {
		return (CamelPackage)getEPackage();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @deprecated
	 * @generated
	 */
	@Deprecated
	public static CamelPackage getPackage() {
		return CamelPackage.eINSTANCE;
	}

} //CamelFactoryImpl
