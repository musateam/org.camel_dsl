/**
 */
package org.camel_dsl.camel;

import org.camel_dsl.deployment.DeploymentModel;

import org.camel_dsl.execution.ExecutionModel;

import org.camel_dsl.location.LocationModel;

import org.camel_dsl.metric.MetricModel;

import org.camel_dsl.organisation.OrganisationModel;

import org.camel_dsl.provider.ProviderModel;

import org.camel_dsl.requirement.RequirementModel;

import org.camel_dsl.scalability.ScalabilityModel;

import org.camel_dsl.security.SecurityModel;

import org.camel_dsl.type.TypeModel;

import org.camel_dsl.unit.UnitModel;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Model</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.camel_dsl.camel.CamelModel#getActions <em>Actions</em>}</li>
 *   <li>{@link org.camel_dsl.camel.CamelModel#getApplications <em>Applications</em>}</li>
 *   <li>{@link org.camel_dsl.camel.CamelModel#getDeploymentModels <em>Deployment Models</em>}</li>
 *   <li>{@link org.camel_dsl.camel.CamelModel#getExecutionModels <em>Execution Models</em>}</li>
 *   <li>{@link org.camel_dsl.camel.CamelModel#getLocationModels <em>Location Models</em>}</li>
 *   <li>{@link org.camel_dsl.camel.CamelModel#getMetricModels <em>Metric Models</em>}</li>
 *   <li>{@link org.camel_dsl.camel.CamelModel#getOrganisationModels <em>Organisation Models</em>}</li>
 *   <li>{@link org.camel_dsl.camel.CamelModel#getProviderModels <em>Provider Models</em>}</li>
 *   <li>{@link org.camel_dsl.camel.CamelModel#getRequirementModels <em>Requirement Models</em>}</li>
 *   <li>{@link org.camel_dsl.camel.CamelModel#getScalabilityModels <em>Scalability Models</em>}</li>
 *   <li>{@link org.camel_dsl.camel.CamelModel#getSecurityModels <em>Security Models</em>}</li>
 *   <li>{@link org.camel_dsl.camel.CamelModel#getTypeModels <em>Type Models</em>}</li>
 *   <li>{@link org.camel_dsl.camel.CamelModel#getUnitModels <em>Unit Models</em>}</li>
 *   <li>{@link org.camel_dsl.camel.CamelModel#getMusaComponentTypes <em>Musa Component Types</em>}</li>
 *   <li>{@link org.camel_dsl.camel.CamelModel#getMusaAgentTypes <em>Musa Agent Types</em>}</li>
 * </ul>
 *
 * @see org.camel_dsl.camel.CamelPackage#getCamelModel()
 * @model
 * @generated
 */
public interface CamelModel extends Model {
	/**
	 * Returns the value of the '<em><b>Actions</b></em>' containment reference list.
	 * The list contents are of type {@link org.camel_dsl.camel.Action}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Actions</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Actions</em>' containment reference list.
	 * @see org.camel_dsl.camel.CamelPackage#getCamelModel_Actions()
	 * @model containment="true" ordered="false"
	 * @generated
	 */
	EList<Action> getActions();

	/**
	 * Returns the value of the '<em><b>Applications</b></em>' containment reference list.
	 * The list contents are of type {@link org.camel_dsl.camel.Application}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Applications</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Applications</em>' containment reference list.
	 * @see org.camel_dsl.camel.CamelPackage#getCamelModel_Applications()
	 * @model containment="true" ordered="false"
	 * @generated
	 */
	EList<Application> getApplications();

	/**
	 * Returns the value of the '<em><b>Deployment Models</b></em>' containment reference list.
	 * The list contents are of type {@link org.camel_dsl.deployment.DeploymentModel}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Deployment Models</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Deployment Models</em>' containment reference list.
	 * @see org.camel_dsl.camel.CamelPackage#getCamelModel_DeploymentModels()
	 * @model containment="true" ordered="false"
	 * @generated
	 */
	EList<DeploymentModel> getDeploymentModels();

	/**
	 * Returns the value of the '<em><b>Execution Models</b></em>' containment reference list.
	 * The list contents are of type {@link org.camel_dsl.execution.ExecutionModel}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Execution Models</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Execution Models</em>' containment reference list.
	 * @see org.camel_dsl.camel.CamelPackage#getCamelModel_ExecutionModels()
	 * @model containment="true" ordered="false"
	 * @generated
	 */
	EList<ExecutionModel> getExecutionModels();

	/**
	 * Returns the value of the '<em><b>Location Models</b></em>' containment reference list.
	 * The list contents are of type {@link org.camel_dsl.location.LocationModel}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Location Models</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Location Models</em>' containment reference list.
	 * @see org.camel_dsl.camel.CamelPackage#getCamelModel_LocationModels()
	 * @model containment="true"
	 * @generated
	 */
	EList<LocationModel> getLocationModels();

	/**
	 * Returns the value of the '<em><b>Metric Models</b></em>' containment reference list.
	 * The list contents are of type {@link org.camel_dsl.metric.MetricModel}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Metric Models</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Metric Models</em>' containment reference list.
	 * @see org.camel_dsl.camel.CamelPackage#getCamelModel_MetricModels()
	 * @model containment="true"
	 * @generated
	 */
	EList<MetricModel> getMetricModels();

	/**
	 * Returns the value of the '<em><b>Organisation Models</b></em>' containment reference list.
	 * The list contents are of type {@link org.camel_dsl.organisation.OrganisationModel}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Organisation Models</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Organisation Models</em>' containment reference list.
	 * @see org.camel_dsl.camel.CamelPackage#getCamelModel_OrganisationModels()
	 * @model containment="true" ordered="false"
	 * @generated
	 */
	EList<OrganisationModel> getOrganisationModels();

	/**
	 * Returns the value of the '<em><b>Provider Models</b></em>' containment reference list.
	 * The list contents are of type {@link org.camel_dsl.provider.ProviderModel}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Provider Models</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Provider Models</em>' containment reference list.
	 * @see org.camel_dsl.camel.CamelPackage#getCamelModel_ProviderModels()
	 * @model containment="true" ordered="false"
	 * @generated
	 */
	EList<ProviderModel> getProviderModels();

	/**
	 * Returns the value of the '<em><b>Requirement Models</b></em>' containment reference list.
	 * The list contents are of type {@link org.camel_dsl.requirement.RequirementModel}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Requirement Models</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Requirement Models</em>' containment reference list.
	 * @see org.camel_dsl.camel.CamelPackage#getCamelModel_RequirementModels()
	 * @model containment="true"
	 * @generated
	 */
	EList<RequirementModel> getRequirementModels();

	/**
	 * Returns the value of the '<em><b>Scalability Models</b></em>' containment reference list.
	 * The list contents are of type {@link org.camel_dsl.scalability.ScalabilityModel}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Scalability Models</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Scalability Models</em>' containment reference list.
	 * @see org.camel_dsl.camel.CamelPackage#getCamelModel_ScalabilityModels()
	 * @model containment="true" ordered="false"
	 * @generated
	 */
	EList<ScalabilityModel> getScalabilityModels();

	/**
	 * Returns the value of the '<em><b>Security Models</b></em>' containment reference list.
	 * The list contents are of type {@link org.camel_dsl.security.SecurityModel}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Security Models</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Security Models</em>' containment reference list.
	 * @see org.camel_dsl.camel.CamelPackage#getCamelModel_SecurityModels()
	 * @model containment="true" ordered="false"
	 * @generated
	 */
	EList<SecurityModel> getSecurityModels();

	/**
	 * Returns the value of the '<em><b>Type Models</b></em>' containment reference list.
	 * The list contents are of type {@link org.camel_dsl.type.TypeModel}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Type Models</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Type Models</em>' containment reference list.
	 * @see org.camel_dsl.camel.CamelPackage#getCamelModel_TypeModels()
	 * @model containment="true" ordered="false"
	 * @generated
	 */
	EList<TypeModel> getTypeModels();

	/**
	 * Returns the value of the '<em><b>Unit Models</b></em>' containment reference list.
	 * The list contents are of type {@link org.camel_dsl.unit.UnitModel}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Unit Models</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Unit Models</em>' containment reference list.
	 * @see org.camel_dsl.camel.CamelPackage#getCamelModel_UnitModels()
	 * @model containment="true" ordered="false"
	 * @generated
	 */
	EList<UnitModel> getUnitModels();

	/**
	 * Returns the value of the '<em><b>Musa Component Types</b></em>' containment reference list.
	 * The list contents are of type {@link org.camel_dsl.camel.MUSAAgentType}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Musa Component Types</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Musa Component Types</em>' containment reference list.
	 * @see org.camel_dsl.camel.CamelPackage#getCamelModel_MusaComponentTypes()
	 * @model containment="true" ordered="false"
	 * @generated
	 */
	EList<MUSAAgentType> getMusaComponentTypes();

	/**
	 * Returns the value of the '<em><b>Musa Agent Types</b></em>' containment reference list.
	 * The list contents are of type {@link org.camel_dsl.camel.MUSAAgentType}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Musa Agent Types</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Musa Agent Types</em>' containment reference list.
	 * @see org.camel_dsl.camel.CamelPackage#getCamelModel_MusaAgentTypes()
	 * @model containment="true" ordered="false"
	 * @generated
	 */
	EList<MUSAAgentType> getMusaAgentTypes();

} // CamelModel
