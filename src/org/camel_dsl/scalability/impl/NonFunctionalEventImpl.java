/**
 */
package org.camel_dsl.scalability.impl;

import org.camel_dsl.metric.MetricCondition;

import org.camel_dsl.scalability.NonFunctionalEvent;
import org.camel_dsl.scalability.ScalabilityPackage;

import org.eclipse.emf.ecore.EClass;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Non Functional Event</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link org.camel_dsl.scalability.impl.NonFunctionalEventImpl#getMetricCondition <em>Metric Condition</em>}</li>
 *   <li>{@link org.camel_dsl.scalability.impl.NonFunctionalEventImpl#isIsViolation <em>Is Violation</em>}</li>
 * </ul>
 *
 * @generated
 */
public class NonFunctionalEventImpl extends SimpleEventImpl implements NonFunctionalEvent {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected NonFunctionalEventImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return ScalabilityPackage.Literals.NON_FUNCTIONAL_EVENT;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MetricCondition getMetricCondition() {
		return (MetricCondition)eGet(ScalabilityPackage.Literals.NON_FUNCTIONAL_EVENT__METRIC_CONDITION, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setMetricCondition(MetricCondition newMetricCondition) {
		eSet(ScalabilityPackage.Literals.NON_FUNCTIONAL_EVENT__METRIC_CONDITION, newMetricCondition);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isIsViolation() {
		return (Boolean)eGet(ScalabilityPackage.Literals.NON_FUNCTIONAL_EVENT__IS_VIOLATION, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setIsViolation(boolean newIsViolation) {
		eSet(ScalabilityPackage.Literals.NON_FUNCTIONAL_EVENT__IS_VIOLATION, newIsViolation);
	}

} //NonFunctionalEventImpl
