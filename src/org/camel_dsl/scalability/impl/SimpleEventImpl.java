/**
 */
package org.camel_dsl.scalability.impl;

import org.camel_dsl.scalability.ScalabilityPackage;
import org.camel_dsl.scalability.SimpleEvent;

import org.eclipse.emf.ecore.EClass;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Simple Event</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class SimpleEventImpl extends EventImpl implements SimpleEvent {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected SimpleEventImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return ScalabilityPackage.Literals.SIMPLE_EVENT;
	}

} //SimpleEventImpl
