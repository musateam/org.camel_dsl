/**
 */
package org.camel_dsl.scalability.impl;

import org.camel_dsl.camel.impl.ModelImpl;

import org.camel_dsl.requirement.ScaleRequirement;

import org.camel_dsl.scalability.Event;
import org.camel_dsl.scalability.EventInstance;
import org.camel_dsl.scalability.EventPattern;
import org.camel_dsl.scalability.ScalabilityModel;
import org.camel_dsl.scalability.ScalabilityPackage;
import org.camel_dsl.scalability.ScalabilityRule;
import org.camel_dsl.scalability.ScalingAction;
import org.camel_dsl.scalability.Timer;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Model</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link org.camel_dsl.scalability.impl.ScalabilityModelImpl#getRules <em>Rules</em>}</li>
 *   <li>{@link org.camel_dsl.scalability.impl.ScalabilityModelImpl#getEvents <em>Events</em>}</li>
 *   <li>{@link org.camel_dsl.scalability.impl.ScalabilityModelImpl#getEventInstances <em>Event Instances</em>}</li>
 *   <li>{@link org.camel_dsl.scalability.impl.ScalabilityModelImpl#getActions <em>Actions</em>}</li>
 *   <li>{@link org.camel_dsl.scalability.impl.ScalabilityModelImpl#getPatterns <em>Patterns</em>}</li>
 *   <li>{@link org.camel_dsl.scalability.impl.ScalabilityModelImpl#getTimers <em>Timers</em>}</li>
 *   <li>{@link org.camel_dsl.scalability.impl.ScalabilityModelImpl#getScaleRequirements <em>Scale Requirements</em>}</li>
 * </ul>
 *
 * @generated
 */
public class ScalabilityModelImpl extends ModelImpl implements ScalabilityModel {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ScalabilityModelImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return ScalabilityPackage.Literals.SCALABILITY_MODEL;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	public EList<ScalabilityRule> getRules() {
		return (EList<ScalabilityRule>)eGet(ScalabilityPackage.Literals.SCALABILITY_MODEL__RULES, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	public EList<Event> getEvents() {
		return (EList<Event>)eGet(ScalabilityPackage.Literals.SCALABILITY_MODEL__EVENTS, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	public EList<EventInstance> getEventInstances() {
		return (EList<EventInstance>)eGet(ScalabilityPackage.Literals.SCALABILITY_MODEL__EVENT_INSTANCES, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	public EList<ScalingAction> getActions() {
		return (EList<ScalingAction>)eGet(ScalabilityPackage.Literals.SCALABILITY_MODEL__ACTIONS, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	public EList<EventPattern> getPatterns() {
		return (EList<EventPattern>)eGet(ScalabilityPackage.Literals.SCALABILITY_MODEL__PATTERNS, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	public EList<Timer> getTimers() {
		return (EList<Timer>)eGet(ScalabilityPackage.Literals.SCALABILITY_MODEL__TIMERS, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	public EList<ScaleRequirement> getScaleRequirements() {
		return (EList<ScaleRequirement>)eGet(ScalabilityPackage.Literals.SCALABILITY_MODEL__SCALE_REQUIREMENTS, true);
	}

} //ScalabilityModelImpl
