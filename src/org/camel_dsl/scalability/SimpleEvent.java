/**
 */
package org.camel_dsl.scalability;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Simple Event</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see org.camel_dsl.scalability.ScalabilityPackage#getSimpleEvent()
 * @model
 * @generated
 */
public interface SimpleEvent extends Event {
} // SimpleEvent
