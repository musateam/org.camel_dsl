/**
 */
package org.camel_dsl.scalability;

import org.camel_dsl.camel.Action;

import org.camel_dsl.deployment.VM;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Scaling Action</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.camel_dsl.scalability.ScalingAction#getVm <em>Vm</em>}</li>
 * </ul>
 *
 * @see org.camel_dsl.scalability.ScalabilityPackage#getScalingAction()
 * @model abstract="true"
 * @generated
 */
public interface ScalingAction extends Action {
	/**
	 * Returns the value of the '<em><b>Vm</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Vm</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Vm</em>' reference.
	 * @see #setVm(VM)
	 * @see org.camel_dsl.scalability.ScalabilityPackage#getScalingAction_Vm()
	 * @model required="true"
	 * @generated
	 */
	VM getVm();

	/**
	 * Sets the value of the '{@link org.camel_dsl.scalability.ScalingAction#getVm <em>Vm</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Vm</em>' reference.
	 * @see #getVm()
	 * @generated
	 */
	void setVm(VM value);

} // ScalingAction
