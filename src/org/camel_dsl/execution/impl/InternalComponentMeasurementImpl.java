/**
 */
package org.camel_dsl.execution.impl;

import org.camel_dsl.deployment.InternalComponentInstance;

import org.camel_dsl.execution.ExecutionPackage;
import org.camel_dsl.execution.InternalComponentMeasurement;

import org.eclipse.emf.ecore.EClass;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Internal Component Measurement</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link org.camel_dsl.execution.impl.InternalComponentMeasurementImpl#getInternalComponentInstance <em>Internal Component Instance</em>}</li>
 * </ul>
 *
 * @generated
 */
public class InternalComponentMeasurementImpl extends MeasurementImpl implements InternalComponentMeasurement {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected InternalComponentMeasurementImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return ExecutionPackage.Literals.INTERNAL_COMPONENT_MEASUREMENT;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public InternalComponentInstance getInternalComponentInstance() {
		return (InternalComponentInstance)eGet(ExecutionPackage.Literals.INTERNAL_COMPONENT_MEASUREMENT__INTERNAL_COMPONENT_INSTANCE, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setInternalComponentInstance(InternalComponentInstance newInternalComponentInstance) {
		eSet(ExecutionPackage.Literals.INTERNAL_COMPONENT_MEASUREMENT__INTERNAL_COMPONENT_INSTANCE, newInternalComponentInstance);
	}

} //InternalComponentMeasurementImpl
