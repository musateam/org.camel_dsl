/**
 */
package org.camel_dsl.security;

import org.eclipse.emf.cdo.CDOObject;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Domain</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.camel_dsl.security.SecurityDomain#getId <em>Id</em>}</li>
 *   <li>{@link org.camel_dsl.security.SecurityDomain#getName <em>Name</em>}</li>
 *   <li>{@link org.camel_dsl.security.SecurityDomain#getSubDomain <em>Sub Domain</em>}</li>
 * </ul>
 *
 * @see org.camel_dsl.security.SecurityPackage#getSecurityDomain()
 * @model
 * @extends CDOObject
 * @generated
 */
public interface SecurityDomain extends CDOObject {
	/**
	 * Returns the value of the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Id</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Id</em>' attribute.
	 * @see #setId(String)
	 * @see org.camel_dsl.security.SecurityPackage#getSecurityDomain_Id()
	 * @model required="true"
	 * @generated
	 */
	String getId();

	/**
	 * Sets the value of the '{@link org.camel_dsl.security.SecurityDomain#getId <em>Id</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Id</em>' attribute.
	 * @see #getId()
	 * @generated
	 */
	void setId(String value);

	/**
	 * Returns the value of the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Name</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Name</em>' attribute.
	 * @see #setName(String)
	 * @see org.camel_dsl.security.SecurityPackage#getSecurityDomain_Name()
	 * @model required="true"
	 *        annotation="http://schema.omg.org/spec/MOF/2.0/emof.xml#Property.oppositeRoleName body='Limit'"
	 *        annotation="http://schema.omg.org/spec/MOF/2.0/emof.xml#Property.oppositeRoleName body='Limit'"
	 * @generated
	 */
	String getName();

	/**
	 * Sets the value of the '{@link org.camel_dsl.security.SecurityDomain#getName <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Name</em>' attribute.
	 * @see #getName()
	 * @generated
	 */
	void setName(String value);

	/**
	 * Returns the value of the '<em><b>Sub Domain</b></em>' reference list.
	 * The list contents are of type {@link org.camel_dsl.security.SecurityDomain}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Sub Domain</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Sub Domain</em>' reference list.
	 * @see org.camel_dsl.security.SecurityPackage#getSecurityDomain_SubDomain()
	 * @model ordered="false"
	 * @generated
	 */
	EList<SecurityDomain> getSubDomain();

} // SecurityDomain
