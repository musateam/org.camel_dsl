/**
 */
package org.camel_dsl.security.util;

import org.camel_dsl.camel.Model;

import org.camel_dsl.metric.CompositeMetric;
import org.camel_dsl.metric.CompositeMetricInstance;
import org.camel_dsl.metric.Metric;
import org.camel_dsl.metric.MetricFormulaParameter;
import org.camel_dsl.metric.MetricInstance;
import org.camel_dsl.metric.Property;
import org.camel_dsl.metric.RawMetric;
import org.camel_dsl.metric.RawMetricInstance;

import org.camel_dsl.requirement.HardRequirement;
import org.camel_dsl.requirement.Requirement;
import org.camel_dsl.requirement.ServiceLevelObjective;

import org.camel_dsl.security.*;

import org.eclipse.emf.common.notify.Adapter;
import org.eclipse.emf.common.notify.Notifier;

import org.eclipse.emf.common.notify.impl.AdapterFactoryImpl;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * The <b>Adapter Factory</b> for the model.
 * It provides an adapter <code>createXXX</code> method for each class of the model.
 * <!-- end-user-doc -->
 * @see org.camel_dsl.security.SecurityPackage
 * @generated
 */
public class SecurityAdapterFactory extends AdapterFactoryImpl {
	/**
	 * The cached model package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected static SecurityPackage modelPackage;

	/**
	 * Creates an instance of the adapter factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SecurityAdapterFactory() {
		if (modelPackage == null) {
			modelPackage = SecurityPackage.eINSTANCE;
		}
	}

	/**
	 * Returns whether this factory is applicable for the type of the object.
	 * <!-- begin-user-doc -->
	 * This implementation returns <code>true</code> if the object is either the model's package or is an instance object of the model.
	 * <!-- end-user-doc -->
	 * @return whether this factory is applicable for the type of the object.
	 * @generated
	 */
	@Override
	public boolean isFactoryForType(Object object) {
		if (object == modelPackage) {
			return true;
		}
		if (object instanceof EObject) {
			return ((EObject)object).eClass().getEPackage() == modelPackage;
		}
		return false;
	}

	/**
	 * The switch that delegates to the <code>createXXX</code> methods.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected SecuritySwitch<Adapter> modelSwitch =
		new SecuritySwitch<Adapter>() {
			@Override
			public Adapter caseSecurityModel(SecurityModel object) {
				return createSecurityModelAdapter();
			}
			@Override
			public Adapter caseSecurityDomain(SecurityDomain object) {
				return createSecurityDomainAdapter();
			}
			@Override
			public Adapter caseSecurityControl(SecurityControl object) {
				return createSecurityControlAdapter();
			}
			@Override
			public Adapter caseRawSecurityMetricInstance(RawSecurityMetricInstance object) {
				return createRawSecurityMetricInstanceAdapter();
			}
			@Override
			public Adapter caseRawSecurityMetric(RawSecurityMetric object) {
				return createRawSecurityMetricAdapter();
			}
			@Override
			public Adapter caseSecurityProperty(SecurityProperty object) {
				return createSecurityPropertyAdapter();
			}
			@Override
			public Adapter caseCertifiable(Certifiable object) {
				return createCertifiableAdapter();
			}
			@Override
			public Adapter caseSecuritySLO(SecuritySLO object) {
				return createSecuritySLOAdapter();
			}
			@Override
			public Adapter caseSecurityCapability(SecurityCapability object) {
				return createSecurityCapabilityAdapter();
			}
			@Override
			public Adapter caseCompositeSecurityMetric(CompositeSecurityMetric object) {
				return createCompositeSecurityMetricAdapter();
			}
			@Override
			public Adapter caseCompositeSecurityMetricInstance(CompositeSecurityMetricInstance object) {
				return createCompositeSecurityMetricInstanceAdapter();
			}
			@Override
			public Adapter caseMUSASecurityControl(MUSASecurityControl object) {
				return createMUSASecurityControlAdapter();
			}
			@Override
			public Adapter caseMUSASecurityCapability(MUSASecurityCapability object) {
				return createMUSASecurityCapabilityAdapter();
			}
			@Override
			public Adapter caseModel(Model object) {
				return createModelAdapter();
			}
			@Override
			public Adapter caseMetricInstance(MetricInstance object) {
				return createMetricInstanceAdapter();
			}
			@Override
			public Adapter caseRawMetricInstance(RawMetricInstance object) {
				return createRawMetricInstanceAdapter();
			}
			@Override
			public Adapter caseMetricFormulaParameter(MetricFormulaParameter object) {
				return createMetricFormulaParameterAdapter();
			}
			@Override
			public Adapter caseMetric(Metric object) {
				return createMetricAdapter();
			}
			@Override
			public Adapter caseRawMetric(RawMetric object) {
				return createRawMetricAdapter();
			}
			@Override
			public Adapter caseProperty(Property object) {
				return createPropertyAdapter();
			}
			@Override
			public Adapter caseRequirement(Requirement object) {
				return createRequirementAdapter();
			}
			@Override
			public Adapter caseHardRequirement(HardRequirement object) {
				return createHardRequirementAdapter();
			}
			@Override
			public Adapter caseServiceLevelObjective(ServiceLevelObjective object) {
				return createServiceLevelObjectiveAdapter();
			}
			@Override
			public Adapter caseCompositeMetric(CompositeMetric object) {
				return createCompositeMetricAdapter();
			}
			@Override
			public Adapter caseCompositeMetricInstance(CompositeMetricInstance object) {
				return createCompositeMetricInstanceAdapter();
			}
			@Override
			public Adapter defaultCase(EObject object) {
				return createEObjectAdapter();
			}
		};

	/**
	 * Creates an adapter for the <code>target</code>.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param target the object to adapt.
	 * @return the adapter for the <code>target</code>.
	 * @generated
	 */
	@Override
	public Adapter createAdapter(Notifier target) {
		return modelSwitch.doSwitch((EObject)target);
	}


	/**
	 * Creates a new adapter for an object of class '{@link org.camel_dsl.security.SecurityModel <em>Model</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.camel_dsl.security.SecurityModel
	 * @generated
	 */
	public Adapter createSecurityModelAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.camel_dsl.security.SecurityDomain <em>Domain</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.camel_dsl.security.SecurityDomain
	 * @generated
	 */
	public Adapter createSecurityDomainAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.camel_dsl.security.SecurityControl <em>Control</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.camel_dsl.security.SecurityControl
	 * @generated
	 */
	public Adapter createSecurityControlAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.camel_dsl.security.RawSecurityMetricInstance <em>Raw Security Metric Instance</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.camel_dsl.security.RawSecurityMetricInstance
	 * @generated
	 */
	public Adapter createRawSecurityMetricInstanceAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.camel_dsl.security.RawSecurityMetric <em>Raw Security Metric</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.camel_dsl.security.RawSecurityMetric
	 * @generated
	 */
	public Adapter createRawSecurityMetricAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.camel_dsl.security.SecurityProperty <em>Property</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.camel_dsl.security.SecurityProperty
	 * @generated
	 */
	public Adapter createSecurityPropertyAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.camel_dsl.security.Certifiable <em>Certifiable</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.camel_dsl.security.Certifiable
	 * @generated
	 */
	public Adapter createCertifiableAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.camel_dsl.security.SecuritySLO <em>SLO</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.camel_dsl.security.SecuritySLO
	 * @generated
	 */
	public Adapter createSecuritySLOAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.camel_dsl.security.SecurityCapability <em>Capability</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.camel_dsl.security.SecurityCapability
	 * @generated
	 */
	public Adapter createSecurityCapabilityAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.camel_dsl.security.CompositeSecurityMetric <em>Composite Security Metric</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.camel_dsl.security.CompositeSecurityMetric
	 * @generated
	 */
	public Adapter createCompositeSecurityMetricAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.camel_dsl.security.CompositeSecurityMetricInstance <em>Composite Security Metric Instance</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.camel_dsl.security.CompositeSecurityMetricInstance
	 * @generated
	 */
	public Adapter createCompositeSecurityMetricInstanceAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.camel_dsl.security.MUSASecurityControl <em>MUSA Security Control</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.camel_dsl.security.MUSASecurityControl
	 * @generated
	 */
	public Adapter createMUSASecurityControlAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.camel_dsl.security.MUSASecurityCapability <em>MUSA Security Capability</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.camel_dsl.security.MUSASecurityCapability
	 * @generated
	 */
	public Adapter createMUSASecurityCapabilityAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.camel_dsl.camel.Model <em>Model</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.camel_dsl.camel.Model
	 * @generated
	 */
	public Adapter createModelAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.camel_dsl.metric.MetricInstance <em>Instance</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.camel_dsl.metric.MetricInstance
	 * @generated
	 */
	public Adapter createMetricInstanceAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.camel_dsl.metric.RawMetricInstance <em>Raw Metric Instance</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.camel_dsl.metric.RawMetricInstance
	 * @generated
	 */
	public Adapter createRawMetricInstanceAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.camel_dsl.metric.MetricFormulaParameter <em>Formula Parameter</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.camel_dsl.metric.MetricFormulaParameter
	 * @generated
	 */
	public Adapter createMetricFormulaParameterAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.camel_dsl.metric.Metric <em>Metric</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.camel_dsl.metric.Metric
	 * @generated
	 */
	public Adapter createMetricAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.camel_dsl.metric.RawMetric <em>Raw Metric</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.camel_dsl.metric.RawMetric
	 * @generated
	 */
	public Adapter createRawMetricAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.camel_dsl.metric.Property <em>Property</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.camel_dsl.metric.Property
	 * @generated
	 */
	public Adapter createPropertyAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.camel_dsl.requirement.Requirement <em>Requirement</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.camel_dsl.requirement.Requirement
	 * @generated
	 */
	public Adapter createRequirementAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.camel_dsl.requirement.HardRequirement <em>Hard Requirement</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.camel_dsl.requirement.HardRequirement
	 * @generated
	 */
	public Adapter createHardRequirementAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.camel_dsl.requirement.ServiceLevelObjective <em>Service Level Objective</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.camel_dsl.requirement.ServiceLevelObjective
	 * @generated
	 */
	public Adapter createServiceLevelObjectiveAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.camel_dsl.metric.CompositeMetric <em>Composite Metric</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.camel_dsl.metric.CompositeMetric
	 * @generated
	 */
	public Adapter createCompositeMetricAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.camel_dsl.metric.CompositeMetricInstance <em>Composite Metric Instance</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.camel_dsl.metric.CompositeMetricInstance
	 * @generated
	 */
	public Adapter createCompositeMetricInstanceAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for the default case.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @generated
	 */
	public Adapter createEObjectAdapter() {
		return null;
	}

} //SecurityAdapterFactory
