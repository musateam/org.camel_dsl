/**
 */
package org.camel_dsl.security;

import org.camel_dsl.metric.CompositeMetricInstance;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Composite Security Metric Instance</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see org.camel_dsl.security.SecurityPackage#getCompositeSecurityMetricInstance()
 * @model
 * @generated
 */
public interface CompositeSecurityMetricInstance extends CompositeMetricInstance {
} // CompositeSecurityMetricInstance
