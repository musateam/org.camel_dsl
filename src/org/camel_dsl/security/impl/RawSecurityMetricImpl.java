/**
 */
package org.camel_dsl.security.impl;

import org.camel_dsl.metric.impl.RawMetricImpl;

import org.camel_dsl.security.RawSecurityMetric;
import org.camel_dsl.security.SecurityPackage;

import org.eclipse.emf.ecore.EClass;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Raw Security Metric</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class RawSecurityMetricImpl extends RawMetricImpl implements RawSecurityMetric {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected RawSecurityMetricImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return SecurityPackage.Literals.RAW_SECURITY_METRIC;
	}

} //RawSecurityMetricImpl
