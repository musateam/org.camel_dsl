/**
 */
package org.camel_dsl.security.impl;

import org.camel_dsl.metric.impl.CompositeMetricImpl;

import org.camel_dsl.security.CompositeSecurityMetric;
import org.camel_dsl.security.SecurityPackage;

import org.eclipse.emf.ecore.EClass;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Composite Security Metric</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class CompositeSecurityMetricImpl extends CompositeMetricImpl implements CompositeSecurityMetric {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected CompositeSecurityMetricImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return SecurityPackage.Literals.COMPOSITE_SECURITY_METRIC;
	}

} //CompositeSecurityMetricImpl
