/**
 */
package org.camel_dsl.security.impl;

import org.camel_dsl.metric.impl.PropertyImpl;

import org.camel_dsl.security.SecurityDomain;
import org.camel_dsl.security.SecurityPackage;
import org.camel_dsl.security.SecurityProperty;

import org.eclipse.emf.ecore.EClass;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Property</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link org.camel_dsl.security.impl.SecurityPropertyImpl#getDomain <em>Domain</em>}</li>
 * </ul>
 *
 * @generated
 */
public class SecurityPropertyImpl extends PropertyImpl implements SecurityProperty {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected SecurityPropertyImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return SecurityPackage.Literals.SECURITY_PROPERTY;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SecurityDomain getDomain() {
		return (SecurityDomain)eGet(SecurityPackage.Literals.SECURITY_PROPERTY__DOMAIN, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setDomain(SecurityDomain newDomain) {
		eSet(SecurityPackage.Literals.SECURITY_PROPERTY__DOMAIN, newDomain);
	}

} //SecurityPropertyImpl
