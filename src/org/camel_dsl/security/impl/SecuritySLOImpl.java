/**
 */
package org.camel_dsl.security.impl;

import org.camel_dsl.requirement.impl.ServiceLevelObjectiveImpl;

import org.camel_dsl.security.SecurityPackage;
import org.camel_dsl.security.SecuritySLO;

import org.eclipse.emf.ecore.EClass;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>SLO</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class SecuritySLOImpl extends ServiceLevelObjectiveImpl implements SecuritySLO {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected SecuritySLOImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return SecurityPackage.Literals.SECURITY_SLO;
	}

} //SecuritySLOImpl
