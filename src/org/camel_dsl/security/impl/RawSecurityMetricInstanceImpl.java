/**
 */
package org.camel_dsl.security.impl;

import org.camel_dsl.metric.impl.RawMetricInstanceImpl;

import org.camel_dsl.security.RawSecurityMetricInstance;
import org.camel_dsl.security.SecurityPackage;

import org.eclipse.emf.ecore.EClass;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Raw Security Metric Instance</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class RawSecurityMetricInstanceImpl extends RawMetricInstanceImpl implements RawSecurityMetricInstance {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected RawSecurityMetricInstanceImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return SecurityPackage.Literals.RAW_SECURITY_METRIC_INSTANCE;
	}

} //RawSecurityMetricInstanceImpl
