/**
 */
package org.camel_dsl.security.impl;

import org.camel_dsl.security.MUSASecurityControl;
import org.camel_dsl.security.SecurityDomain;
import org.camel_dsl.security.SecurityPackage;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.internal.cdo.CDOObjectImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>MUSA Security Control</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link org.camel_dsl.security.impl.MUSASecurityControlImpl#getId <em>Id</em>}</li>
 *   <li>{@link org.camel_dsl.security.impl.MUSASecurityControlImpl#getName <em>Name</em>}</li>
 *   <li>{@link org.camel_dsl.security.impl.MUSASecurityControlImpl#getDomain <em>Domain</em>}</li>
 *   <li>{@link org.camel_dsl.security.impl.MUSASecurityControlImpl#getSubDomain <em>Sub Domain</em>}</li>
 *   <li>{@link org.camel_dsl.security.impl.MUSASecurityControlImpl#getSpecification <em>Specification</em>}</li>
 * </ul>
 *
 * @generated
 */
public class MUSASecurityControlImpl extends CDOObjectImpl implements MUSASecurityControl {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected MUSASecurityControlImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return SecurityPackage.Literals.MUSA_SECURITY_CONTROL;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected int eStaticFeatureCount() {
		return 0;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getId() {
		return (String)eGet(SecurityPackage.Literals.MUSA_SECURITY_CONTROL__ID, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setId(String newId) {
		eSet(SecurityPackage.Literals.MUSA_SECURITY_CONTROL__ID, newId);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getName() {
		return (String)eGet(SecurityPackage.Literals.MUSA_SECURITY_CONTROL__NAME, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setName(String newName) {
		eSet(SecurityPackage.Literals.MUSA_SECURITY_CONTROL__NAME, newName);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SecurityDomain getDomain() {
		return (SecurityDomain)eGet(SecurityPackage.Literals.MUSA_SECURITY_CONTROL__DOMAIN, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setDomain(SecurityDomain newDomain) {
		eSet(SecurityPackage.Literals.MUSA_SECURITY_CONTROL__DOMAIN, newDomain);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SecurityDomain getSubDomain() {
		return (SecurityDomain)eGet(SecurityPackage.Literals.MUSA_SECURITY_CONTROL__SUB_DOMAIN, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setSubDomain(SecurityDomain newSubDomain) {
		eSet(SecurityPackage.Literals.MUSA_SECURITY_CONTROL__SUB_DOMAIN, newSubDomain);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getSpecification() {
		return (String)eGet(SecurityPackage.Literals.MUSA_SECURITY_CONTROL__SPECIFICATION, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setSpecification(String newSpecification) {
		eSet(SecurityPackage.Literals.MUSA_SECURITY_CONTROL__SPECIFICATION, newSpecification);
	}

} //MUSASecurityControlImpl
