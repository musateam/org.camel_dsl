/**
 */
package org.camel_dsl.security.impl;

import org.camel_dsl.metric.impl.CompositeMetricInstanceImpl;

import org.camel_dsl.security.CompositeSecurityMetricInstance;
import org.camel_dsl.security.SecurityPackage;

import org.eclipse.emf.ecore.EClass;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Composite Security Metric Instance</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class CompositeSecurityMetricInstanceImpl extends CompositeMetricInstanceImpl implements CompositeSecurityMetricInstance {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected CompositeSecurityMetricInstanceImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return SecurityPackage.Literals.COMPOSITE_SECURITY_METRIC_INSTANCE;
	}

} //CompositeSecurityMetricInstanceImpl
