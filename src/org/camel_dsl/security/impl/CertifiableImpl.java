/**
 */
package org.camel_dsl.security.impl;

import org.camel_dsl.security.Certifiable;
import org.camel_dsl.security.SecurityPackage;

import org.eclipse.emf.ecore.EClass;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Certifiable</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class CertifiableImpl extends SecurityPropertyImpl implements Certifiable {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected CertifiableImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return SecurityPackage.Literals.CERTIFIABLE;
	}

} //CertifiableImpl
