/**
 */
package org.camel_dsl.security.impl;

import org.camel_dsl.camel.impl.ModelImpl;

import org.camel_dsl.requirement.SecurityRequirement;

import org.camel_dsl.security.CompositeSecurityMetric;
import org.camel_dsl.security.CompositeSecurityMetricInstance;
import org.camel_dsl.security.MUSASecurityCapability;
import org.camel_dsl.security.MUSASecurityControl;
import org.camel_dsl.security.RawSecurityMetric;
import org.camel_dsl.security.RawSecurityMetricInstance;
import org.camel_dsl.security.SecurityCapability;
import org.camel_dsl.security.SecurityControl;
import org.camel_dsl.security.SecurityDomain;
import org.camel_dsl.security.SecurityModel;
import org.camel_dsl.security.SecurityPackage;
import org.camel_dsl.security.SecurityProperty;
import org.camel_dsl.security.SecuritySLO;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Model</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link org.camel_dsl.security.impl.SecurityModelImpl#getSecurityControls <em>Security Controls</em>}</li>
 *   <li>{@link org.camel_dsl.security.impl.SecurityModelImpl#getSecurityRequirements <em>Security Requirements</em>}</li>
 *   <li>{@link org.camel_dsl.security.impl.SecurityModelImpl#getSecurityProperties <em>Security Properties</em>}</li>
 *   <li>{@link org.camel_dsl.security.impl.SecurityModelImpl#getRawSecurityMetrics <em>Raw Security Metrics</em>}</li>
 *   <li>{@link org.camel_dsl.security.impl.SecurityModelImpl#getCompositeSecurityMetrics <em>Composite Security Metrics</em>}</li>
 *   <li>{@link org.camel_dsl.security.impl.SecurityModelImpl#getRawSecurityMetricInstances <em>Raw Security Metric Instances</em>}</li>
 *   <li>{@link org.camel_dsl.security.impl.SecurityModelImpl#getCompositeSecurityMetricInstances <em>Composite Security Metric Instances</em>}</li>
 *   <li>{@link org.camel_dsl.security.impl.SecurityModelImpl#getSecurityDomains <em>Security Domains</em>}</li>
 *   <li>{@link org.camel_dsl.security.impl.SecurityModelImpl#getSecurityCapabilities <em>Security Capabilities</em>}</li>
 *   <li>{@link org.camel_dsl.security.impl.SecurityModelImpl#getSecuritySLOs <em>Security SL Os</em>}</li>
 *   <li>{@link org.camel_dsl.security.impl.SecurityModelImpl#getMUSAsecurityControls <em>MUS Asecurity Controls</em>}</li>
 *   <li>{@link org.camel_dsl.security.impl.SecurityModelImpl#getMUSAsecurityCapabilities <em>MUS Asecurity Capabilities</em>}</li>
 * </ul>
 *
 * @generated
 */
public class SecurityModelImpl extends ModelImpl implements SecurityModel {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected SecurityModelImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return SecurityPackage.Literals.SECURITY_MODEL;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	public EList<SecurityControl> getSecurityControls() {
		return (EList<SecurityControl>)eGet(SecurityPackage.Literals.SECURITY_MODEL__SECURITY_CONTROLS, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	public EList<SecurityRequirement> getSecurityRequirements() {
		return (EList<SecurityRequirement>)eGet(SecurityPackage.Literals.SECURITY_MODEL__SECURITY_REQUIREMENTS, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	public EList<SecurityProperty> getSecurityProperties() {
		return (EList<SecurityProperty>)eGet(SecurityPackage.Literals.SECURITY_MODEL__SECURITY_PROPERTIES, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	public EList<RawSecurityMetric> getRawSecurityMetrics() {
		return (EList<RawSecurityMetric>)eGet(SecurityPackage.Literals.SECURITY_MODEL__RAW_SECURITY_METRICS, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	public EList<CompositeSecurityMetric> getCompositeSecurityMetrics() {
		return (EList<CompositeSecurityMetric>)eGet(SecurityPackage.Literals.SECURITY_MODEL__COMPOSITE_SECURITY_METRICS, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	public EList<RawSecurityMetricInstance> getRawSecurityMetricInstances() {
		return (EList<RawSecurityMetricInstance>)eGet(SecurityPackage.Literals.SECURITY_MODEL__RAW_SECURITY_METRIC_INSTANCES, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	public EList<CompositeSecurityMetricInstance> getCompositeSecurityMetricInstances() {
		return (EList<CompositeSecurityMetricInstance>)eGet(SecurityPackage.Literals.SECURITY_MODEL__COMPOSITE_SECURITY_METRIC_INSTANCES, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	public EList<SecurityDomain> getSecurityDomains() {
		return (EList<SecurityDomain>)eGet(SecurityPackage.Literals.SECURITY_MODEL__SECURITY_DOMAINS, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	public EList<SecurityCapability> getSecurityCapabilities() {
		return (EList<SecurityCapability>)eGet(SecurityPackage.Literals.SECURITY_MODEL__SECURITY_CAPABILITIES, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	public EList<SecuritySLO> getSecuritySLOs() {
		return (EList<SecuritySLO>)eGet(SecurityPackage.Literals.SECURITY_MODEL__SECURITY_SL_OS, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	public EList<MUSASecurityControl> getMUSAsecurityControls() {
		return (EList<MUSASecurityControl>)eGet(SecurityPackage.Literals.SECURITY_MODEL__MUS_ASECURITY_CONTROLS, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	public EList<MUSASecurityCapability> getMUSAsecurityCapabilities() {
		return (EList<MUSASecurityCapability>)eGet(SecurityPackage.Literals.SECURITY_MODEL__MUS_ASECURITY_CAPABILITIES, true);
	}

} //SecurityModelImpl
