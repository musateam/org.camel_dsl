/**
 */
package org.camel_dsl.security.impl;

import org.camel_dsl.security.MUSASecurityCapability;
import org.camel_dsl.security.MUSASecurityControl;
import org.camel_dsl.security.SecurityPackage;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.internal.cdo.CDOObjectImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>MUSA Security Capability</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link org.camel_dsl.security.impl.MUSASecurityCapabilityImpl#getName <em>Name</em>}</li>
 *   <li>{@link org.camel_dsl.security.impl.MUSASecurityCapabilityImpl#getMUSAsecurityControls <em>MUS Asecurity Controls</em>}</li>
 * </ul>
 *
 * @generated
 */
public class MUSASecurityCapabilityImpl extends CDOObjectImpl implements MUSASecurityCapability {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected MUSASecurityCapabilityImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return SecurityPackage.Literals.MUSA_SECURITY_CAPABILITY;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected int eStaticFeatureCount() {
		return 0;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getName() {
		return (String)eGet(SecurityPackage.Literals.MUSA_SECURITY_CAPABILITY__NAME, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setName(String newName) {
		eSet(SecurityPackage.Literals.MUSA_SECURITY_CAPABILITY__NAME, newName);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	public EList<MUSASecurityControl> getMUSAsecurityControls() {
		return (EList<MUSASecurityControl>)eGet(SecurityPackage.Literals.MUSA_SECURITY_CAPABILITY__MUS_ASECURITY_CONTROLS, true);
	}

} //MUSASecurityCapabilityImpl
