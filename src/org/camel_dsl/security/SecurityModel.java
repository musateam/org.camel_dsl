/**
 */
package org.camel_dsl.security;

import org.camel_dsl.camel.Model;

import org.camel_dsl.requirement.SecurityRequirement;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Model</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.camel_dsl.security.SecurityModel#getSecurityControls <em>Security Controls</em>}</li>
 *   <li>{@link org.camel_dsl.security.SecurityModel#getSecurityRequirements <em>Security Requirements</em>}</li>
 *   <li>{@link org.camel_dsl.security.SecurityModel#getSecurityProperties <em>Security Properties</em>}</li>
 *   <li>{@link org.camel_dsl.security.SecurityModel#getRawSecurityMetrics <em>Raw Security Metrics</em>}</li>
 *   <li>{@link org.camel_dsl.security.SecurityModel#getCompositeSecurityMetrics <em>Composite Security Metrics</em>}</li>
 *   <li>{@link org.camel_dsl.security.SecurityModel#getRawSecurityMetricInstances <em>Raw Security Metric Instances</em>}</li>
 *   <li>{@link org.camel_dsl.security.SecurityModel#getCompositeSecurityMetricInstances <em>Composite Security Metric Instances</em>}</li>
 *   <li>{@link org.camel_dsl.security.SecurityModel#getSecurityDomains <em>Security Domains</em>}</li>
 *   <li>{@link org.camel_dsl.security.SecurityModel#getSecurityCapabilities <em>Security Capabilities</em>}</li>
 *   <li>{@link org.camel_dsl.security.SecurityModel#getSecuritySLOs <em>Security SL Os</em>}</li>
 *   <li>{@link org.camel_dsl.security.SecurityModel#getMUSAsecurityControls <em>MUS Asecurity Controls</em>}</li>
 *   <li>{@link org.camel_dsl.security.SecurityModel#getMUSAsecurityCapabilities <em>MUS Asecurity Capabilities</em>}</li>
 * </ul>
 *
 * @see org.camel_dsl.security.SecurityPackage#getSecurityModel()
 * @model
 * @generated
 */
public interface SecurityModel extends Model {
	/**
	 * Returns the value of the '<em><b>Security Controls</b></em>' containment reference list.
	 * The list contents are of type {@link org.camel_dsl.security.SecurityControl}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Security Controls</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Security Controls</em>' containment reference list.
	 * @see org.camel_dsl.security.SecurityPackage#getSecurityModel_SecurityControls()
	 * @model containment="true" ordered="false"
	 * @generated
	 */
	EList<SecurityControl> getSecurityControls();

	/**
	 * Returns the value of the '<em><b>Security Requirements</b></em>' containment reference list.
	 * The list contents are of type {@link org.camel_dsl.requirement.SecurityRequirement}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Security Requirements</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Security Requirements</em>' containment reference list.
	 * @see org.camel_dsl.security.SecurityPackage#getSecurityModel_SecurityRequirements()
	 * @model containment="true" ordered="false"
	 * @generated
	 */
	EList<SecurityRequirement> getSecurityRequirements();

	/**
	 * Returns the value of the '<em><b>Security Properties</b></em>' containment reference list.
	 * The list contents are of type {@link org.camel_dsl.security.SecurityProperty}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Security Properties</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Security Properties</em>' containment reference list.
	 * @see org.camel_dsl.security.SecurityPackage#getSecurityModel_SecurityProperties()
	 * @model containment="true" ordered="false"
	 * @generated
	 */
	EList<SecurityProperty> getSecurityProperties();

	/**
	 * Returns the value of the '<em><b>Raw Security Metrics</b></em>' containment reference list.
	 * The list contents are of type {@link org.camel_dsl.security.RawSecurityMetric}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Raw Security Metrics</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Raw Security Metrics</em>' containment reference list.
	 * @see org.camel_dsl.security.SecurityPackage#getSecurityModel_RawSecurityMetrics()
	 * @model containment="true" ordered="false"
	 * @generated
	 */
	EList<RawSecurityMetric> getRawSecurityMetrics();

	/**
	 * Returns the value of the '<em><b>Composite Security Metrics</b></em>' containment reference list.
	 * The list contents are of type {@link org.camel_dsl.security.CompositeSecurityMetric}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Composite Security Metrics</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Composite Security Metrics</em>' containment reference list.
	 * @see org.camel_dsl.security.SecurityPackage#getSecurityModel_CompositeSecurityMetrics()
	 * @model containment="true" ordered="false"
	 * @generated
	 */
	EList<CompositeSecurityMetric> getCompositeSecurityMetrics();

	/**
	 * Returns the value of the '<em><b>Raw Security Metric Instances</b></em>' containment reference list.
	 * The list contents are of type {@link org.camel_dsl.security.RawSecurityMetricInstance}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Raw Security Metric Instances</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Raw Security Metric Instances</em>' containment reference list.
	 * @see org.camel_dsl.security.SecurityPackage#getSecurityModel_RawSecurityMetricInstances()
	 * @model containment="true" ordered="false"
	 *        annotation="teneo.jpa value='@JoinColumn(name=\"rawsecuritymetricInstances\")'"
	 * @generated
	 */
	EList<RawSecurityMetricInstance> getRawSecurityMetricInstances();

	/**
	 * Returns the value of the '<em><b>Composite Security Metric Instances</b></em>' containment reference list.
	 * The list contents are of type {@link org.camel_dsl.security.CompositeSecurityMetricInstance}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Composite Security Metric Instances</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Composite Security Metric Instances</em>' containment reference list.
	 * @see org.camel_dsl.security.SecurityPackage#getSecurityModel_CompositeSecurityMetricInstances()
	 * @model containment="true" ordered="false"
	 *        annotation="teneo.jpa value='@JoinColumn(name=\"compositesecuritymetricInstances\")'"
	 * @generated
	 */
	EList<CompositeSecurityMetricInstance> getCompositeSecurityMetricInstances();

	/**
	 * Returns the value of the '<em><b>Security Domains</b></em>' containment reference list.
	 * The list contents are of type {@link org.camel_dsl.security.SecurityDomain}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Security Domains</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Security Domains</em>' containment reference list.
	 * @see org.camel_dsl.security.SecurityPackage#getSecurityModel_SecurityDomains()
	 * @model containment="true" ordered="false"
	 * @generated
	 */
	EList<SecurityDomain> getSecurityDomains();

	/**
	 * Returns the value of the '<em><b>Security Capabilities</b></em>' containment reference list.
	 * The list contents are of type {@link org.camel_dsl.security.SecurityCapability}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Security Capabilities</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Security Capabilities</em>' containment reference list.
	 * @see org.camel_dsl.security.SecurityPackage#getSecurityModel_SecurityCapabilities()
	 * @model containment="true" ordered="false"
	 * @generated
	 */
	EList<SecurityCapability> getSecurityCapabilities();

	/**
	 * Returns the value of the '<em><b>Security SL Os</b></em>' containment reference list.
	 * The list contents are of type {@link org.camel_dsl.security.SecuritySLO}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Security SL Os</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Security SL Os</em>' containment reference list.
	 * @see org.camel_dsl.security.SecurityPackage#getSecurityModel_SecuritySLOs()
	 * @model containment="true"
	 * @generated
	 */
	EList<SecuritySLO> getSecuritySLOs();

	/**
	 * Returns the value of the '<em><b>MUS Asecurity Controls</b></em>' containment reference list.
	 * The list contents are of type {@link org.camel_dsl.security.MUSASecurityControl}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>MUS Asecurity Controls</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>MUS Asecurity Controls</em>' containment reference list.
	 * @see org.camel_dsl.security.SecurityPackage#getSecurityModel_MUSAsecurityControls()
	 * @model containment="true" ordered="false"
	 * @generated
	 */
	EList<MUSASecurityControl> getMUSAsecurityControls();

	/**
	 * Returns the value of the '<em><b>MUS Asecurity Capabilities</b></em>' containment reference list.
	 * The list contents are of type {@link org.camel_dsl.security.MUSASecurityCapability}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>MUS Asecurity Capabilities</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>MUS Asecurity Capabilities</em>' containment reference list.
	 * @see org.camel_dsl.security.SecurityPackage#getSecurityModel_MUSAsecurityCapabilities()
	 * @model containment="true" ordered="false"
	 * @generated
	 */
	EList<MUSASecurityCapability> getMUSAsecurityCapabilities();

} // SecurityModel
