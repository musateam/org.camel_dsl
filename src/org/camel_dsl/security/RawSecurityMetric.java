/**
 */
package org.camel_dsl.security;

import org.camel_dsl.metric.RawMetric;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Raw Security Metric</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see org.camel_dsl.security.SecurityPackage#getRawSecurityMetric()
 * @model
 * @generated
 */
public interface RawSecurityMetric extends RawMetric {
} // RawSecurityMetric
