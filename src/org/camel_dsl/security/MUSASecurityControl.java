/**
 */
package org.camel_dsl.security;

import org.eclipse.emf.cdo.CDOObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>MUSA Security Control</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.camel_dsl.security.MUSASecurityControl#getId <em>Id</em>}</li>
 *   <li>{@link org.camel_dsl.security.MUSASecurityControl#getName <em>Name</em>}</li>
 *   <li>{@link org.camel_dsl.security.MUSASecurityControl#getDomain <em>Domain</em>}</li>
 *   <li>{@link org.camel_dsl.security.MUSASecurityControl#getSubDomain <em>Sub Domain</em>}</li>
 *   <li>{@link org.camel_dsl.security.MUSASecurityControl#getSpecification <em>Specification</em>}</li>
 * </ul>
 *
 * @see org.camel_dsl.security.SecurityPackage#getMUSASecurityControl()
 * @model
 * @extends CDOObject
 * @generated
 */
public interface MUSASecurityControl extends CDOObject {
	/**
	 * Returns the value of the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Id</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Id</em>' attribute.
	 * @see #setId(String)
	 * @see org.camel_dsl.security.SecurityPackage#getMUSASecurityControl_Id()
	 * @model required="true"
	 * @generated
	 */
	String getId();

	/**
	 * Sets the value of the '{@link org.camel_dsl.security.MUSASecurityControl#getId <em>Id</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Id</em>' attribute.
	 * @see #getId()
	 * @generated
	 */
	void setId(String value);

	/**
	 * Returns the value of the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Name</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Name</em>' attribute.
	 * @see #setName(String)
	 * @see org.camel_dsl.security.SecurityPackage#getMUSASecurityControl_Name()
	 * @model required="true"
	 * @generated
	 */
	String getName();

	/**
	 * Sets the value of the '{@link org.camel_dsl.security.MUSASecurityControl#getName <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Name</em>' attribute.
	 * @see #getName()
	 * @generated
	 */
	void setName(String value);

	/**
	 * Returns the value of the '<em><b>Domain</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Domain</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Domain</em>' reference.
	 * @see #setDomain(SecurityDomain)
	 * @see org.camel_dsl.security.SecurityPackage#getMUSASecurityControl_Domain()
	 * @model required="true"
	 * @generated
	 */
	SecurityDomain getDomain();

	/**
	 * Sets the value of the '{@link org.camel_dsl.security.MUSASecurityControl#getDomain <em>Domain</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Domain</em>' reference.
	 * @see #getDomain()
	 * @generated
	 */
	void setDomain(SecurityDomain value);

	/**
	 * Returns the value of the '<em><b>Sub Domain</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Sub Domain</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Sub Domain</em>' reference.
	 * @see #setSubDomain(SecurityDomain)
	 * @see org.camel_dsl.security.SecurityPackage#getMUSASecurityControl_SubDomain()
	 * @model
	 * @generated
	 */
	SecurityDomain getSubDomain();

	/**
	 * Sets the value of the '{@link org.camel_dsl.security.MUSASecurityControl#getSubDomain <em>Sub Domain</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Sub Domain</em>' reference.
	 * @see #getSubDomain()
	 * @generated
	 */
	void setSubDomain(SecurityDomain value);

	/**
	 * Returns the value of the '<em><b>Specification</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Specification</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Specification</em>' attribute.
	 * @see #setSpecification(String)
	 * @see org.camel_dsl.security.SecurityPackage#getMUSASecurityControl_Specification()
	 * @model required="true"
	 *        annotation="http://schema.omg.org/spec/MOF/2.0/emof.xml#Property.oppositeRoleName body='AgreementType'"
	 *        annotation="teneo.jpa value='@Column(length=4000)'"
	 * @generated
	 */
	String getSpecification();

	/**
	 * Sets the value of the '{@link org.camel_dsl.security.MUSASecurityControl#getSpecification <em>Specification</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Specification</em>' attribute.
	 * @see #getSpecification()
	 * @generated
	 */
	void setSpecification(String value);

} // MUSASecurityControl
