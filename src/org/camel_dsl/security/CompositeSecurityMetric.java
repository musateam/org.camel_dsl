/**
 */
package org.camel_dsl.security;

import org.camel_dsl.metric.CompositeMetric;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Composite Security Metric</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see org.camel_dsl.security.SecurityPackage#getCompositeSecurityMetric()
 * @model
 * @generated
 */
public interface CompositeSecurityMetric extends CompositeMetric {
} // CompositeSecurityMetric
