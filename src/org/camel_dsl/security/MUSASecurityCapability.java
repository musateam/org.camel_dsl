/**
 */
package org.camel_dsl.security;

import org.eclipse.emf.cdo.CDOObject;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>MUSA Security Capability</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.camel_dsl.security.MUSASecurityCapability#getName <em>Name</em>}</li>
 *   <li>{@link org.camel_dsl.security.MUSASecurityCapability#getMUSAsecurityControls <em>MUS Asecurity Controls</em>}</li>
 * </ul>
 *
 * @see org.camel_dsl.security.SecurityPackage#getMUSASecurityCapability()
 * @model
 * @extends CDOObject
 * @generated
 */
public interface MUSASecurityCapability extends CDOObject {
	/**
	 * Returns the value of the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Name</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Name</em>' attribute.
	 * @see #setName(String)
	 * @see org.camel_dsl.security.SecurityPackage#getMUSASecurityCapability_Name()
	 * @model required="true"
	 *        annotation="http://schema.omg.org/spec/MOF/2.0/emof.xml#Property.oppositeRoleName body='ServiceLevelObjectiveType'"
	 * @generated
	 */
	String getName();

	/**
	 * Sets the value of the '{@link org.camel_dsl.security.MUSASecurityCapability#getName <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Name</em>' attribute.
	 * @see #getName()
	 * @generated
	 */
	void setName(String value);

	/**
	 * Returns the value of the '<em><b>MUS Asecurity Controls</b></em>' reference list.
	 * The list contents are of type {@link org.camel_dsl.security.MUSASecurityControl}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>MUS Asecurity Controls</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>MUS Asecurity Controls</em>' reference list.
	 * @see org.camel_dsl.security.SecurityPackage#getMUSASecurityCapability_MUSAsecurityControls()
	 * @model ordered="false"
	 * @generated
	 */
	EList<MUSASecurityControl> getMUSAsecurityControls();

} // MUSASecurityCapability
