/**
 */
package org.camel_dsl.security;

import org.camel_dsl.metric.RawMetricInstance;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Raw Security Metric Instance</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see org.camel_dsl.security.SecurityPackage#getRawSecurityMetricInstance()
 * @model
 * @generated
 */
public interface RawSecurityMetricInstance extends RawMetricInstance {
} // RawSecurityMetricInstance
