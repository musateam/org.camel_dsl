/**
 */
package org.camel_dsl.requirement;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Hard Requirement</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see org.camel_dsl.requirement.RequirementPackage#getHardRequirement()
 * @model abstract="true"
 * @generated
 */
public interface HardRequirement extends Requirement {
} // HardRequirement
