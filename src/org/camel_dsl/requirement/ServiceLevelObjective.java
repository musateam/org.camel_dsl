/**
 */
package org.camel_dsl.requirement;

import org.camel_dsl.metric.Condition;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Service Level Objective</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.camel_dsl.requirement.ServiceLevelObjective#getCustomServiceLevel <em>Custom Service Level</em>}</li>
 * </ul>
 *
 * @see org.camel_dsl.requirement.RequirementPackage#getServiceLevelObjective()
 * @model
 * @generated
 */
public interface ServiceLevelObjective extends HardRequirement {
	/**
	 * Returns the value of the '<em><b>Custom Service Level</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Custom Service Level</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Custom Service Level</em>' reference.
	 * @see #setCustomServiceLevel(Condition)
	 * @see org.camel_dsl.requirement.RequirementPackage#getServiceLevelObjective_CustomServiceLevel()
	 * @model required="true"
	 * @generated
	 */
	Condition getCustomServiceLevel();

	/**
	 * Sets the value of the '{@link org.camel_dsl.requirement.ServiceLevelObjective#getCustomServiceLevel <em>Custom Service Level</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Custom Service Level</em>' reference.
	 * @see #getCustomServiceLevel()
	 * @generated
	 */
	void setCustomServiceLevel(Condition value);

} // ServiceLevelObjective
