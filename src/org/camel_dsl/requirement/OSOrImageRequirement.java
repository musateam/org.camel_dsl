/**
 */
package org.camel_dsl.requirement;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>OS Or Image Requirement</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see org.camel_dsl.requirement.RequirementPackage#getOSOrImageRequirement()
 * @model abstract="true"
 * @generated
 */
public interface OSOrImageRequirement extends HardRequirement {
} // OSOrImageRequirement
