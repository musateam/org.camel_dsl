/**
 */
package org.camel_dsl.requirement.impl;

import org.camel_dsl.requirement.HardwareRequirement;
import org.camel_dsl.requirement.RequirementPackage;

import org.eclipse.emf.ecore.EClass;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Hardware Requirement</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public abstract class HardwareRequirementImpl extends HardRequirementImpl implements HardwareRequirement {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected HardwareRequirementImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return RequirementPackage.Literals.HARDWARE_REQUIREMENT;
	}

} //HardwareRequirementImpl
