/**
 */
package org.camel_dsl.requirement.impl;

import org.camel_dsl.organisation.CloudProvider;

import org.camel_dsl.requirement.ProviderRequirement;
import org.camel_dsl.requirement.RequirementPackage;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Provider Requirement</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link org.camel_dsl.requirement.impl.ProviderRequirementImpl#getProviders <em>Providers</em>}</li>
 * </ul>
 *
 * @generated
 */
public class ProviderRequirementImpl extends HardRequirementImpl implements ProviderRequirement {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ProviderRequirementImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return RequirementPackage.Literals.PROVIDER_REQUIREMENT;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	public EList<CloudProvider> getProviders() {
		return (EList<CloudProvider>)eGet(RequirementPackage.Literals.PROVIDER_REQUIREMENT__PROVIDERS, true);
	}

} //ProviderRequirementImpl
