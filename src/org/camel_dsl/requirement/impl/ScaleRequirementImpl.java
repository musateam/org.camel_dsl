/**
 */
package org.camel_dsl.requirement.impl;

import org.camel_dsl.requirement.RequirementPackage;
import org.camel_dsl.requirement.ScaleRequirement;

import org.eclipse.emf.ecore.EClass;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Scale Requirement</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public abstract class ScaleRequirementImpl extends HardRequirementImpl implements ScaleRequirement {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ScaleRequirementImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return RequirementPackage.Literals.SCALE_REQUIREMENT;
	}

} //ScaleRequirementImpl
