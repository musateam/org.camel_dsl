/**
 */
package org.camel_dsl.requirement.impl;

import org.camel_dsl.metric.Condition;

import org.camel_dsl.requirement.RequirementPackage;
import org.camel_dsl.requirement.ServiceLevelObjective;

import org.eclipse.emf.ecore.EClass;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Service Level Objective</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link org.camel_dsl.requirement.impl.ServiceLevelObjectiveImpl#getCustomServiceLevel <em>Custom Service Level</em>}</li>
 * </ul>
 *
 * @generated
 */
public class ServiceLevelObjectiveImpl extends HardRequirementImpl implements ServiceLevelObjective {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ServiceLevelObjectiveImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return RequirementPackage.Literals.SERVICE_LEVEL_OBJECTIVE;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Condition getCustomServiceLevel() {
		return (Condition)eGet(RequirementPackage.Literals.SERVICE_LEVEL_OBJECTIVE__CUSTOM_SERVICE_LEVEL, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setCustomServiceLevel(Condition newCustomServiceLevel) {
		eSet(RequirementPackage.Literals.SERVICE_LEVEL_OBJECTIVE__CUSTOM_SERVICE_LEVEL, newCustomServiceLevel);
	}

} //ServiceLevelObjectiveImpl
