/**
 */
package org.camel_dsl.requirement.impl;

import org.camel_dsl.location.Location;

import org.camel_dsl.requirement.LocationRequirement;
import org.camel_dsl.requirement.RequirementPackage;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Location Requirement</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link org.camel_dsl.requirement.impl.LocationRequirementImpl#getLocations <em>Locations</em>}</li>
 * </ul>
 *
 * @generated
 */
public class LocationRequirementImpl extends HardRequirementImpl implements LocationRequirement {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected LocationRequirementImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return RequirementPackage.Literals.LOCATION_REQUIREMENT;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	public EList<Location> getLocations() {
		return (EList<Location>)eGet(RequirementPackage.Literals.LOCATION_REQUIREMENT__LOCATIONS, true);
	}

} //LocationRequirementImpl
