/**
 */
package org.camel_dsl.requirement;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Scale Requirement</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see org.camel_dsl.requirement.RequirementPackage#getScaleRequirement()
 * @model abstract="true"
 * @generated
 */
public interface ScaleRequirement extends HardRequirement {
} // ScaleRequirement
