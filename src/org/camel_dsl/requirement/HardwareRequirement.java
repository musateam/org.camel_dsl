/**
 */
package org.camel_dsl.requirement;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Hardware Requirement</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see org.camel_dsl.requirement.RequirementPackage#getHardwareRequirement()
 * @model abstract="true"
 * @generated
 */
public interface HardwareRequirement extends HardRequirement {
} // HardwareRequirement
