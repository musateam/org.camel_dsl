/**
 */
package org.camel_dsl.requirement;

import org.camel_dsl.organisation.CloudProvider;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Provider Requirement</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.camel_dsl.requirement.ProviderRequirement#getProviders <em>Providers</em>}</li>
 * </ul>
 *
 * @see org.camel_dsl.requirement.RequirementPackage#getProviderRequirement()
 * @model
 * @generated
 */
public interface ProviderRequirement extends HardRequirement {
	/**
	 * Returns the value of the '<em><b>Providers</b></em>' reference list.
	 * The list contents are of type {@link org.camel_dsl.organisation.CloudProvider}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Providers</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Providers</em>' reference list.
	 * @see org.camel_dsl.requirement.RequirementPackage#getProviderRequirement_Providers()
	 * @model required="true"
	 * @generated
	 */
	EList<CloudProvider> getProviders();

} // ProviderRequirement
