/**
 */
package org.camel_dsl.requirement;

import org.camel_dsl.camel.Model;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Model</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.camel_dsl.requirement.RequirementModel#getRequirements <em>Requirements</em>}</li>
 * </ul>
 *
 * @see org.camel_dsl.requirement.RequirementPackage#getRequirementModel()
 * @model
 * @generated
 */
public interface RequirementModel extends Model {
	/**
	 * Returns the value of the '<em><b>Requirements</b></em>' containment reference list.
	 * The list contents are of type {@link org.camel_dsl.requirement.Requirement}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Requirements</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Requirements</em>' containment reference list.
	 * @see org.camel_dsl.requirement.RequirementPackage#getRequirementModel_Requirements()
	 * @model containment="true"
	 * @generated
	 */
	EList<Requirement> getRequirements();

} // RequirementModel
