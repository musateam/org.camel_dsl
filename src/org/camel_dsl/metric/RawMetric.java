/**
 */
package org.camel_dsl.metric;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Raw Metric</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see org.camel_dsl.metric.MetricPackage#getRawMetric()
 * @model
 * @generated
 */
public interface RawMetric extends Metric {
} // RawMetric
