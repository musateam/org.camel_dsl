/**
 */
package org.camel_dsl.metric;

import org.camel_dsl.unit.MonetaryUnit;
import org.camel_dsl.unit.TimeIntervalUnit;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Property Condition</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.camel_dsl.metric.PropertyCondition#getPropertyContext <em>Property Context</em>}</li>
 *   <li>{@link org.camel_dsl.metric.PropertyCondition#getUnit <em>Unit</em>}</li>
 *   <li>{@link org.camel_dsl.metric.PropertyCondition#getTimeUnit <em>Time Unit</em>}</li>
 * </ul>
 *
 * @see org.camel_dsl.metric.MetricPackage#getPropertyCondition()
 * @model
 * @generated
 */
public interface PropertyCondition extends Condition {
	/**
	 * Returns the value of the '<em><b>Property Context</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Property Context</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Property Context</em>' reference.
	 * @see #setPropertyContext(PropertyContext)
	 * @see org.camel_dsl.metric.MetricPackage#getPropertyCondition_PropertyContext()
	 * @model required="true"
	 * @generated
	 */
	PropertyContext getPropertyContext();

	/**
	 * Sets the value of the '{@link org.camel_dsl.metric.PropertyCondition#getPropertyContext <em>Property Context</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Property Context</em>' reference.
	 * @see #getPropertyContext()
	 * @generated
	 */
	void setPropertyContext(PropertyContext value);

	/**
	 * Returns the value of the '<em><b>Unit</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Unit</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Unit</em>' reference.
	 * @see #setUnit(MonetaryUnit)
	 * @see org.camel_dsl.metric.MetricPackage#getPropertyCondition_Unit()
	 * @model
	 * @generated
	 */
	MonetaryUnit getUnit();

	/**
	 * Sets the value of the '{@link org.camel_dsl.metric.PropertyCondition#getUnit <em>Unit</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Unit</em>' reference.
	 * @see #getUnit()
	 * @generated
	 */
	void setUnit(MonetaryUnit value);

	/**
	 * Returns the value of the '<em><b>Time Unit</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Time Unit</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Time Unit</em>' reference.
	 * @see #setTimeUnit(TimeIntervalUnit)
	 * @see org.camel_dsl.metric.MetricPackage#getPropertyCondition_TimeUnit()
	 * @model
	 * @generated
	 */
	TimeIntervalUnit getTimeUnit();

	/**
	 * Sets the value of the '{@link org.camel_dsl.metric.PropertyCondition#getTimeUnit <em>Time Unit</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Time Unit</em>' reference.
	 * @see #getTimeUnit()
	 * @generated
	 */
	void setTimeUnit(TimeIntervalUnit value);

} // PropertyCondition
