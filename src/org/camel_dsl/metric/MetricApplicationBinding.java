/**
 */
package org.camel_dsl.metric;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Application Binding</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see org.camel_dsl.metric.MetricPackage#getMetricApplicationBinding()
 * @model
 * @generated
 */
public interface MetricApplicationBinding extends MetricObjectBinding {
} // MetricApplicationBinding
