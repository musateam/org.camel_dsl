/**
 */
package org.camel_dsl.metric;

import org.eclipse.emf.cdo.CDOObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Sensor</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.camel_dsl.metric.Sensor#getName <em>Name</em>}</li>
 *   <li>{@link org.camel_dsl.metric.Sensor#getConfiguration <em>Configuration</em>}</li>
 *   <li>{@link org.camel_dsl.metric.Sensor#isIsPush <em>Is Push</em>}</li>
 * </ul>
 *
 * @see org.camel_dsl.metric.MetricPackage#getSensor()
 * @model
 * @extends CDOObject
 * @generated
 */
public interface Sensor extends CDOObject {
	/**
	 * Returns the value of the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Name</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Name</em>' attribute.
	 * @see #setName(String)
	 * @see org.camel_dsl.metric.MetricPackage#getSensor_Name()
	 * @model required="true"
	 * @generated
	 */
	String getName();

	/**
	 * Sets the value of the '{@link org.camel_dsl.metric.Sensor#getName <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Name</em>' attribute.
	 * @see #getName()
	 * @generated
	 */
	void setName(String value);

	/**
	 * Returns the value of the '<em><b>Configuration</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Configuration</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Configuration</em>' attribute.
	 * @see #setConfiguration(String)
	 * @see org.camel_dsl.metric.MetricPackage#getSensor_Configuration()
	 * @model annotation="http://schema.omg.org/spec/MOF/2.0/emof.xml#Property.oppositeRoleName body='ResourceGroup' unique='false' upper='*'"
	 * @generated
	 */
	String getConfiguration();

	/**
	 * Sets the value of the '{@link org.camel_dsl.metric.Sensor#getConfiguration <em>Configuration</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Configuration</em>' attribute.
	 * @see #getConfiguration()
	 * @generated
	 */
	void setConfiguration(String value);

	/**
	 * Returns the value of the '<em><b>Is Push</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Is Push</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Is Push</em>' attribute.
	 * @see #setIsPush(boolean)
	 * @see org.camel_dsl.metric.MetricPackage#getSensor_IsPush()
	 * @model annotation="http://schema.omg.org/spec/MOF/2.0/emof.xml#Property.oppositeRoleName body='ScalabilityRule' unique='false' upper='*'"
	 * @generated
	 */
	boolean isIsPush();

	/**
	 * Sets the value of the '{@link org.camel_dsl.metric.Sensor#isIsPush <em>Is Push</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Is Push</em>' attribute.
	 * @see #isIsPush()
	 * @generated
	 */
	void setIsPush(boolean value);

} // Sensor
