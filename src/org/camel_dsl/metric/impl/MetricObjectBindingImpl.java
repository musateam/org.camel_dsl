/**
 */
package org.camel_dsl.metric.impl;

import org.camel_dsl.execution.ExecutionContext;

import org.camel_dsl.metric.MetricObjectBinding;
import org.camel_dsl.metric.MetricPackage;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.internal.cdo.CDOObjectImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Object Binding</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link org.camel_dsl.metric.impl.MetricObjectBindingImpl#getName <em>Name</em>}</li>
 *   <li>{@link org.camel_dsl.metric.impl.MetricObjectBindingImpl#getExecutionContext <em>Execution Context</em>}</li>
 * </ul>
 *
 * @generated
 */
public abstract class MetricObjectBindingImpl extends CDOObjectImpl implements MetricObjectBinding {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected MetricObjectBindingImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return MetricPackage.Literals.METRIC_OBJECT_BINDING;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected int eStaticFeatureCount() {
		return 0;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getName() {
		return (String)eGet(MetricPackage.Literals.METRIC_OBJECT_BINDING__NAME, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setName(String newName) {
		eSet(MetricPackage.Literals.METRIC_OBJECT_BINDING__NAME, newName);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ExecutionContext getExecutionContext() {
		return (ExecutionContext)eGet(MetricPackage.Literals.METRIC_OBJECT_BINDING__EXECUTION_CONTEXT, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setExecutionContext(ExecutionContext newExecutionContext) {
		eSet(MetricPackage.Literals.METRIC_OBJECT_BINDING__EXECUTION_CONTEXT, newExecutionContext);
	}

} //MetricObjectBindingImpl
