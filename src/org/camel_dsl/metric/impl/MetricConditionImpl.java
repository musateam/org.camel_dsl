/**
 */
package org.camel_dsl.metric.impl;

import org.camel_dsl.metric.MetricCondition;
import org.camel_dsl.metric.MetricContext;
import org.camel_dsl.metric.MetricPackage;

import org.eclipse.emf.ecore.EClass;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Condition</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link org.camel_dsl.metric.impl.MetricConditionImpl#getMetricContext <em>Metric Context</em>}</li>
 * </ul>
 *
 * @generated
 */
public class MetricConditionImpl extends ConditionImpl implements MetricCondition {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected MetricConditionImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return MetricPackage.Literals.METRIC_CONDITION;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MetricContext getMetricContext() {
		return (MetricContext)eGet(MetricPackage.Literals.METRIC_CONDITION__METRIC_CONTEXT, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setMetricContext(MetricContext newMetricContext) {
		eSet(MetricPackage.Literals.METRIC_CONDITION__METRIC_CONTEXT, newMetricContext);
	}

} //MetricConditionImpl
