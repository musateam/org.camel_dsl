/**
 */
package org.camel_dsl.metric.impl;

import org.camel_dsl.metric.MetricPackage;
import org.camel_dsl.metric.RawMetricInstance;
import org.camel_dsl.metric.Sensor;

import org.eclipse.emf.ecore.EClass;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Raw Metric Instance</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link org.camel_dsl.metric.impl.RawMetricInstanceImpl#getSensor <em>Sensor</em>}</li>
 * </ul>
 *
 * @generated
 */
public class RawMetricInstanceImpl extends MetricInstanceImpl implements RawMetricInstance {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected RawMetricInstanceImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return MetricPackage.Literals.RAW_METRIC_INSTANCE;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Sensor getSensor() {
		return (Sensor)eGet(MetricPackage.Literals.RAW_METRIC_INSTANCE__SENSOR, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setSensor(Sensor newSensor) {
		eSet(MetricPackage.Literals.RAW_METRIC_INSTANCE__SENSOR, newSensor);
	}

} //RawMetricInstanceImpl
