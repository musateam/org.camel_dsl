/**
 */
package org.camel_dsl.metric.impl;

import org.camel_dsl.metric.MetricApplicationBinding;
import org.camel_dsl.metric.MetricPackage;

import org.eclipse.emf.ecore.EClass;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Application Binding</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class MetricApplicationBindingImpl extends MetricObjectBindingImpl implements MetricApplicationBinding {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected MetricApplicationBindingImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return MetricPackage.Literals.METRIC_APPLICATION_BINDING;
	}

} //MetricApplicationBindingImpl
