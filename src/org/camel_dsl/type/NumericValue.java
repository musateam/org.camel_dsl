/**
 */
package org.camel_dsl.type;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Numeric Value</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see org.camel_dsl.type.TypePackage#getNumericValue()
 * @model abstract="true"
 * @generated
 */
public interface NumericValue extends SingleValue {
} // NumericValue
