/**
 */
package org.camel_dsl.type;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Boolean Value Type</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.camel_dsl.type.BooleanValueType#getPrimitiveType <em>Primitive Type</em>}</li>
 * </ul>
 *
 * @see org.camel_dsl.type.TypePackage#getBooleanValueType()
 * @model
 * @generated
 */
public interface BooleanValueType extends ValueType {
	/**
	 * Returns the value of the '<em><b>Primitive Type</b></em>' attribute.
	 * The literals are from the enumeration {@link org.camel_dsl.type.TypeEnum}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Primitive Type</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Primitive Type</em>' attribute.
	 * @see org.camel_dsl.type.TypeEnum
	 * @see #setPrimitiveType(TypeEnum)
	 * @see org.camel_dsl.type.TypePackage#getBooleanValueType_PrimitiveType()
	 * @model required="true"
	 * @generated
	 */
	TypeEnum getPrimitiveType();

	/**
	 * Sets the value of the '{@link org.camel_dsl.type.BooleanValueType#getPrimitiveType <em>Primitive Type</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Primitive Type</em>' attribute.
	 * @see org.camel_dsl.type.TypeEnum
	 * @see #getPrimitiveType()
	 * @generated
	 */
	void setPrimitiveType(TypeEnum value);

} // BooleanValueType
