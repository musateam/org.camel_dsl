/**
 */
package org.camel_dsl.type;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Positive Inf</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see org.camel_dsl.type.TypePackage#getPositiveInf()
 * @model
 * @generated
 */
public interface PositiveInf extends NumericValue {
} // PositiveInf
