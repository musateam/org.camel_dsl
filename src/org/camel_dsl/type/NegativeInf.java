/**
 */
package org.camel_dsl.type;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Negative Inf</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see org.camel_dsl.type.TypePackage#getNegativeInf()
 * @model
 * @generated
 */
public interface NegativeInf extends NumericValue {
} // NegativeInf
