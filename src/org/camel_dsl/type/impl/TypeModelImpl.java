/**
 */
package org.camel_dsl.type.impl;

import org.camel_dsl.camel.impl.ModelImpl;

import org.camel_dsl.type.SingleValue;
import org.camel_dsl.type.TypeModel;
import org.camel_dsl.type.TypePackage;
import org.camel_dsl.type.ValueType;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Model</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link org.camel_dsl.type.impl.TypeModelImpl#getDataTypes <em>Data Types</em>}</li>
 *   <li>{@link org.camel_dsl.type.impl.TypeModelImpl#getValues <em>Values</em>}</li>
 * </ul>
 *
 * @generated
 */
public class TypeModelImpl extends ModelImpl implements TypeModel {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected TypeModelImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return TypePackage.Literals.TYPE_MODEL;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	public EList<ValueType> getDataTypes() {
		return (EList<ValueType>)eGet(TypePackage.Literals.TYPE_MODEL__DATA_TYPES, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	public EList<SingleValue> getValues() {
		return (EList<SingleValue>)eGet(TypePackage.Literals.TYPE_MODEL__VALUES, true);
	}

} //TypeModelImpl
