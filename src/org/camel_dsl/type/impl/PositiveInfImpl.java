/**
 */
package org.camel_dsl.type.impl;

import org.camel_dsl.type.PositiveInf;
import org.camel_dsl.type.TypePackage;

import org.eclipse.emf.ecore.EClass;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Positive Inf</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class PositiveInfImpl extends NumericValueImpl implements PositiveInf {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected PositiveInfImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return TypePackage.Literals.POSITIVE_INF;
	}

} //PositiveInfImpl
