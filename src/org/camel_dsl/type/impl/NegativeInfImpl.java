/**
 */
package org.camel_dsl.type.impl;

import org.camel_dsl.type.NegativeInf;
import org.camel_dsl.type.TypePackage;

import org.eclipse.emf.ecore.EClass;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Negative Inf</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class NegativeInfImpl extends NumericValueImpl implements NegativeInf {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected NegativeInfImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return TypePackage.Literals.NEGATIVE_INF;
	}

} //NegativeInfImpl
