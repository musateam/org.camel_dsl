/**
 */
package org.camel_dsl.provider;

import org.eclipse.emf.cdo.CDOObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Scope</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see org.camel_dsl.provider.ProviderPackage#getScope()
 * @model abstract="true"
 * @extends CDOObject
 * @generated
 */
public interface Scope extends CDOObject {
} // Scope
