/**
 */
package org.camel_dsl.provider;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Excludes</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see org.camel_dsl.provider.ProviderPackage#getExcludes()
 * @model
 * @generated
 */
public interface Excludes extends Constraint {
} // Excludes
