/**
 */
package org.camel_dsl.provider;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Group Cardinality</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see org.camel_dsl.provider.ProviderPackage#getGroupCardinality()
 * @model
 * @generated
 */
public interface GroupCardinality extends Cardinality {
} // GroupCardinality
