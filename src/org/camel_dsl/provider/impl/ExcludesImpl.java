/**
 */
package org.camel_dsl.provider.impl;

import org.camel_dsl.provider.Excludes;
import org.camel_dsl.provider.ProviderPackage;

import org.eclipse.emf.ecore.EClass;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Excludes</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class ExcludesImpl extends ConstraintImpl implements Excludes {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ExcludesImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return ProviderPackage.Literals.EXCLUDES;
	}

} //ExcludesImpl
