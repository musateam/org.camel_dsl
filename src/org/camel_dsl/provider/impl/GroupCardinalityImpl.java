/**
 */
package org.camel_dsl.provider.impl;

import org.camel_dsl.provider.GroupCardinality;
import org.camel_dsl.provider.ProviderPackage;

import org.eclipse.emf.ecore.EClass;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Group Cardinality</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class GroupCardinalityImpl extends CardinalityImpl implements GroupCardinality {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected GroupCardinalityImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return ProviderPackage.Literals.GROUP_CARDINALITY;
	}

} //GroupCardinalityImpl
