/**
 */
package org.camel_dsl.provider.impl;

import org.camel_dsl.provider.Alternative;
import org.camel_dsl.provider.Feature;
import org.camel_dsl.provider.GroupCardinality;
import org.camel_dsl.provider.ProviderPackage;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Alternative</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link org.camel_dsl.provider.impl.AlternativeImpl#getGroupCardinality <em>Group Cardinality</em>}</li>
 *   <li>{@link org.camel_dsl.provider.impl.AlternativeImpl#getVariants <em>Variants</em>}</li>
 * </ul>
 *
 * @generated
 */
public class AlternativeImpl extends FeatureImpl implements Alternative {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected AlternativeImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return ProviderPackage.Literals.ALTERNATIVE;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public GroupCardinality getGroupCardinality() {
		return (GroupCardinality)eGet(ProviderPackage.Literals.ALTERNATIVE__GROUP_CARDINALITY, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setGroupCardinality(GroupCardinality newGroupCardinality) {
		eSet(ProviderPackage.Literals.ALTERNATIVE__GROUP_CARDINALITY, newGroupCardinality);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	public EList<Feature> getVariants() {
		return (EList<Feature>)eGet(ProviderPackage.Literals.ALTERNATIVE__VARIANTS, true);
	}

} //AlternativeImpl
