/**
 */
package org.camel_dsl.provider;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Exclusive</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see org.camel_dsl.provider.ProviderPackage#getExclusive()
 * @model
 * @generated
 */
public interface Exclusive extends Alternative {
} // Exclusive
