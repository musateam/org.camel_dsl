/**
 */
package org.camel_dsl.provider;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Product</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see org.camel_dsl.provider.ProviderPackage#getProduct()
 * @model
 * @generated
 */
public interface Product extends Scope {
} // Product
