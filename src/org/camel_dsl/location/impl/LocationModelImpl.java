/**
 */
package org.camel_dsl.location.impl;

import org.camel_dsl.camel.impl.ModelImpl;

import org.camel_dsl.location.CloudLocation;
import org.camel_dsl.location.Country;
import org.camel_dsl.location.GeographicalRegion;
import org.camel_dsl.location.LocationModel;
import org.camel_dsl.location.LocationPackage;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Model</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link org.camel_dsl.location.impl.LocationModelImpl#getCloudLocations <em>Cloud Locations</em>}</li>
 *   <li>{@link org.camel_dsl.location.impl.LocationModelImpl#getCountries <em>Countries</em>}</li>
 *   <li>{@link org.camel_dsl.location.impl.LocationModelImpl#getRegions <em>Regions</em>}</li>
 * </ul>
 *
 * @generated
 */
public class LocationModelImpl extends ModelImpl implements LocationModel {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected LocationModelImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return LocationPackage.Literals.LOCATION_MODEL;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	public EList<CloudLocation> getCloudLocations() {
		return (EList<CloudLocation>)eGet(LocationPackage.Literals.LOCATION_MODEL__CLOUD_LOCATIONS, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	public EList<Country> getCountries() {
		return (EList<Country>)eGet(LocationPackage.Literals.LOCATION_MODEL__COUNTRIES, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	public EList<GeographicalRegion> getRegions() {
		return (EList<GeographicalRegion>)eGet(LocationPackage.Literals.LOCATION_MODEL__REGIONS, true);
	}

} //LocationModelImpl
