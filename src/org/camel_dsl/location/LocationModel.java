/**
 */
package org.camel_dsl.location;

import org.camel_dsl.camel.Model;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Model</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.camel_dsl.location.LocationModel#getCloudLocations <em>Cloud Locations</em>}</li>
 *   <li>{@link org.camel_dsl.location.LocationModel#getCountries <em>Countries</em>}</li>
 *   <li>{@link org.camel_dsl.location.LocationModel#getRegions <em>Regions</em>}</li>
 * </ul>
 *
 * @see org.camel_dsl.location.LocationPackage#getLocationModel()
 * @model
 * @generated
 */
public interface LocationModel extends Model {
	/**
	 * Returns the value of the '<em><b>Cloud Locations</b></em>' containment reference list.
	 * The list contents are of type {@link org.camel_dsl.location.CloudLocation}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Cloud Locations</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Cloud Locations</em>' containment reference list.
	 * @see org.camel_dsl.location.LocationPackage#getLocationModel_CloudLocations()
	 * @model containment="true"
	 * @generated
	 */
	EList<CloudLocation> getCloudLocations();

	/**
	 * Returns the value of the '<em><b>Countries</b></em>' containment reference list.
	 * The list contents are of type {@link org.camel_dsl.location.Country}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Countries</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Countries</em>' containment reference list.
	 * @see org.camel_dsl.location.LocationPackage#getLocationModel_Countries()
	 * @model containment="true"
	 * @generated
	 */
	EList<Country> getCountries();

	/**
	 * Returns the value of the '<em><b>Regions</b></em>' containment reference list.
	 * The list contents are of type {@link org.camel_dsl.location.GeographicalRegion}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Regions</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Regions</em>' containment reference list.
	 * @see org.camel_dsl.location.LocationPackage#getLocationModel_Regions()
	 * @model containment="true"
	 * @generated
	 */
	EList<GeographicalRegion> getRegions();

} // LocationModel
