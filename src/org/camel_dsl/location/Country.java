/**
 */
package org.camel_dsl.location;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Country</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see org.camel_dsl.location.LocationPackage#getCountry()
 * @model
 * @generated
 */
public interface Country extends GeographicalRegion {
} // Country
