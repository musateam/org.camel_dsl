/**
 */
package org.camel_dsl.organisation;

import org.eclipse.emf.cdo.CDOObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Entity</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see org.camel_dsl.organisation.OrganisationPackage#getEntity()
 * @model
 * @extends CDOObject
 * @generated
 */
public interface Entity extends CDOObject {
} // Entity
