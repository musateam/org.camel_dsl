/**
 */
package org.camel_dsl.organisation.impl;

import org.camel_dsl.camel.impl.ModelImpl;

import org.camel_dsl.organisation.CloudProvider;
import org.camel_dsl.organisation.DataCenter;
import org.camel_dsl.organisation.ExternalIdentifier;
import org.camel_dsl.organisation.Organisation;
import org.camel_dsl.organisation.OrganisationModel;
import org.camel_dsl.organisation.OrganisationPackage;
import org.camel_dsl.organisation.Permission;
import org.camel_dsl.organisation.ResourceFilter;
import org.camel_dsl.organisation.Role;
import org.camel_dsl.organisation.RoleAssignment;
import org.camel_dsl.organisation.SecurityLevel;
import org.camel_dsl.organisation.User;
import org.camel_dsl.organisation.UserGroup;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Model</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link org.camel_dsl.organisation.impl.OrganisationModelImpl#getOrganisation <em>Organisation</em>}</li>
 *   <li>{@link org.camel_dsl.organisation.impl.OrganisationModelImpl#getProvider <em>Provider</em>}</li>
 *   <li>{@link org.camel_dsl.organisation.impl.OrganisationModelImpl#getExternalIdentifiers <em>External Identifiers</em>}</li>
 *   <li>{@link org.camel_dsl.organisation.impl.OrganisationModelImpl#getUsers <em>Users</em>}</li>
 *   <li>{@link org.camel_dsl.organisation.impl.OrganisationModelImpl#getUserGroups <em>User Groups</em>}</li>
 *   <li>{@link org.camel_dsl.organisation.impl.OrganisationModelImpl#getDataCentres <em>Data Centres</em>}</li>
 *   <li>{@link org.camel_dsl.organisation.impl.OrganisationModelImpl#getRoles <em>Roles</em>}</li>
 *   <li>{@link org.camel_dsl.organisation.impl.OrganisationModelImpl#getRoleAssigments <em>Role Assigments</em>}</li>
 *   <li>{@link org.camel_dsl.organisation.impl.OrganisationModelImpl#getPermissions <em>Permissions</em>}</li>
 *   <li>{@link org.camel_dsl.organisation.impl.OrganisationModelImpl#getSecurityLevel <em>Security Level</em>}</li>
 *   <li>{@link org.camel_dsl.organisation.impl.OrganisationModelImpl#getResourceFilters <em>Resource Filters</em>}</li>
 * </ul>
 *
 * @generated
 */
public class OrganisationModelImpl extends ModelImpl implements OrganisationModel {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected OrganisationModelImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return OrganisationPackage.Literals.ORGANISATION_MODEL;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Organisation getOrganisation() {
		return (Organisation)eGet(OrganisationPackage.Literals.ORGANISATION_MODEL__ORGANISATION, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setOrganisation(Organisation newOrganisation) {
		eSet(OrganisationPackage.Literals.ORGANISATION_MODEL__ORGANISATION, newOrganisation);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public CloudProvider getProvider() {
		return (CloudProvider)eGet(OrganisationPackage.Literals.ORGANISATION_MODEL__PROVIDER, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setProvider(CloudProvider newProvider) {
		eSet(OrganisationPackage.Literals.ORGANISATION_MODEL__PROVIDER, newProvider);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	public EList<ExternalIdentifier> getExternalIdentifiers() {
		return (EList<ExternalIdentifier>)eGet(OrganisationPackage.Literals.ORGANISATION_MODEL__EXTERNAL_IDENTIFIERS, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	public EList<User> getUsers() {
		return (EList<User>)eGet(OrganisationPackage.Literals.ORGANISATION_MODEL__USERS, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	public EList<UserGroup> getUserGroups() {
		return (EList<UserGroup>)eGet(OrganisationPackage.Literals.ORGANISATION_MODEL__USER_GROUPS, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	public EList<DataCenter> getDataCentres() {
		return (EList<DataCenter>)eGet(OrganisationPackage.Literals.ORGANISATION_MODEL__DATA_CENTRES, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	public EList<Role> getRoles() {
		return (EList<Role>)eGet(OrganisationPackage.Literals.ORGANISATION_MODEL__ROLES, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	public EList<RoleAssignment> getRoleAssigments() {
		return (EList<RoleAssignment>)eGet(OrganisationPackage.Literals.ORGANISATION_MODEL__ROLE_ASSIGMENTS, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	public EList<Permission> getPermissions() {
		return (EList<Permission>)eGet(OrganisationPackage.Literals.ORGANISATION_MODEL__PERMISSIONS, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SecurityLevel getSecurityLevel() {
		return (SecurityLevel)eGet(OrganisationPackage.Literals.ORGANISATION_MODEL__SECURITY_LEVEL, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setSecurityLevel(SecurityLevel newSecurityLevel) {
		eSet(OrganisationPackage.Literals.ORGANISATION_MODEL__SECURITY_LEVEL, newSecurityLevel);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	public EList<ResourceFilter> getResourceFilters() {
		return (EList<ResourceFilter>)eGet(OrganisationPackage.Literals.ORGANISATION_MODEL__RESOURCE_FILTERS, true);
	}

} //OrganisationModelImpl
