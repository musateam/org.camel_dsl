/**
 */
package org.camel_dsl.organisation.impl;

import java.util.Date;

import org.camel_dsl.organisation.MUSACredentials;
import org.camel_dsl.organisation.OrganisationPackage;

import org.eclipse.emf.ecore.EClass;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>MUSA Credentials</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link org.camel_dsl.organisation.impl.MUSACredentialsImpl#getUsername <em>Username</em>}</li>
 *   <li>{@link org.camel_dsl.organisation.impl.MUSACredentialsImpl#getPassword <em>Password</em>}</li>
 *   <li>{@link org.camel_dsl.organisation.impl.MUSACredentialsImpl#getEndTime <em>End Time</em>}</li>
 * </ul>
 *
 * @generated
 */
public class MUSACredentialsImpl extends CredentialsImpl implements MUSACredentials {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected MUSACredentialsImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return OrganisationPackage.Literals.MUSA_CREDENTIALS;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getUsername() {
		return (String)eGet(OrganisationPackage.Literals.MUSA_CREDENTIALS__USERNAME, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setUsername(String newUsername) {
		eSet(OrganisationPackage.Literals.MUSA_CREDENTIALS__USERNAME, newUsername);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getPassword() {
		return (String)eGet(OrganisationPackage.Literals.MUSA_CREDENTIALS__PASSWORD, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setPassword(String newPassword) {
		eSet(OrganisationPackage.Literals.MUSA_CREDENTIALS__PASSWORD, newPassword);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Date getEndTime() {
		return (Date)eGet(OrganisationPackage.Literals.MUSA_CREDENTIALS__END_TIME, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setEndTime(Date newEndTime) {
		eSet(OrganisationPackage.Literals.MUSA_CREDENTIALS__END_TIME, newEndTime);
	}

} //MUSACredentialsImpl
