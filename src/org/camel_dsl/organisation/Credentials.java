/**
 */
package org.camel_dsl.organisation;

import org.eclipse.emf.cdo.CDOObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Credentials</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see org.camel_dsl.organisation.OrganisationPackage#getCredentials()
 * @model abstract="true"
 * @extends CDOObject
 * @generated
 */
public interface Credentials extends CDOObject {
} // Credentials
