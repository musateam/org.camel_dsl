/**
 */
package org.camel_dsl.organisation;

import org.eclipse.emf.cdo.CDOObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Resource Filter</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.camel_dsl.organisation.ResourceFilter#getName <em>Name</em>}</li>
 *   <li>{@link org.camel_dsl.organisation.ResourceFilter#getResourcePattern <em>Resource Pattern</em>}</li>
 * </ul>
 *
 * @see org.camel_dsl.organisation.OrganisationPackage#getResourceFilter()
 * @model abstract="true"
 * @extends CDOObject
 * @generated
 */
public interface ResourceFilter extends CDOObject {
	/**
	 * Returns the value of the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Name</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Name</em>' attribute.
	 * @see #setName(String)
	 * @see org.camel_dsl.organisation.OrganisationPackage#getResourceFilter_Name()
	 * @model required="true"
	 * @generated
	 */
	String getName();

	/**
	 * Sets the value of the '{@link org.camel_dsl.organisation.ResourceFilter#getName <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Name</em>' attribute.
	 * @see #getName()
	 * @generated
	 */
	void setName(String value);

	/**
	 * Returns the value of the '<em><b>Resource Pattern</b></em>' attribute.
	 * The literals are from the enumeration {@link org.camel_dsl.organisation.ResourcePattern}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Resource Pattern</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Resource Pattern</em>' attribute.
	 * @see org.camel_dsl.organisation.ResourcePattern
	 * @see #setResourcePattern(ResourcePattern)
	 * @see org.camel_dsl.organisation.OrganisationPackage#getResourceFilter_ResourcePattern()
	 * @model required="true"
	 * @generated
	 */
	ResourcePattern getResourcePattern();

	/**
	 * Sets the value of the '{@link org.camel_dsl.organisation.ResourceFilter#getResourcePattern <em>Resource Pattern</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Resource Pattern</em>' attribute.
	 * @see org.camel_dsl.organisation.ResourcePattern
	 * @see #getResourcePattern()
	 * @generated
	 */
	void setResourcePattern(ResourcePattern value);

} // ResourceFilter
