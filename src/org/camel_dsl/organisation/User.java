/**
 */
package org.camel_dsl.organisation;

import org.camel_dsl.deployment.DeploymentModel;

import org.camel_dsl.requirement.RequirementModel;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>User</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.camel_dsl.organisation.User#getName <em>Name</em>}</li>
 *   <li>{@link org.camel_dsl.organisation.User#getEmail <em>Email</em>}</li>
 *   <li>{@link org.camel_dsl.organisation.User#getFirstName <em>First Name</em>}</li>
 *   <li>{@link org.camel_dsl.organisation.User#getLastName <em>Last Name</em>}</li>
 *   <li>{@link org.camel_dsl.organisation.User#getWww <em>Www</em>}</li>
 *   <li>{@link org.camel_dsl.organisation.User#getExternalIdentifiers <em>External Identifiers</em>}</li>
 *   <li>{@link org.camel_dsl.organisation.User#getRequirementModels <em>Requirement Models</em>}</li>
 *   <li>{@link org.camel_dsl.organisation.User#getCloudCredentials <em>Cloud Credentials</em>}</li>
 *   <li>{@link org.camel_dsl.organisation.User#getDeploymentModels <em>Deployment Models</em>}</li>
 *   <li>{@link org.camel_dsl.organisation.User#getMUSACredentials <em>MUSA Credentials</em>}</li>
 * </ul>
 *
 * @see org.camel_dsl.organisation.OrganisationPackage#getUser()
 * @model
 * @generated
 */
public interface User extends Entity {
	/**
	 * Returns the value of the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Name</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Name</em>' attribute.
	 * @see #setName(String)
	 * @see org.camel_dsl.organisation.OrganisationPackage#getUser_Name()
	 * @model required="true"
	 * @generated
	 */
	String getName();

	/**
	 * Sets the value of the '{@link org.camel_dsl.organisation.User#getName <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Name</em>' attribute.
	 * @see #getName()
	 * @generated
	 */
	void setName(String value);

	/**
	 * Returns the value of the '<em><b>Email</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Email</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Email</em>' attribute.
	 * @see #setEmail(String)
	 * @see org.camel_dsl.organisation.OrganisationPackage#getUser_Email()
	 * @model required="true"
	 * @generated
	 */
	String getEmail();

	/**
	 * Sets the value of the '{@link org.camel_dsl.organisation.User#getEmail <em>Email</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Email</em>' attribute.
	 * @see #getEmail()
	 * @generated
	 */
	void setEmail(String value);

	/**
	 * Returns the value of the '<em><b>First Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>First Name</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>First Name</em>' attribute.
	 * @see #setFirstName(String)
	 * @see org.camel_dsl.organisation.OrganisationPackage#getUser_FirstName()
	 * @model required="true"
	 * @generated
	 */
	String getFirstName();

	/**
	 * Sets the value of the '{@link org.camel_dsl.organisation.User#getFirstName <em>First Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>First Name</em>' attribute.
	 * @see #getFirstName()
	 * @generated
	 */
	void setFirstName(String value);

	/**
	 * Returns the value of the '<em><b>Last Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Last Name</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Last Name</em>' attribute.
	 * @see #setLastName(String)
	 * @see org.camel_dsl.organisation.OrganisationPackage#getUser_LastName()
	 * @model required="true"
	 * @generated
	 */
	String getLastName();

	/**
	 * Sets the value of the '{@link org.camel_dsl.organisation.User#getLastName <em>Last Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Last Name</em>' attribute.
	 * @see #getLastName()
	 * @generated
	 */
	void setLastName(String value);

	/**
	 * Returns the value of the '<em><b>Www</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Www</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Www</em>' attribute.
	 * @see #setWww(String)
	 * @see org.camel_dsl.organisation.OrganisationPackage#getUser_Www()
	 * @model
	 * @generated
	 */
	String getWww();

	/**
	 * Sets the value of the '{@link org.camel_dsl.organisation.User#getWww <em>Www</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Www</em>' attribute.
	 * @see #getWww()
	 * @generated
	 */
	void setWww(String value);

	/**
	 * Returns the value of the '<em><b>External Identifiers</b></em>' containment reference list.
	 * The list contents are of type {@link org.camel_dsl.organisation.ExternalIdentifier}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>External Identifiers</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>External Identifiers</em>' containment reference list.
	 * @see org.camel_dsl.organisation.OrganisationPackage#getUser_ExternalIdentifiers()
	 * @model containment="true"
	 * @generated
	 */
	EList<ExternalIdentifier> getExternalIdentifiers();

	/**
	 * Returns the value of the '<em><b>Requirement Models</b></em>' reference list.
	 * The list contents are of type {@link org.camel_dsl.requirement.RequirementModel}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Requirement Models</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Requirement Models</em>' reference list.
	 * @see org.camel_dsl.organisation.OrganisationPackage#getUser_RequirementModels()
	 * @model
	 * @generated
	 */
	EList<RequirementModel> getRequirementModels();

	/**
	 * Returns the value of the '<em><b>Cloud Credentials</b></em>' containment reference list.
	 * The list contents are of type {@link org.camel_dsl.organisation.CloudCredentials}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Cloud Credentials</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Cloud Credentials</em>' containment reference list.
	 * @see org.camel_dsl.organisation.OrganisationPackage#getUser_CloudCredentials()
	 * @model containment="true"
	 * @generated
	 */
	EList<CloudCredentials> getCloudCredentials();

	/**
	 * Returns the value of the '<em><b>Deployment Models</b></em>' reference list.
	 * The list contents are of type {@link org.camel_dsl.deployment.DeploymentModel}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Deployment Models</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Deployment Models</em>' reference list.
	 * @see org.camel_dsl.organisation.OrganisationPackage#getUser_DeploymentModels()
	 * @model
	 * @generated
	 */
	EList<DeploymentModel> getDeploymentModels();

	/**
	 * Returns the value of the '<em><b>MUSA Credentials</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>MUSA Credentials</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>MUSA Credentials</em>' containment reference.
	 * @see #setMUSACredentials(MUSACredentials)
	 * @see org.camel_dsl.organisation.OrganisationPackage#getUser_MUSACredentials()
	 * @model containment="true" required="true"
	 * @generated
	 */
	MUSACredentials getMUSACredentials();

	/**
	 * Sets the value of the '{@link org.camel_dsl.organisation.User#getMUSACredentials <em>MUSA Credentials</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>MUSA Credentials</em>' containment reference.
	 * @see #getMUSACredentials()
	 * @generated
	 */
	void setMUSACredentials(MUSACredentials value);

} // User
